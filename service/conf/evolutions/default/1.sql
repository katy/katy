# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table author (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  constraint pk_author primary key (id))
;

create table banner (
  id                        bigint auto_increment not null,
  link                      varchar(255),
  description               varchar(255),
  width                     integer,
  height                    integer,
  date                      datetime,
  constraint pk_banner primary key (id))
;

create table book (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255),
  descr                     varchar(255),
  date                      datetime,
  author_id                 bigint,
  genre_id                  bigint,
  constraint pk_book primary key (id))
;

create table college (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255),
  descr                     varchar(255),
  date                      datetime,
  constraint pk_college primary key (id))
;

create table comment (
  id                        bigint auto_increment not null,
  content                   longtext not null,
  owner_id                  bigint,
  material_id               bigint,
  comment_id                bigint,
  date                      datetime,
  constraint pk_comment primary key (id))
;

create table genre (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255),
  descr                     varchar(255),
  date                      datetime,
  parent_id                 bigint,
  cached_genres             varchar(255),
  constraint pk_genre primary key (id))
;

create table material (
  id                        bigint auto_increment not null,
  header                    varchar(255) not null,
  content                   longtext not null,
  image                     varchar(255),
  type                      varchar(255) not null,
  owner_id                  bigint,
  publication               varchar(255),
  cached_attr_users         varchar(255),
  cached_attr_books         varchar(255),
  cached_attr_authors       varchar(255),
  cached_attr_genres        varchar(255),
  theme                     varchar(255),
  cached_igo_users          varchar(255),
  cached_igo_users_count    integer,
  cached_ilike_users        varchar(255),
  cached_ilike_users_count  integer,
  cached_comments_count     integer,
  cached_comments           varchar(255),
  date                      datetime,
  start_date                datetime,
  end_date                  datetime,
  mat_status_id             bigint,
  editor_status_id          bigint,
  constraint pk_material primary key (id))
;

create table material_status (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255),
  descr                     varchar(255),
  date                      datetime,
  constraint pk_material_status primary key (id))
;

create table media (
  id                        bigint auto_increment not null,
  owner_id                  bigint,
  name                      varchar(255),
  type                      varchar(255) not null,
  constraint pk_media primary key (id))
;

create table page (
  id                        bigint auto_increment not null,
  header                    varchar(255) not null,
  content                   longtext not null,
  visible                   tinyint(1) default 0,
  user_view                 tinyint(1) default 0,
  user_edit                 tinyint(1) default 0,
  editor_view               tinyint(1) default 0,
  editor_edit               tinyint(1) default 0,
  constraint pk_page primary key (id))
;

create table params (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255) not null,
  descr                     varchar(255),
  constraint pk_params primary key (id))
;

create table security_role (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  constraint uq_security_role_name unique (name),
  constraint pk_security_role primary key (id))
;

create table status (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255),
  descr                     varchar(255),
  date                      datetime,
  constraint pk_status primary key (id))
;

create table users (
  id                        bigint auto_increment not null,
  password                  varchar(255) not null,
  hash                      varchar(255),
  ip                        varchar(255),
  first_name                varchar(255),
  second_name               varchar(255),
  middle_name               varchar(255),
  cached_band               varchar(255),
  register_date             datetime,
  last_enter                datetime,
  cached_unviwed_comments   varchar(255),
  cached_unviwed_comments_count integer,
  status_id                 bigint,
  email                     varchar(255) not null,
  birthday                  datetime,
  college_id                bigint,
  avatar                    varchar(255),
  cached_igo_materials      varchar(255),
  cached_igo_materials_count integer,
  cached_ilike_materials    varchar(255),
  constraint uq_users_email unique (email),
  constraint pk_users primary key (id))
;


create table comment_users (
  comment_id                     bigint not null,
  users_id                       bigint not null,
  constraint pk_comment_users primary key (comment_id, users_id))
;

create table material_book (
  material_id                    bigint not null,
  book_id                        bigint not null,
  constraint pk_material_book primary key (material_id, book_id))
;

create table material_author (
  material_id                    bigint not null,
  author_id                      bigint not null,
  constraint pk_material_author primary key (material_id, author_id))
;

create table material_genre (
  material_id                    bigint not null,
  genre_id                       bigint not null,
  constraint pk_material_genre primary key (material_id, genre_id))
;

create table users_security_role (
  users_id                       bigint not null,
  security_role_id               bigint not null,
  constraint pk_users_security_role primary key (users_id, security_role_id))
;

create table users_users (
  person_id                      bigint not null,
  friend_id                      bigint not null,
  constraint pk_users_users primary key (person_id, friend_id))
;

create table users_book (
  users_id                       bigint not null,
  book_id                        bigint not null,
  constraint pk_users_book primary key (users_id, book_id))
;

create table users_author (
  users_id                       bigint not null,
  author_id                      bigint not null,
  constraint pk_users_author primary key (users_id, author_id))
;

create table users_genre (
  users_id                       bigint not null,
  genre_id                       bigint not null,
  constraint pk_users_genre primary key (users_id, genre_id))
;

create table users_college (
  users_id                       bigint not null,
  college_id                     bigint not null,
  constraint pk_users_college primary key (users_id, college_id))
;

create table users_igo_materials (
  user_id                        bigint not null,
  material_id                    bigint not null,
  constraint pk_users_igo_materials primary key (user_id, material_id))
;

create table users_ilike_materials (
  user_id                        bigint not null,
  material_id                    bigint not null,
  constraint pk_users_ilike_materials primary key (user_id, material_id))
;
alter table book add constraint fk_book_author_1 foreign key (author_id) references author (id) on delete restrict on update restrict;
create index ix_book_author_1 on book (author_id);
alter table book add constraint fk_book_genre_2 foreign key (genre_id) references genre (id) on delete restrict on update restrict;
create index ix_book_genre_2 on book (genre_id);
alter table comment add constraint fk_comment_owner_3 foreign key (owner_id) references users (id) on delete restrict on update restrict;
create index ix_comment_owner_3 on comment (owner_id);
alter table comment add constraint fk_comment_material_4 foreign key (material_id) references material (id) on delete restrict on update restrict;
create index ix_comment_material_4 on comment (material_id);
alter table comment add constraint fk_comment_comment_5 foreign key (comment_id) references comment (id) on delete restrict on update restrict;
create index ix_comment_comment_5 on comment (comment_id);
alter table genre add constraint fk_genre_parent_6 foreign key (parent_id) references genre (id) on delete restrict on update restrict;
create index ix_genre_parent_6 on genre (parent_id);
alter table material add constraint fk_material_owner_7 foreign key (owner_id) references users (id) on delete restrict on update restrict;
create index ix_material_owner_7 on material (owner_id);
alter table material add constraint fk_material_matStatus_8 foreign key (mat_status_id) references material_status (id) on delete restrict on update restrict;
create index ix_material_matStatus_8 on material (mat_status_id);
alter table material add constraint fk_material_editorStatus_9 foreign key (editor_status_id) references material_status (id) on delete restrict on update restrict;
create index ix_material_editorStatus_9 on material (editor_status_id);
alter table users add constraint fk_users_status_10 foreign key (status_id) references status (id) on delete restrict on update restrict;
create index ix_users_status_10 on users (status_id);
alter table users add constraint fk_users_college_11 foreign key (college_id) references college (id) on delete restrict on update restrict;
create index ix_users_college_11 on users (college_id);



alter table comment_users add constraint fk_comment_users_comment_01 foreign key (comment_id) references comment (id) on delete restrict on update restrict;

alter table comment_users add constraint fk_comment_users_users_02 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table material_book add constraint fk_material_book_material_01 foreign key (material_id) references material (id) on delete restrict on update restrict;

alter table material_book add constraint fk_material_book_book_02 foreign key (book_id) references book (id) on delete restrict on update restrict;

alter table material_author add constraint fk_material_author_material_01 foreign key (material_id) references material (id) on delete restrict on update restrict;

alter table material_author add constraint fk_material_author_author_02 foreign key (author_id) references author (id) on delete restrict on update restrict;

alter table material_genre add constraint fk_material_genre_material_01 foreign key (material_id) references material (id) on delete restrict on update restrict;

alter table material_genre add constraint fk_material_genre_genre_02 foreign key (genre_id) references genre (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_security_role_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

alter table users_users add constraint fk_users_users_users_01 foreign key (person_id) references users (id) on delete restrict on update restrict;

alter table users_users add constraint fk_users_users_users_02 foreign key (friend_id) references users (id) on delete restrict on update restrict;

alter table users_book add constraint fk_users_book_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_book add constraint fk_users_book_book_02 foreign key (book_id) references book (id) on delete restrict on update restrict;

alter table users_author add constraint fk_users_author_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_author add constraint fk_users_author_author_02 foreign key (author_id) references author (id) on delete restrict on update restrict;

alter table users_genre add constraint fk_users_genre_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_genre add constraint fk_users_genre_genre_02 foreign key (genre_id) references genre (id) on delete restrict on update restrict;

alter table users_college add constraint fk_users_college_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_college add constraint fk_users_college_college_02 foreign key (college_id) references college (id) on delete restrict on update restrict;

alter table users_igo_materials add constraint fk_users_igo_materials_users_01 foreign key (user_id) references users (id) on delete restrict on update restrict;

alter table users_igo_materials add constraint fk_users_igo_materials_material_02 foreign key (material_id) references material (id) on delete restrict on update restrict;

alter table users_ilike_materials add constraint fk_users_ilike_materials_users_01 foreign key (user_id) references users (id) on delete restrict on update restrict;

alter table users_ilike_materials add constraint fk_users_ilike_materials_material_02 foreign key (material_id) references material (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table author;

drop table users_author;

drop table material_author;

drop table banner;

drop table book;

drop table users_book;

drop table material_book;

drop table college;

drop table users_college;

drop table comment;

drop table comment_users;

drop table genre;

drop table users_genre;

drop table material_genre;

drop table material;

drop table users_igo_materials;

drop table users_ilike_materials;

drop table material_status;

drop table media;

drop table page;

drop table params;

drop table security_role;

drop table status;

drop table users;

drop table users_security_role;

drop table users_users;

SET FOREIGN_KEY_CHECKS=1;

