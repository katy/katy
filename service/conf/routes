# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Main actions
GET     /                           				  controllers.Application.index()

# Application actions

GET     /app/getbookform                              controllers.Materials.getBookInputForm(bookNameF:String, bookAuthorF:String, bookGenreF:String)
GET     /app/getauthorform                            controllers.Materials.getAuthorInputForm(bookAuthorF:String)
GET     /app/getgenreform                             controllers.Materials.getGenreInputForm(bookGenreF:String)



# Users actions

POST	/users/login                                  controllers.Users.login(email: String, password: String)


GET     /users/user/:id/igo/count                     controllers.Materials.getIgoMaterialsCountA(id: Long)
GET     /users/user/:id/my/count                      controllers.Materials.getMyMaterialsCountA(id: Long)
GET     /users/user/:id/drafts/count                  controllers.Materials.getMyDraftsCountA(id: Long)
GET     /users/user/:id/comments/unviewed/count       controllers.Materials.getUnviewedCommentsCountA(id: Long)
GET     /users/user/:id/comments/unviewed/setviewed   controllers.Comments.setCommentViewed(id: Long, cid: Long)

GET     /users/logout                                 controllers.Users.logout()
GET     /users/user/:id/profile                       controllers.Users.profile(id: Long)
GET     /users/profileAuthUser                        controllers.Users.profileAuthUser()

GET     /users/afterRegister                          controllers.Users.afterRegister()
GET     /users/register                               controllers.Users.register()
POST    /users/register                               controllers.Users.processRegister()
GET     /users/user/:id/edit                          controllers.Users.editProfile(id: Long)

POST    /users/user/:id/band/setListenObject          controllers.Users.setListenObject(id: Long, name: String, objId : Long, isListen: Boolean)

GET     /users/user/:id/band/listen/:name/pages/count      controllers.Users.getListenObjectsPagesCount(id: Long, name: String)
GET     /users/user/:id/band/listen/:name/pages/page/:nid  controllers.Users.getListenObjectsPage(id: Long, name: String, nid: Integer)

POST	/users/user/:id/secondName		              controllers.Users.changeUserSecondName(id: Long, newValue: String)
POST	/users/user/:id/firstName		              controllers.Users.changeUserFirstName(id: Long, newValue: String)
POST	/users/user/:id/middleName		              controllers.Users.changeUserMiddleName(id: Long, newValue: String)
POST	/users/user/:id/birthday		              controllers.Users.changeUserBirthday(id: Long, newValue: String)
POST	/users/user/:id/status		                  controllers.Users.changeUserStatus(id: Long, newValue: String)
POST	/users/user/:id/college         		      controllers.Users.changeUserCollege(id: Long, newValue: String)

GET 	/users/user/:id/band/edit         		      controllers.Users.editUserBand(id: Long)

POST    /users/user/:id/avatar                        controllers.Users.processAvatarUpload(id: Long)

GET     /users/user/:uid/materials/material/:mid/isgo    controllers.Users.isIgo(uid :Long, mid :Long) 
POST    /users/user/:uid/materials/material/:mid/setgo   controllers.Users.setIgo(uid :Long, mid: Long, isGoing :Boolean) 

GET     /users/user/:uid/materials/material/:mid/islike    controllers.Users.isIlike(uid :Long, mid :Long) 
POST    /users/user/:uid/materials/material/:mid/setlike   controllers.Users.setIlike(uid :Long, mid: Long, isGoing :Boolean) 

# Admin actions

GET     /admin                                      controllers.Admin.admin()
GET     /admin/tests                                controllers.Admin.adminTests()

## ВУЗ'ы
GET	/admin/colleges				    controllers.Admin.adminColleges()
GET     /admin/colleges/pages/count                 controllers.Admin.getCollegesPagesCount()
GET     /admin/colleges/pages/page/:index           controllers.Admin.getCollegesPage(index : Integer)
POST 	/admin/colleges/college/:id/name	    controllers.Admin.changeCollegeName(id: Long, newValue: String)

## Жанры
GET	/admin/genres		                    controllers.Admin.adminGenres()
GET     /admin/genres/pages/count                   controllers.Admin.getGenresPagesCount()
GET     /admin/genres/pages/page/:index             controllers.Admin.getGenresPage(index : Integer)
POST 	/admin/genres/genre/:id/name	            controllers.Admin.changeGenreName(id: Long, newValue: String)
#GET     /admin/genres/pagesCount/:genreType         controllers.Admin.getGenresPagesCount(genreType: String)
#GET     /admin/genres/page/:genreType/:index        controllers.Admin.getGenresPage(genreType: String, index : Integer)
#POST    /admin/genres/genre/:id/setBase             controllers.Admin.setGenreBase(id: Long, isGenre: Boolean)


## Параметры
GET	/admin/params		                    controllers.Admin.adminParams()
GET     /admin/params/pages/count                   controllers.Admin.getParamsPagesCount()
GET     /admin/params/pages/page/:index             controllers.Admin.getParamsPage(index : Integer)
POST 	/admin/params/param/:id/name	            controllers.Admin.changeParamName(id: Long, newValue: String)
POST	/admin/params/param/:id/value	            controllers.Admin.changeParamValue(id: Long, newValue: String)
POST	/admin/params/param/:id/descr	            controllers.Admin.changeParamDescr(id: Long, newValue: String)

## Страницы
GET	/pages/create	  	            	    controllers.Pages.create()
POST	/pages/modify	  	            	    controllers.Pages.processEdit(id: Long)
GET	/pages/page/:id	  	            	    controllers.Pages.page(id: Long)
GET	/pages/page/:id/remove 	            	    controllers.Pages.remove(id: Long)
GET	/pages/page/:id/visible	            	    controllers.Pages.setVisible(id: Long, visible: Boolean)
GET	/pages/page/:id/edit	            	    controllers.Pages.edit(id: Long)

GET	/admin/pages 		  	            controllers.Admin.adminPages()
GET     /admin/pages/pagesCount		            controllers.Admin.getPagesCount()
GET     /admin/pages/page/:id 	        	    controllers.Admin.getPagesPage(id : Integer)

## Пользователи
GET	/admin/users			            controllers.Admin.adminUsers()
POST    /admin/users/user/:id/setEditor             controllers.Admin.setUserEditor(id: Long, isEditor: Boolean)
GET     /admin/users/pagesCount/:userType           controllers.Admin.getUsersPagesCount(userType: String)
GET     /admin/users/page/:userType/:index          controllers.Admin.getUsersPage(userType: String, index : Integer)

# Materials actions

GET 	/materials/view/custom/college/:id          controllers.Materials.materialsByCollege(id: Long)
GET 	/materials/view/custom/genre/:id            controllers.Materials.materialsByGenre(id: Long)
GET 	/materials/view/custom/book/:id             controllers.Materials.materialsByBook(id: Long)
GET 	/materials/view/custom/author/:id           controllers.Materials.materialsByAuthor(id: Long)
GET 	/materials/view/custom/owner/:id            controllers.Materials.materialsByOwner(id: Long)

POST 	/materials/remove/:id            controllers.Materials.remove(id: Long)


GET 	/materials/view/custom/byfield/:fieldName/:fieldId/:uid/pages/count            controllers.Materials.getMaterialsCustomByFieldPagesCount(uid: Long, fieldName: String, fieldId: Long)
GET 	/materials/view/custom/byfield/:fieldName/:fieldId/:uid/pages/page/:index      controllers.Materials.getMaterialsCustomByFieldPage(uid: Long, fieldName: String, fieldId: Long, index: Integer)

GET 	/materials/view/byclass/:className/user/:uid              controllers.Materials.materialsByClass(uid: Long, className: String)
GET 	/materials/view/byclass/:className/user/:uid/pages/count            controllers.Materials.getMaterialsByClassPagesCount(uid: Long, className: String)
GET 	/materials/view/byclass/:className/user/:uid/pages/page/:index      controllers.Materials.getMaterialsByClassPage(uid: Long, className: String, index: Integer)

GET 	/materials/publish/interview                controllers.Materials.publishInterview(uid: Long)
GET 	/materials/publish/knigoverth               controllers.Materials.publishKnigoverth(uid: Long)
GET 	/materials/publish/review                   controllers.Materials.publishReview(uid: Long)
GET 	/materials/publish/affiche                  controllers.Materials.publishAffiche(uid: Long)

POST 	/materials/publish/interview                controllers.Materials.processPublishInterview(uid: Long, mid: Long)
POST 	/materials/publish/knigoverth               controllers.Materials.processPublishKnigoverth(uid: Long, mid: Long)
POST 	/materials/publish/review                   controllers.Materials.processPublishReview(uid: Long, mid: Long)
POST 	/materials/publish/affiche                  controllers.Materials.processPublishAffiche(uid: Long, mid: Long)

#GET     /materials/all/:params/pages/page/:id       controllers.Materials.getMaterialsPage(params: String, id: Integer)
#GET     /materials/all/:params/pages/count          controllers.Materials.getMaterialsPagesCount(params: String)

GET	    /materials/material/:id                     controllers.Materials.viewMaterial(id : Long)

GET	    /materials/material/:id/editMaterialText/user/:uid  controllers.Materials.editMaterial(uid: Long, id: Long)
#POST	    /materials/material/:id/editMaterialText/user/:uid  controllers.Materials.processEditMaterialText(uid: Long, id: Long)

#POST	    /materials/material/:id/removeMaterial/user/:uid  controllers.Materials.removeMaterial(uid: Long, id: Long)
POST	    /materials/material/:id/setVisibleMaterial/user/:uid  controllers.Materials.setVisible(uid: Long, id: Long, isVisible: Boolean)



GET     /materials/material/:id/getIgoUsersCount    controllers.Materials.getIgoUsersCount(id: Long)
GET     /materials/material/:id/getIlikeUsersCount  controllers.Materials.getIlikeUsersCount(id: Long)

# Comments actions

GET     /materials/material/:matId/comments/pages/page/:pageId            controllers.Comments.getCommentsPage(uid :Long, matId: Long, pageId: Integer)
GET     /materials/material/:matId/comments/pages/count                   controllers.Comments.getCommentsPageSize(matId: Long)
POST 	/materials/material/:matId/comments/send                		  controllers.Comments.sendComment(matId: Long, cid: Long, content: String)

POST 	/comments/comment/:cid					                		  controllers.Comments.remove(cid: Long)

#####################################################


GET     /assets/javascripts/routes            controllers.Application.javascriptRoutes()

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file               controllers.Assets.at(path="/public", file)


#POST	/social/vk/auth/:uid	controllers.Social.socialAction(uid: Long)