package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Helper {

	public static Random random = new Random();

	public static String getDoubleMD5(String input) {
		return getMD5Hash(getMD5Hash(input.trim()));
	}

	public static String getMD5RandomString() {
		return getMD5Hash(generateRandomString());
	}

	public static String getMD5Hash(String input) {
		String hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			hash = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}

	public static String generateRandomString() {
		byte[] temp = new byte[30];
		random.nextBytes(temp);
		return new String(temp);
	}

	public static void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}

	public static String checkPassword(String password) {
		if(password == null || password.trim().length() < 8) {
			return "Пароль должен быть больше 8 символов";
		}
		return null;
	}
	
}
