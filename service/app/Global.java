
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import models.College;
import models.Genre;
import models.Param;
import models.SecurityRole;
import models.Status;
import models.User;
import play.Application;
import play.GlobalSettings;

import com.avaje.ebean.Ebean;


public class Global extends GlobalSettings {
	
	@Override
	public void onStart(Application application) {



//		if(Play.isDev())
//			Ebean.getServer(null).getAdminLogging().setDebugGeneratedSql(true);
		
		if (Param.find.findRowCount() == 0) {
			Param param = new Param();
			param.name = controllers.Application.PROJECT_NAME;
			param.descr = "Имя проекта, отображается на главной странице вверху";
			param.value = "inaword";
			param.save();
			
			param = new Param();
			param.name = controllers.Application.PRODUCT_IMAGES_FOLDER;
			param.descr = "Директория для расположения изображений.";
			param.value = "images/content/";
			param.save();
			
			File file = new File(param.value);
			if(!file.exists())
				file.mkdirs();
			
			param = new Param();
			param.name = controllers.Application.PICTURE_SIZE;
			param.descr = "Размер загружаемой картинки в байтах.";
			param.value = "300000";
			param.save();			
			
			param = new Param();
			param.name = controllers.Application.PAGES_SIZE;
			param.descr = "Имя проекта, отображается на главной странице вверху";
			param.value = "10";
			param.save();
		}
		
		if(SecurityRole.find.findRowCount() == 0) {
			SecurityRole.getAdminRole();
			SecurityRole.getEditorRole();
			SecurityRole.getUserRole();
		}
		
		if(Status.find.findRowCount() == 0) {
			Status.getProfessorStatus();
			Status.getStudentStatus();
			//Status.getUnknownStatus();
		}

		if(College.find.findRowCount() == 0) {
			College college;

			college = new College();
			college.setName("МГУ");
			college.save();

			college = new College();
			college.setName("АПИ при ИГиП РАН");
			college.save();

			college = new College();
			college.setName("Финансовый университет (бывш. ГУМФ)");
			college.save();

			college = new College();
			college.setName("АГПС МЧС России");
			college.save();

			college = new College();
			college.setName("АГЗ МЧС России");
			college.save();

			college = new College();
			college.setName("АМИ");
			college.save();

			college = new College();
			college.setName("МИГУП (бывш. АПУ)");
			college.save();

			college = new College();
			college.setName("АТиСО");
			college.save();

			college = new College();
			college.setName("АУ МВД РФ");
			college.save();

			college = new College();
			college.setName("АФПС РФ");
			college.save();

			college = new College();
			college.setName("АФСБ РФ");
			college.save();

			college = new College();
			college.setName("АХИ им. В. С. Попова");
			college.save();

			college = new College();
			college.setName("АЭИ");
			college.save();

			college = new College();
			college.setName("АЭиУ");
			college.save();

			college = new College();
			college.setName("АРБИ");
			college.save();

			college = new College();
			college.setName("ВА РВСН им. Петра Великого (бывш. ВА им. Дзержинского)");
			college.save();

			college = new College();
			college.setName("ВИУ МО РФ");
			college.save();

			college = new College();
			college.setName("ВВА им. Жуковского и Ю. А. Гагарина (бывш. ВВИА)");
			college.save();

			college = new College();
			college.setName("ВУ МО РФ (бывш. ВПА им. Ленина)");
			college.save();

			college = new College();
			college.setName("ВУРХБЗ");
			college.save();

			college = new College();
			college.setName("ВУ (восточный)");
			college.save();

			college = new College();
			college.setName("ВАВТ");
			college.save();

			college = new College();
			college.setName("Финансовый университет (бывш. ВГНА)");
			college.save();

			college = new College();
			college.setName("ВГИК");
			college.save();

			college = new College();
			college.setName("Финансовый университет при Правительстве РФ");
			college.save();

			college = new College();
			college.setName("ВКШ МЭРТ");
			college.save();

			college = new College();
			college.setName("ВТШ С. Мелконяна");
			college.save();

			college = new College();
			college.setName("ВШ ГМУ");
			college.save();

			college = new College();
			college.setName("ВШПП");
			college.save();

			college = new College();
			college.setName("ВШП");
			college.save();

			college = new College();
			college.setName("ВШСО");
			college.save();

			college = new College();
			college.setName("ВТУ им. Щепкина");
			college.save();

			college = new College();
			college.setName("ТИ им. Щукина (бывш. ВТУ им. Щукина)");
			college.save();

			college = new College();
			college.setName("ВИУ");
			college.save();

			college = new College();
			college.setName("ГАСИС");
			college.save();

			college = new College();
			college.setName("ГАСК");
			college.save();

			college = new College();
			college.setName("ГКА им. Маймонида");
			college.save();

			college = new College();
			college.setName("Гос. ИРЯ им. Пушкина");
			college.save();

			college = new College();
			college.setName("ГМПИ им. Ипполитова-Иванова");
			college.save();

			college = new College();
			college.setName("ГСИИ");
			college.save();

			college = new College();
			college.setName("НИУ ВШЭ (ГУ-ВШЭ)");
			college.save();

			college = new College();
			college.setName("ГАУГН (бывш. ГУГН)");
			college.save();

			college = new College();
			college.setName("ГУЗ");
			college.save();

			college = new College();
			college.setName("ГУУ");
			college.save();

			college = new College();
			college.setName("ГЭИ");
			college.save();

			college = new College();
			college.setName("ГЭИТИ");
			college.save();

			college = new College();
			college.setName("Гуманитарный институт");
			college.save();

			college = new College();
			college.setName("ГИТР");
			college.save();

			college = new College();
			college.setName("МГПИ (ныне МГПУ)");
			college.save();

			college = new College();
			college.setName("ДА МИД РФ");
			college.save();

			college = new College();
			college.setName("МОИУП");
			college.save();

			college = new College();
			college.setName("Юринфор");
			college.save();

			college = new College();
			college.setName("ИБП");
			college.save();

			college = new College();
			college.setName("ИБПМ");
			college.save();

			college = new College();
			college.setName("МОИУ (бывш. ИБПИТ)");
			college.save();

			college = new College();
			college.setName("ИБПУ");
			college.save();

			college = new College();
			college.setName("РГИС (ВСК)");
			college.save();

			college = new College();
			college.setName("ИГУ МГИМО МИД РФ");
			college.save();

			college = new College();
			college.setName("ИГА");
			college.save();

			college = new College();
			college.setName("ИГиМУ ГУУ");
			college.save();

			college = new College();
			college.setName("ИГУМО");
			college.save();

			college = new College();
			college.setName("ИДК");
			college.save();

			college = new College();
			college.setName("ИНЕСНЭК");
			college.save();

			college = new College();
			college.setName("ИЖЛТ");
			college.save();

			college = new College();
			college.setName("ИИТЭМ");
			college.save();

			college = new College();
			college.setName("ИИР");
			college.save();

			college = new College();
			college.setName("ИК");
			college.save();

			college = new College();
			college.setName("ИКиП");
			college.save();

			college = new College();
			college.setName("ИКСИ АФСБ РФ");
			college.save();

			college = new College();
			college.setName("ИМБО");
			college.save();

			college = new College();
			college.setName("ИМПЭ им. Грибоедова");
			college.save();

			college = new College();
			college.setName("ИМТП");
			college.save();

			college = new College();
			college.setName("ИМЭС");
			college.save();

			college = new College();
			college.setName("ИМПиПП");
			college.save();

			college = new College();
			college.setName("ИМЭ");
			college.save();

			college = new College();
			college.setName("ИМЭИ (мировой экономики и информатизации)");
			college.save();

			college = new College();
			college.setName("ИПВ");
			college.save();

			college = new College();
			college.setName("ИПО");
			college.save();

			college = new College();
			college.setName("ИПП");
			college.save();

			college = new College();
			college.setName("ИП им. Выготского РГГУ");
			college.save();

			college = new College();
			college.setName("ИРЭСПиП");
			college.save();

			college = new College();
			college.setName("ИСБ");
			college.save();

			college = new College();
			college.setName("ИСИ");
			college.save();

			college = new College();
			college.setName("ИСБТиСУ");
			college.save();

			college = new College();
			college.setName("ИСН");
			college.save();

			college = new College();
			college.setName("ИССО");
			college.save();

			college = new College();
			college.setName("ИНЭКБИ");
			college.save();

			college = new College();
			college.setName("ИЭиК");
			college.save();

			college = new College();
			college.setName("ИЭП");
			college.save();

			college = new College();
			college.setName("ИНЭП");
			college.save();

			college = new College();
			college.setName("ИЭКП РАМА");
			college.save();

			college = new College();
			college.setName("ИЭФП");
			college.save();

			college = new College();
			college.setName("ИЭПиУР");
			college.save();

			college = new College();
			college.setName("ИЯиК им. Л. Толстого");
			college.save();

			college = new College();
			college.setName("ЛИ им. Горького");
			college.save();

			college = new College();
			college.setName("МАТИ");
			college.save();

			college = new College();
			college.setName("МВШ");
			college.save();

			college = new College();
			college.setName("МИГМТ");
			college.save();

			college = new College();
			college.setName("МИМ Линк");
			college.save();

			college = new College();
			college.setName("МИЭП");
			college.save();

			college = new College();
			college.setName("МНЭПУ");
			college.save();

			college = new College();
			college.setName("МИЭПП при МНЭПУ");
			college.save();

			college = new College();
			college.setName("МСИ им. Державина");
			college.save();

			college = new College();
			college.setName("МУМ");
			college.save();

			college = new College();
			college.setName("МЮИ");
			college.save();

			college = new College();
			college.setName("НОУ МТИ");
			college.save();

			college = new College();
			college.setName("МАП (международная)");
			college.save();

			college = new College();
			college.setName("МГИИТ (бывш. МАТГРБ)");
			college.save();

			college = new College();
			college.setName("МАЭП");
			college.save();

			college = new College();
			college.setName("МГАВМиБ им. К.И. Скрябина (бывш. МВА)");
			college.save();

			college = new College();
			college.setName("МГАВТ");
			college.save();

			college = new College();
			college.setName("МГУПИ");
			college.save();

			college = new College();
			college.setName("МИТХТ");
			college.save();

			college = new College();
			college.setName("МГАФК (Мосспортакадемия)");
			college.save();

			college = new College();
			college.setName("МГАХ");
			college.save();

			college = new College();
			college.setName("МГК им. Чайковского");
			college.save();

			college = new College();
			college.setName("МГЮА им. О. Е. Кутафина");
			college.save();

			college = new College();
			college.setName("Первый МГМУ им. И.М. Сеченова (бывш. ММА им. Сеченова)");
			college.save();

			college = new College();
			college.setName("Мирбис");
			college.save();

			college = new College();
			college.setName("РГАУ-МСХА им. Тимирязева");
			college.save();

			college = new College();
			college.setName("МФЮА");
			college.save();

			college = new College();
			college.setName("МАИ");
			college.save();

			college = new College();
			college.setName("МАрхИ");
			college.save();

			college = new College();
			college.setName("МБИ");
			college.save();

			college = new College();
			college.setName("МВИРЭКВ");
			college.save();

			college = new College();
			college.setName("МГИЮ");
			college.save();

			college = new College();
			college.setName("МГПУ");
			college.save();

			college = new College();
			college.setName("МГППУ");
			college.save();

			college = new College();
			college.setName("МАДИ");
			college.save();

			college = new College();
			college.setName("МГАУ им. Горячкина");
			college.save();

			college = new College();
			college.setName("МГАХИ им. Сурикова");
			college.save();

			college = new College();
			college.setName("МГВМИ");
			college.save();

			college = new College();
			college.setName("РГГРУ (МГРИ) им. Орджоникидзе");
			college.save();

			college = new College();
			college.setName("МГГУ");
			college.save();

			college = new College();
			college.setName("МГИУ");
			college.save();

			college = new College();
			college.setName("МГИМО (У) МИД России");
			college.save();

			college = new College();
			college.setName("МГИМ им. Шнитке");
			college.save();

			college = new College();
			college.setName("МГТУ МИРЭА");
			college.save();

			college = new College();
			college.setName("НИТУ МИСиС");
			college.save();

			college = new College();
			college.setName("МИЭМ НИУ ВШЭ");
			college.save();

			college = new College();
			college.setName("МИЭТ");
			college.save();

			college = new College();
			college.setName("МГЛУ (бывш. МГПИИЯ им. Мориса Тореза)");
			college.save();

			college = new College();
			college.setName("МГМСУ им. А. И. Евдокимова");
			college.save();

			college = new College();
			college.setName("МГГУ (бывш. МГОПУ) им. Шолохова");
			college.save();

			college = new College();
			college.setName("МГОУ им. Черномырдина");
			college.save();

			college = new College();
			college.setName("МГСУ НИУ (МГСУ-МИСИ)");
			college.save();

			college = new College();
			college.setName("ТИ МГУДТ (бывш. МГТУ им. Косыгина)");
			college.save();

			college = new College();
			college.setName("МГТУГА");
			college.save();

			college = new College();
			college.setName("МГТУ им. Н.Э. Баумана");
			college.save();

			college = new College();
			college.setName("Университет машиностроения (МАМИ)");
			college.save();

			college = new College();
			college.setName("МГТУ Станкин");
			college.save();

			college = new College();
			college.setName("МИИГАиК");
			college.save();

			college = new College();
			college.setName("МГУДТ");
			college.save();

			college = new College();
			college.setName("МГУИЭ (бывш. МИХМ)");
			college.save();

			college = new College();
			college.setName("МГУКИ");
			college.save();

			college = new College();
			college.setName("МГУЛ");
			college.save();

			college = new College();
			college.setName("МГУПП");
			college.save();

			college = new College();
			college.setName("МГУПБ");
			college.save();

			college = new College();
			college.setName("МГУП (природообустройства)");
			college.save();

			college = new College();
			college.setName("МИИТ");
			college.save();

			college = new College();
			college.setName("МГУС / РГУТиС (бывш. МТИ)");
			college.save();

			college = new College();
			college.setName("МЭСИ");
			college.save();

			college = new College();
			college.setName("МГХПА им. Строганова");
			college.save();

			college = new College();
			college.setName("МГЭИ");
			college.save();

			college = new College();
			college.setName("МГИ им. Дашковой");
			college.save();

			college = new College();
			college.setName("НИЯУ МИФИ");
			college.save();

			college = new College();
			college.setName("МИБД");
			college.save();

			college = new College();
			college.setName("МИБУА");
			college.save();

			college = new College();
			college.setName("МИИЯ");
			college.save();

			college = new College();
			college.setName("МГАКХиС (ВЗИСИ)");
			college.save();

			college = new College();
			college.setName("МИМСР");
			college.save();

			college = new College();
			college.setName("МИМБ при ВАВТ МЭРТ РФ");
			college.save();

			college = new College();
			college.setName("МИП");
			college.save();

			college = new College();
			college.setName("МИПП");
			college.save();

			college = new College();
			college.setName("МИГКУ");
			college.save();

			college = new College();
			college.setName("МИУ (управления)");
			college.save();

			college = new College();
			college.setName("МУ им. С.Ю. Витте (бывш. МИЭМП)");
			college.save();

			college = new College();
			college.setName("МИЭПП");
			college.save();

			college = new College();
			college.setName("МКВИ филиал СПБГУКиТ");
			college.save();

			college = new College();
			college.setName("МНЮИ");
			college.save();

			college = new College();
			college.setName("МОСА (бывш. МОСУ)");
			college.save();

			college = new College();
			college.setName("МПГУ (бывш. МГПИ им. Ленина)");
			college.save();

			college = new College();
			college.setName("МПУ");
			college.save();

			college = new College();
			college.setName("МПСУ (бывш. МПСИ)");
			college.save();

			college = new College();
			college.setName("МРИВСЭО");
			college.save();

			college = new College();
			college.setName("МТУСИ");
			college.save();

			college = new College();
			college.setName("МТИ (транспортный)");
			college.save();

			college = new College();
			college.setName("МосУ МВД РФ");
			college.save();

			college = new College();
			college.setName("МУ Туро");
			college.save();

			college = new College();
			college.setName("МФТИ (ГУ)");
			college.save();

			college = new College();
			college.setName("СПбГУП (МФ)");
			college.save();

			college = new College();
			college.setName("СПбИВЭСЭП в Москве");
			college.save();

			college = new College();
			college.setName("МФЭИ");
			college.save();

			college = new College();
			college.setName("МГТА (бывш. МЭЛИ)");
			college.save();

			college = new College();
			college.setName("МЭФИ");
			college.save();

			college = new College();
			college.setName("НОУ МЭИ");
			college.save();

			college = new College();
			college.setName("НИУ МЭИ (МЭИ ТУ)");
			college.save();

			college = new College();
			college.setName("МАХУ памяти 1905 года");
			college.save();

			college = new College();
			college.setName("ЛГУ им. Пушкина в Москве");
			college.save();

			college = new College();
			college.setName("НИБ");
			college.save();

			college = new College();
			college.setName("ИППиП");
			college.save();

			college = new College();
			college.setName("Континент &lt;Евро-Азия&gt;");
			college.save();

			college = new College();
			college.setName("ПМЮИ");
			college.save();

			college = new College();
			college.setName("ИТТ (подмосковный филиал)");
			college.save();

			college = new College();
			college.setName("РАА им. Плевако");
			college.save();

			college = new College();
			college.setName("РАЖВЗ");
			college.save();

			college = new College();
			college.setName("РАМиА");
			college.save();

			college = new College();
			college.setName("РАМ им. Гнесиных");
			college.save();

			college = new College();
			college.setName("РАП (правосудия)");
			college.save();

			college = new College();
			college.setName("РАП (предпринимательства)");
			college.save();

			college = new College();
			college.setName("ГИТИС");
			college.save();

			college = new College();
			college.setName("РГАТиЗ");
			college.save();

			college = new College();
			college.setName("РГУФКСМиТ (ГЦОЛИФК)");
			college.save();

			college = new College();
			college.setName("РМАПО");
			college.save();

			college = new College();
			college.setName("РМАТ");
			college.save();

			college = new College();
			college.setName("РПА МЮ РФ");
			college.save();

			college = new College();
			college.setName("РТА");
			college.save();

			college = new College();
			college.setName("РЭУ им. Г. В. Плеханова");
			college.save();

			college = new College();
			college.setName("РГАЗУ");
			college.save();

			college = new College();
			college.setName("РГГУ");
			college.save();

			college = new College();
			college.setName("РГАИС (бывш. РГИИС)");
			college.save();

			college = new College();
			college.setName("РНИМУ (бывш. РГМУ) им. Н.И. Пирогова");
			college.save();

			college = new College();
			college.setName("РГТЭУ");
			college.save();

			college = new College();
			college.setName("РГУИТП");
			college.save();

			college = new College();
			college.setName("РГУНГ им. Губкина");
			college.save();

			college = new College();
			college.setName("ИТЛП МГУТУ (бывш. РосЗИТЛП)");
			college.save();

			college = new College();
			college.setName("РосНОУ");
			college.save();

			college = new College();
			college.setName("РПИ Св. Иоанна Богослова");
			college.save();

			college = new College();
			college.setName("РУДН");
			college.save();

			college = new College();
			college.setName("РХТУ им. Менделеева");
			college.save();

			college = new College();
			college.setName("РИУ");
			college.save();

			college = new College();
			college.setName("УГМУ (бывш. РУИ)");
			college.save();

			college = new College();
			college.setName("СДИ");
			college.save();

			college = new College();
			college.setName("СГА");
			college.save();

			college = new College();
			college.setName("СФГА (НОУ СГИ)");
			college.save();

			college = new College();
			college.setName("УНИК");
			college.save();

			college = new College();
			college.setName("МАО (УНН - Натальи Нестеровой)");
			college.save();

			college = new College();
			college.setName("УРАО");
			college.save();

			college = new College();
			college.setName("Финансовый университет (ФА)  при Правительстве РФ");
			college.save();

			college = new College();
			college.setName("Школа-студия МХАТ");
			college.save();

			college = new College();
			college.setName("НОУ ЮА");
			college.save();

			college = new College();
			college.setName("МИТРО");
			college.save();

			college = new College();
			college.setName("РГСУ");
			college.save();

			college = new College();
			college.setName("МГОУ (областной)");
			college.save();

			college = new College();
			college.setName("МИФКиС");
			college.save();

			college = new College();
			college.setName("МАБиУ");
			college.save();

			college = new College();
			college.setName("МИР");
			college.save();

			college = new College();
			college.setName("МосГУ");
			college.save();

			college = new College();
			college.setName("МЭПИ");
			college.save();

			college = new College();
			college.setName("МГГЭИ (бывш. МГСГИ)");
			college.save();

			college = new College();
			college.setName("МИЮ");
			college.save();

			college = new College();
			college.setName("МВШМ МВД СССР (до 1992г.)");
			college.save();

			college = new College();
			college.setName("ИМДТ");
			college.save();

			college = new College();
			college.setName("РАХИ");
			college.save();

			college = new College();
			college.setName("МХПИ");
			college.save();

			college = new College();
			college.setName("ИТиГ");
			college.save();

			college = new College();
			college.setName("РУК (бывш. МУПК)");
			college.save();

			college = new College();
			college.setName("МГУУ Правительства Москвы");
			college.save();

			college = new College();
			college.setName("МГИМ");
			college.save();

			college = new College();
			college.setName("ИЭАУ");
			college.save();

			college = new College();
			college.setName("МСЭИ");
			college.save();

			college = new College();
			college.setName("ИНЭС");
			college.save();

			college = new College();
			college.setName("НИ ВШУ");
			college.save();

			college = new College();
			college.setName("МИМЭМО (бывш. МИМЭО)");
			college.save();

			college = new College();
			college.setName("МИЛ");
			college.save();

			college = new College();
			college.setName("ВШК");
			college.save();

			college = new College();
			college.setName("МоСПИ при МПГУ");
			college.save();

			college = new College();
			college.setName("ПСТГУ");
			college.save();

			college = new College();
			college.setName("МТИ «ВТУ» (бывш. ВТУ ЮНЕСКО)");
			college.save();

			college = new College();
			college.setName("НИЕВ");
			college.save();

			college = new College();
			college.setName("АЭБ МВД РФ (бывш. АНП ФСНП РФ)");
			college.save();

			college = new College();
			college.setName("РИПТИБ (ИМЦ)");
			college.save();

			college = new College();
			college.setName("Институт бизнеса и дизайна (бывш. ИнОБО)");
			college.save();

			college = new College();
			college.setName("Coll&#232;ge Universitaire Fran&#231;ais de Moscou");
			college.save();

			college = new College();
			college.setName("БВШД");
			college.save();

			college = new College();
			college.setName("ИМСГС");
			college.save();

			college = new College();
			college.setName("РЭШ");
			college.save();

			college = new College();
			college.setName("НИД");
			college.save();

			college = new College();
			college.setName("МГЛИ");
			college.save();

			college = new College();
			college.setName("СЭИ");
			college.save();

			college = new College();
			college.setName("МИНРО");
			college.save();

			college = new College();
			college.setName("УМЛ при МГК КПСС");
			college.save();

			college = new College();
			college.setName("НИМ");
			college.save();

			college = new College();
			college.setName("ББИ");
			college.save();

			college = new College();
			college.setName("ОИТиР");
			college.save();

			college = new College();
			college.setName("ИГТМ (бывш. ИАЯ)");
			college.save();

			college = new College();
			college.setName("МИРТШБ");
			college.save();

			college = new College();
			college.setName("МосАП при Правительстве Москвы");
			college.save();

			college = new College();
			college.setName("ВИА им. Куйбышева");
			college.save();

			college = new College();
			college.setName("ИПК");
			college.save();

			college = new College();
			college.setName("ИИМ");
			college.save();

			college = new College();
			college.setName("МНИ");
			college.save();

			college = new College();
			college.setName("СИЮ");
			college.save();

			college = new College();
			college.setName("ИОСО РАО");
			college.save();

			college = new College();
			college.setName("МИ 21");
			college.save();

			college = new College();
			college.setName("МАМАРМЕН");
			college.save();

			college = new College();
			college.setName("ИМЛИ им. Горького");
			college.save();

			college = new College();
			college.setName("МАОК");
			college.save();

			college = new College();
			college.setName("ИМЭМО РАН");
			college.save();

			college = new College();
			college.setName("ВА БТВ");
			college.save();

			college = new College();
			college.setName("ГПИ");
			college.save();

			college = new College();
			college.setName("ПИУ");
			college.save();

			college = new College();
			college.setName("АУМП");
			college.save();

			college = new College();
			college.setName("МПИ ФСБ РФ (бывш. МВИ ФПС РФ)");
			college.save();

			college = new College();
			college.setName("АСОУ");
			college.save();

			college = new College();
			college.setName("МВШБ");
			college.save();

			college = new College();
			college.setName("SSE Russia");
			college.save();

			college = new College();
			college.setName("УСНГ");
			college.save();

			college = new College();
			college.setName("ИЭФ Синергия");
			college.save();

			college = new College();
			college.setName("МИАПП (ИАПП)");
			college.save();

			college = new College();
			college.setName("МВШСЭН");
			college.save();

			college = new College();
			college.setName("ИИС");
			college.save();

			college = new College();
			college.setName("АБиП");
			college.save();

			college = new College();
			college.setName("МИУС");
			college.save();

			college = new College();
			college.setName("НМУ");
			college.save();

			college = new College();
			college.setName("МИП (психоанализа)");
			college.save();

			college = new College();
			college.setName("ИУиИ");
			college.save();

			college = new College();
			college.setName("МЭИ (экономический)");
			college.save();

			college = new College();
			college.setName("МГЮИ");
			college.save();

			college = new College();
			college.setName("РШЧП");
			college.save();

			college = new College();
			college.setName("ИАМТ");
			college.save();

			college = new College();
			college.setName("МОВУКИ");
			college.save();

			college = new College();
			college.setName("УИИТ");
			college.save();

			college = new College();
			college.setName("ВИППКРК");
			college.save();

			college = new College();
			college.setName("МИМО");
			college.save();

			college = new College();
			college.setName("ИФТИ св. Фомы");
			college.save();

			college = new College();
			college.setName("ИРТ");
			college.save();

			college = new College();
			college.setName("ИПИУ");
			college.save();

			college = new College();
			college.setName("ИСОМ");
			college.save();

			college = new College();
			college.setName("СФИ");
			college.save();

			college = new College();
			college.setName("ВАШГД");
			college.save();

			college = new College();
			college.setName("ИГУПИТ");
			college.save();

			college = new College();
			college.setName("ИТЭФ");
			college.save();

			college = new College();
			college.setName("ЭЭИ");
			college.save();

			college = new College();
			college.setName("СГЭИ");
			college.save();

			college = new College();
			college.setName("ИСПЭ");
			college.save();

			college = new College();
			college.setName("МАГМУ");
			college.save();

			college = new College();
			college.setName("ИРДПО");
			college.save();

			college = new College();
			college.setName("ФПИ");
			college.save();

			college = new College();
			college.setName("ПИ РАО");
			college.save();

			college = new College();
			college.setName("ПИЮ");
			college.save();

			college = new College();
			college.setName("ПИП");
			college.save();

			college = new College();
			college.setName("ИСЭ");
			college.save();

			college = new College();
			college.setName("МАБ");
			college.save();

			college = new College();
			college.setName("ИФЭП ОЗ");
			college.save();

			college = new College();
			college.setName("МИКБ (бывш. МАКБ)");
			college.save();

			college = new College();
			college.setName("МСГИ");
			college.save();

			college = new College();
			college.setName("ИКТ");
			college.save();

			college = new College();
			college.setName("МВШЭ ФИ");
			college.save();

			college = new College();
			college.setName("СИП");
			college.save();

			college = new College();
			college.setName("ИЭУП");
			college.save();

			college = new College();
			college.setName("МЭГУ");
			college.save();

			college = new College();
			college.setName("ИПЭ");
			college.save();

			college = new College();
			college.setName("ИКМ");
			college.save();

			college = new College();
			college.setName("ЕУП");
			college.save();

			college = new College();
			college.setName("ВАГШ ВС РФ");
			college.save();

			college = new College();
			college.setName("ОВА ВС РФ (бывш. ВА им. Фрунзе)");
			college.save();

			college = new College();
			college.setName("ИНиСБ");
			college.save();

			college = new College();
			college.setName("МАРТИТ");
			college.save();

			college = new College();
			college.setName("ИУП");
			college.save();

			college = new College();
			college.setName("ИУЭПИ");
			college.save();

			college = new College();
			college.setName("ВГШ им. Дубнова (бывш. ЕУМ)");
			college.save();

			college = new College();
			college.setName("НИСД");
			college.save();

			college = new College();
			college.setName("ЭГИ");
			college.save();

			college = new College();
			college.setName("ИЭ РАН");
			college.save();

			college = new College();
			college.setName("ИЗиСП при Правительстве РФ");
			college.save();

			college = new College();
			college.setName("ИО РАН");
			college.save();

			college = new College();
			college.setName("МИАН");
			college.save();

			college = new College();
			college.setName("ИГиП РАН");
			college.save();

			college = new College();
			college.setName("МИУ (исламский)");
			college.save();

			college = new College();
			college.setName("СИИЯ");
			college.save();

			college = new College();
			college.setName("ИОН (бывш. АНО)");
			college.save();

			college = new College();
			college.setName("ИЕ РАН");
			college.save();

			college = new College();
			college.setName("НИИ СП РФ");
			college.save();

			college = new College();
			college.setName("МИОО (бывш. МИПКРО)");
			college.save();

			college = new College();
			college.setName("МЮИ МВД РФ");
			college.save();

			college = new College();
			college.setName("ИСК РАН");
			college.save();

			college = new College();
			college.setName("ГАСиЖКК");
			college.save();

			college = new College();
			college.setName("МОЮИ при ПЦПИ Минюста РФ (бывш. ИЭФП)");
			college.save();

			college = new College();
			college.setName("ВКСР");
			college.save();

			college = new College();
			college.setName("ИПУ РАН");
			college.save();

			college = new College();
			college.setName("МТИ (теологический)");
			college.save();

			college = new College();
			college.setName("ИТИРШБ");
			college.save();

			college = new College();
			college.setName("ИСПТ");
			college.save();

			college = new College();
			college.setName("АПК и ППРО");
			college.save();

			college = new College();
			college.setName("ГНИИ РНС ФНС РФ");
			college.save();

			college = new College();
			college.setName("МИПК МГТУ им. Баумана");
			college.save();

			college = new College();
			college.setName("ИРИ РАН");
			college.save();

			college = new College();
			college.setName("ИУВ НМХЦ им. Пирогова");
			college.save();

			college = new College();
			college.setName("ВШИИ");
			college.save();

			college = new College();
			college.setName("МБШ");
			college.save();

			college = new College();
			college.setName("ИПБ России");
			college.save();

			college = new College();
			college.setName("ЦЭПЛ РАН");
			college.save();

			college = new College();
			college.setName("ГИИ РАН");
			college.save();

			college = new College();
			college.setName("ИС РАН");
			college.save();

			college = new College();
			college.setName("ИЯ РАН");
			college.save();

			college = new College();
			college.setName("РИСК (RISC)");
			college.save();

			college = new College();
			college.setName("ИМЕТ РАН");
			college.save();

			college = new College();
			college.setName("ЕАОИ");
			college.save();

			college = new College();
			college.setName("ИСПИ РАН");
			college.save();

			college = new College();
			college.setName("ЗНУИ");
			college.save();

			college = new College();
			college.setName("ИЭПП");
			college.save();

			college = new College();
			college.setName("ИДНТ");
			college.save();

			college = new College();
			college.setName("ИЕК");
			college.save();

			college = new College();
			college.setName("IIC");
			college.save();

			college = new College();
			college.setName("ВШДСИ при РАТИ (ГИТИС)");
			college.save();

			college = new College();
			college.setName("МСДС");
			college.save();

			college = new College();
			college.setName("ТАЕХ");
			college.save();

			college = new College();
			college.setName("ВИЭСХ");
			college.save();

			college = new College();
			college.setName("ИНЭОС РАН");
			college.save();

			college = new College();
			college.setName("ИЛА РАН");
			college.save();

			college = new College();
			college.setName("ГИУВ МО РФ");
			college.save();

			college = new College();
			college.setName("ИПУ Аскери");
			college.save();

			college = new College();
			college.setName("РИК РАН");
			college.save();

			college = new College();
			college.setName("МИЭЭ");
			college.save();

			college = new College();
			college.setName("ФИРО");
			college.save();

			college = new College();
			college.setName("ИПМех РАН");
			college.save();

			college = new College();
			college.setName("ВНИИВСГЭ");
			college.save();

			college = new College();
			college.setName("ИХО РАО");
			college.save();

			college = new College();
			college.setName("ИТТИ");
			college.save();

			college = new College();
			college.setName("ИОФ РАН");
			college.save();

			college = new College();
			college.setName("ВСТИСП");
			college.save();

			college = new College();
			college.setName("ИИП");
			college.save();

			college = new College();
			college.setName("ИПД МФПА");
			college.save();

			college = new College();
			college.setName("ВВИ (бывш. ВВФ при МВА им. Скрябина)");
			college.save();

			college = new College();
			college.setName("ФИАН им. Лебедева");
			college.save();

			college = new College();
			college.setName("РЗТИ");
			college.save();

			college = new College();
			college.setName("МИПК РЭУ им. Г. В. Плеханова");
			college.save();

			college = new College();
			college.setName("ЭПИ");
			college.save();

			college = new College();
			college.setName("ИХ им. Вишневского РАМН");
			college.save();

			college = new College();
			college.setName("СИУС");
			college.save();

			college = new College();
			college.setName("ВКИЯ МЭРТ");
			college.save();

			college = new College();
			college.setName("ИП РАН");
			college.save();

			college = new College();
			college.setName("МУГУ");
			college.save();

			college = new College();
			college.setName("ИОХ РАН");
			college.save();

			college = new College();
			college.setName("МИПК РРиСХП");
			college.save();

			college = new College();
			college.setName("ИНП РАН");
			college.save();

			college = new College();
			college.setName("ЦЭМИ РАН");
			college.save();

			college = new College();
			college.setName("ИЭА РАН им. Н. Н. Миклухо-Маклая");
			college.save();

			college = new College();
			college.setName("МНИОИ им. П. А. Герцена");
			college.save();

			college = new College();
			college.setName("ИИО РАО");
			college.save();

			college = new College();
			college.setName("ИПМ им. М. В. Келдыша РАН");
			college.save();

			college = new College();
			college.setName("ИАф РАН");
			college.save();

			college = new College();
			college.setName("АГПРФ");
			college.save();

			college = new College();
			college.setName("ИМБП РАН");
			college.save();

			college = new College();
			college.setName("ИРЭИ");
			college.save();

			college = new College();
			college.setName("УКУ");
			college.save();

			college = new College();
			college.setName("ИГ РАН");
			college.save();

			college = new College();
			college.setName("ИПНГ РАН");
			college.save();

			college = new College();
			college.setName("ИСЖ");
			college.save();

			college = new College();
			college.setName("ИВИ РАН");
			college.save();

			college = new College();
			college.setName("ИИСТ");
			college.save();

			college = new College();
			college.setName("ИТИ им. П. М. Ершова");
			college.save();

			college = new College();
			college.setName("ИВ РАН");
			college.save();

			college = new College();
			college.setName("АСЭР (Академия &quot;Континент&quot;)");
			college.save();

			college = new College();
			college.setName("МУИЦ");
			college.save();

			college = new College();
			college.setName("НИИ  РАХ");
			college.save();

			college = new College();
			college.setName("ИХФ РАН");
			college.save();

			college = new College();
			college.setName("МИГИП");
			college.save();

			college = new College();
			college.setName("ИАМ");
			college.save();

			college = new College();
			college.setName("ПГИ &quot;Со-действие&quot;");
			college.save();

			college = new College();
			college.setName("ИНТУИТ");
			college.save();

			college = new College();
			college.setName("МГИ");
			college.save();

			college = new College();
			college.setName("ИПИ");
			college.save();

			college = new College();
			college.setName("ИБХ РАН");
			college.save();

			college = new College();
			college.setName("МВУ (Венский)");
			college.save();

			college = new College();
			college.setName("ВЦ РАН");
			college.save();

			college = new College();
			college.setName("ВНИИФК");
			college.save();

			college = new College();
			college.setName("AIBEc");
			college.save();

			college = new College();
			college.setName("ИСИН");
			college.save();

			college = new College();
			college.setName("ВНШТ");
			college.save();

			college = new College();
			college.setName("НИИОСП им. М. Н. Герсеванова");
			college.save();

			college = new College();
			college.setName("ИСл РАН");
			college.save();

			college = new College();
			college.setName("ИФ РАН");
			college.save();

			college = new College();
			college.setName("ЕБС");
			college.save();

			college = new College();
			college.setName("ИПИ РАН");
			college.save();

			college = new College();
			college.setName("ГОСНИТИ");
			college.save();

			college = new College();
			college.setName("НОУ АРТ");
			college.save();

			college = new College();
			college.setName("ИНФО - Рутения");
			college.save();

			college = new College();
			college.setName("ГИА (GIA)");
			college.save();

			college = new College();
			college.setName("ИДВ РАН");
			college.save();

			college = new College();
			college.setName("ИБГ РАН");
			college.save();

			college = new College();
			college.setName("ИПЭБ");
			college.save();

			college = new College();
			college.setName("МАБТ &quot;Форекс Клуб&quot;");
			college.save();

			college = new College();
			college.setName("ИНЭУМ им. И. С. Брука");
			college.save();

			college = new College();
			college.setName("АИС");
			college.save();

			college = new College();
			college.setName("ИФЗ РАН");
			college.save();

			college = new College();
			college.setName("НИСК");
			college.save();

			college = new College();
			college.setName("ИСВ РАО (бывш. НИИ ОПВ АПН СССР)");
			college.save();

			college = new College();
			college.setName("МАН Сан-Марино");
			college.save();

			college = new College();
			college.setName("ИМАШ РАН им. А. А. Благонравова");
			college.save();

			college = new College();
			college.setName("МФПИ");
			college.save();

			college = new College();
			college.setName("ИПиКП");
			college.save();

			college = new College();
			college.setName("ИКСР");
			college.save();

			college = new College();
			college.setName("НИИ СПиПРЛ");
			college.save();

			college = new College();
			college.setName("ИХТ АИИ");
			college.save();

			college = new College();
			college.setName("ВГКПИ");
			college.save();

			college = new College();
			college.setName("РБУ");
			college.save();

			college = new College();
			college.setName("ИТПИ");
			college.save();

			college = new College();
			college.setName("ИММ РАН");
			college.save();

			college = new College();
			college.setName("МАФСИ");
			college.save();

			college = new College();
			college.setName("НИФХИ");
			college.save();

			college = new College();
			college.setName("ИИЯМС");
			college.save();

			college = new College();
			college.setName("ИАТУ");
			college.save();

			college = new College();
			college.setName("АПРИКТ");
			college.save();

			college = new College();
			college.setName("ИКИ РАН");
			college.save();

			college = new College();
			college.setName("Кора Москва");
			college.save();

			college = new College();
			college.setName("ИПКИР");
			college.save();

			college = new College();
			college.setName("ИППК &quot;Профессионал&quot;");
			college.save();

			college = new College();
			college.setName("ЦНИИТМАШ");
			college.save();

			college = new College();
			college.setName("ИНХС РАН");
			college.save();

			college = new College();
			college.setName("ППДС");
			college.save();

			college = new College();
			college.setName("НИКФИ");
			college.save();

			college = new College();
			college.setName("ИЭСО");
			college.save();

			college = new College();
			college.setName("ИСП РАН");
			college.save();

			college = new College();
			college.setName("МПДАиС");
			college.save();

			college = new College();
			college.setName("ИОНХ РАН");
			college.save();

			college = new College();
			college.setName("ИВП РАН");
			college.save();

			college = new College();
			college.setName("ИСМО РАО");
			college.save();

			college = new College();
			college.setName("ИВМ РАН");
			college.save();

			college = new College();
			college.setName("ИФР РАН");
			college.save();

			college = new College();
			college.setName("ИА РАН");
			college.save();

			college = new College();
			college.setName("ИПКОН РАН");
			college.save();

			college = new College();
			college.setName("РИСИ");
			college.save();

			college = new College();
			college.setName("ИНИОН РАН");
			college.save();

			college = new College();
			college.setName("АСМС");
			college.save();

			college = new College();
			college.setName("РНЦ КИ");
			college.save();

			college = new College();
			college.setName("УМЦ ЖДТ");
			college.save();

			college = new College();
			college.setName("Общецерковная аспирантура им. Кирилла и Мефодия");
			college.save();

			college = new College();
			college.setName("МШУ Сколково");
			college.save();

			college = new College();
			college.setName("МТС");
			college.save();

			college = new College();
			college.setName("МЕИЭФП");
			college.save();

			college = new College();
			college.setName("ИОГен РАН");
			college.save();

			college = new College();
			college.setName("ИНБИ РАН");
			college.save();

			college = new College();
			college.setName("ВИАПИ им. А. А. Никонова");
			college.save();

			college = new College();
			college.setName("ГУ ИМЭИ");
			college.save();

			college = new College();
			college.setName("РТИ им. А. Л. Минца");
			college.save();

			college = new College();
			college.setName("СОПС РАН");
			college.save();

			college = new College();
			college.setName("МИТП РАН");
			college.save();

			college = new College();
			college.setName("ВНИИгеосистем");
			college.save();

			college = new College();
			college.setName("МАСИ");
			college.save();

			college = new College();
			college.setName("ИПТиК");
			college.save();

			college = new College();
			college.setName("ВИНИТИ РАН");
			college.save();

			college = new College();
			college.setName("РАПС (бывш. ИПК МПС России)");
			college.save();

			college = new College();
			college.setName("ИВФ РАО");
			college.save();

			college = new College();
			college.setName("ИРЭ им. В.А. Котельникова РАН");
			college.save();

			college = new College();
			college.setName("НИИ ФХМ Росздрава");
			college.save();

			college = new College();
			college.setName("НИИ ОПП РАМН");
			college.save();

			college = new College();
			college.setName("ЦНИИС");
			college.save();

			college = new College();
			college.setName("ИПКгосслужбы");
			college.save();

			college = new College();
			college.setName("ИФБТ");
			college.save();

			college = new College();
			college.setName("ВИГИС");
			college.save();

			college = new College();
			college.setName("ПАПО");
			college.save();

			college = new College();
			college.setName("ИСОУКиТ");
			college.save();

			college = new College();
			college.setName("ВНИИДАД");
			college.save();

			college = new College();
			college.setName("ИПК ДСЗН");
			college.save();

			college = new College();
			college.setName("ИСП РАО");
			college.save();

			college = new College();
			college.setName("ГКНПЦ им. М. В. Хруничева");
			college.save();

			college = new College();
			college.setName("Академия АйТи");
			college.save();

			college = new College();
			college.setName("IUPFAN");
			college.save();

			college = new College();
			college.setName("Centro Gregorio Fern&#225;ndez");
			college.save();

			college = new College();
			college.setName("АРМО");
			college.save();

			college = new College();
			college.setName("ИИСУ");
			college.save();

			college = new College();
			college.setName("МГААИИ С. Андрияки");
			college.save();

			college = new College();
			college.setName("ИФХЭ РАН");
			college.save();

			college = new College();
			college.setName("МИСБ");
			college.save();

			college = new College();
			college.setName("МАА");
			college.save();

			college = new College();
			college.setName("ВНИИНТПИ");
			college.save();

			college = new College();
			college.setName("ИСО РАО");
			college.save();

			college = new College();
			college.setName("ИМАД Стрелка");
			college.save();

			college = new College();
			college.setName("ИПГ");
			college.save();

			college = new College();
			college.setName("ИИППР");
			college.save();

			college = new College();
			college.setName("ИППД РАО");
			college.save();

			college = new College();
			college.setName("Университет рабочих корреспондентов им. Ульяновой");
			college.save();

			college = new College();
			college.setName("ВНИИЖТ");
			college.save();

			college = new College();
			college.setName("АБиСП");
			college.save();

			college = new College();
			college.setName("МКТА");
			college.save();

			college = new College();
			college.setName("ИЭУСП");
			college.save();

			college = new College();
			college.setName("БОН");
			college.save();

			college = new College();
			college.setName("ИПК ФМБА России");
			college.save();

			college = new College();
			college.setName("САМБ");
			college.save();

			college = new College();
			college.setName("ИСА РАН");
			college.save();

			college = new College();
			college.setName("ВНИИМС");
			college.save();

			college = new College();
			college.setName("ШАД");
			college.save();

			college = new College();
			college.setName("ЦНИИПСК им. Мельникова");
			college.save();

			college = new College();
			college.setName("ИНО");
			college.save();

			college = new College();
			college.setName("ИРЯ РАН");
			college.save();

			college = new College();
			college.setName("НИИ Вирусологии  РАМН");
			college.save();

			college = new College();
			college.setName("ИБХФ РАН");
			college.save();

			college = new College();
			college.setName("ОТИ");
			college.save();

			college = new College();
			college.setName("ЭЮИ");
			college.save();

			college = new College();
			college.setName("НИИ ТСС");
			college.save();

			college = new College();
			college.setName("ГосНИИгенетика");
			college.save();

			college = new College();
			college.setName("ИМБ РАН");
			college.save();

			college = new College();
			college.setName("ИЕГО");
			college.save();

			college = new College();
			college.setName("ИРЕА");
			college.save();

			college = new College();
			college.setName("ИГиСП");
			college.save();

			college = new College();
			college.setName("ИДМ");
			college.save();

			college = new College();
			college.setName("ИЭСБ");
			college.save();

			college = new College();
			college.setName("МИСАО");
			college.save();

			college = new College();
			college.setName("НЦССХ им. А. Н. Бакулева РАМН");
			college.save();

			college = new College();
			college.setName("МИГТиК");
			college.save();

			college = new College();
			college.setName("ВНИИПВТИ");
			college.save();

			college = new College();
			college.setName("ВДА");
			college.save();

			college = new College();
			college.setName("ИУ (МскУниверситет)");
			college.save();

			college = new College();
			college.setName("РАКО АПК");
			college.save();

			college = new College();
			college.setName("ЮИ (МскУниверситет)");
			college.save();

			college = new College();
			college.setName("МОСГИ");
			college.save();

			college = new College();
			college.setName("МГИМТ");
			college.save();

			college = new College();
			college.setName("ПУ Первое сентября");
			college.save();

			college = new College();
			college.setName("Touro College - Lander Institute Moscow");
			college.save();

			college = new College();
			college.setName("ИУБ");
			college.save();

			college = new College();
			college.setName("АМИР");
			college.save();

			college = new College();
			college.setName("НИИРПО");
			college.save();

			college = new College();
			college.setName("ИТ (МскУниверситет)");
			college.save();

			college = new College();
			college.setName("ИЭ (МскУниверситет)");
			college.save();

			college = new College();
			college.setName("ИП (МскУниверситет)");
			college.save();

			college = new College();
			college.setName("МОЮИ");
			college.save();

			college = new College();
			college.setName("ЦНИИчермет им. И.П. Бардина");
			college.save();

			college = new College();
			college.setName("ИПСУ");
			college.save();

			college = new College();
			college.setName("ГЕОХИ РАН");
			college.save();

			college = new College();
			college.setName("25 ГосНИИ химмотологии МО РФ");
			college.save();

			college = new College();
			college.setName("ВИЭВ");
			college.save();

			college = new College();
			college.setName("МОНИКИ им. М.Ф.Владимирского");
			college.save();

			college = new College();
			college.setName("ИТИП РАО");
			college.save();

			college = new College();
			college.setName("ЭНЦ Минздравсоцразвития РФ");
			college.save();

			college = new College();
			college.setName("НАМИ");
			college.save();

			college = new College();
			college.setName("Академия ГРУ");
			college.save();

			college = new College();
			college.setName("НИИ СП им. Склифосовского");
			college.save();

			college = new College();
			college.setName("ИАБиА");
			college.save();

			college = new College();
			college.setName("МАРШ");
			college.save();

			college = new College();
			college.setName("ИНАСАН");
			college.save();

			college = new College();
			college.setName("ГИНФО");
			college.save();

			college = new College();
			college.setName("МИКТиР");
			college.save();

			college = new College();
			college.setName("МИНЭКБИК");
			college.save();

			college = new College();
			college.setName("СГЛА");
			college.save();

			college = new College();
			college.setName("ИППИ РАН");
			college.save();

			college = new College();
			college.setName("MBS");
			college.save();

			college = new College();
			college.setName("ЦНИИП градостроительства РААСН");
			college.save();

			college = new College();
			college.setName("ESMOD MOSCOU");
			college.save();

			college = new College();
			college.setName("ИМГ РАН");
			college.save();

			college = new College();
			college.setName("ВФЭУ");
			college.save();

			college = new College();
			college.setName("АВР (бывш. КИ КГБ)");
			college.save();

			college = new College();
			college.setName("ОИВТ РАН");
			college.save();

			college = new College();
			college.setName("МСЕХ");
			college.save();

			college = new College();
			college.setName("НИИ урологии МЗ РФ");
			college.save();

			college = new College();
			college.setName("МНИИП");
			college.save();

			college = new College();
			college.setName("CBS");
			college.save();

			college = new College();
			college.setName("РПУ");
			college.save();

			college = new College();
			college.setName("ВНИИГиМ им. А. Н. Костякова");
			college.save();

			college = new College();
			college.setName("ИНСТЭП");
			college.save();

			college = new College();
			college.setName("ИХП");
			college.save();

			college = new College();
			college.setName("ЦИТО им. Н. Н. Приорова");
			college.save();

			college = new College();
			college.setName("ВНИИнефть им. А. П. Крылова");
			college.save();

			college = new College();
			college.setName("ВШСИ Константина Райкина");
			college.save();
			
			/*college = new College();
			college.setName("Не определен");
			college.save();*/	
		}
		
		
		if (User.find.findRowCount() == 0) {
			
			User user = new User();
			user.setPassword("b1520bfe7e0b2d4415900242c1f932df");
			user.setEmail("cromlehg@gmail.com");
			user.setFirstName("Alexander");
			user.setSecondName("Strakh");
			user.setMiddleName("Valentinovich");
			user.addAdminRole();
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				user.setBirthday(formatter.parse("1986-07-21"));	
		        } catch(ParseException e) {
		        }
			
			user.save();
			
			Ebean.saveManyToManyAssociations(user, "roles");
	
		
			user = new User();
			user.setPassword("2d5f992a5a7523351f21c5036974961b");
			user.setEmail("katya.tupova@gmail.com");
			user.setFirstName("Екатерина");
			user.setSecondName("Тупова");
			user.addEditorRole();	
			user.addAdminRole();	
			user.save();
			
			Ebean.saveManyToManyAssociations(user, "roles");
			
			
			user = new User();
			user.setPassword("b95330a0b5014990b5dae663195d1e4a");
			user.setEmail("editor@editor.ru");
			user.setFirstName("Минаев");
			user.setSecondName("Николай");
			user.addEditorRole();	
			user.save();
			
			Ebean.saveManyToManyAssociations(user, "roles");
	
			user = new User();
			user.setPassword("2d5f992a5a7523351f21c5036974961b");
			user.setEmail("in.a.word.editorial@gmail.com");
			user.addEditorRole();	
			user.save();
			
			Ebean.saveManyToManyAssociations(user, "roles");
	
		}
		
		if(Genre.find.findRowCount() == 0) {
			Genre genre1 = new Genre();
			genre1.setName("Детектив");
			genre1.save();

			Genre genre2 = new Genre();
			genre2.setName("Зарубежный детектив");
			genre1.addGenre(genre2);
			genre2.save();

			Genre genre3 = new Genre();
			genre3.setName("Российский детектив");
			genre1.addGenre(genre3);
			genre3.save();

			Genre genre4 = new Genre();
			genre4.setName("Поэзия. Драматургия");
			genre4.save();

			Genre genre5 = new Genre();
			genre5.setName("Драматургия");
			genre4.addGenre(genre5);
			genre5.save();

			Genre genre6 = new Genre();
			genre6.setName("Зарубежная поэзия");
			genre4.addGenre(genre6);
			genre6.save();

			Genre genre7 = new Genre();
			genre7.setName("Российская поэзия");
			genre4.addGenre(genre7);
			genre7.save();

			Genre genre8 = new Genre();
			genre8.setName("Сборники поэзии авторов разных стран");
			genre4.addGenre(genre8);
			genre8.save();

			Genre genre9 = new Genre();
			genre9.setName("Фантастика");
			genre9.save();

			Genre genre10 = new Genre();
			genre10.setName("Зарубежная фантастика");
			genre9.addGenre(genre10);
			genre10.save();

			Genre genre11 = new Genre();
			genre11.setName("Комикс, манга, графический роман");
			genre9.addGenre(genre11);
			genre11.save();

			Genre genre12 = new Genre();
			genre12.setName("Российская фантастика");
			genre9.addGenre(genre12);
			genre12.save();

			Genre genre13 = new Genre();
			genre13.setName("Художественно-документальная проза. Мемуары");
			genre13.save();

			Genre genre14 = new Genre();
			genre14.setName("Биографии");
			genre13.addGenre(genre14);
			genre14.save();

			Genre genre15 = new Genre();
			genre15.setName("Мемуары. Дневники. Эпистолярный жанр");
			genre13.addGenre(genre15);
			genre15.save();

			Genre genre16 = new Genre();
			genre16.setName("Публицистика");
			genre13.addGenre(genre16);
			genre16.save();

			Genre genre17 = new Genre();
			genre17.setName("Афоризмы. Цитаты. Фольклор. Сборники разных жанров");
			genre17.save();

			Genre genre18 = new Genre();
			genre18.setName("Зарубежная проза");
			genre18.save();

			Genre genre19 = new Genre();
			genre19.setName("Российская проза");
			genre19.save();

			Genre genre20 = new Genre();
			genre20.setName("Юмористическая литература. Сатира");
			genre20.save();

			Genre genre22 = new Genre();
			genre22.setName("Военное дело");
			genre22.save();

			Genre genre23 = new Genre();
			genre23.setName("Военная наука. История военного дела");
			genre22.addGenre(genre23);
			genre23.save();

			Genre genre24 = new Genre();
			genre24.setName("Военная техника и оружие");
			genre22.addGenre(genre24);
			genre24.save();

			Genre genre25 = new Genre();
			genre25.setName("Военные мемуары");
			genre22.addGenre(genre25);
			genre25.save();

			Genre genre26 = new Genre();
			genre26.setName("Специальные службы");
			genre22.addGenre(genre26);
			genre26.save();

			Genre genre27 = new Genre();
			genre27.setName("Информационные и компьютерные технологии");
			genre27.save();

			Genre genre28 = new Genre();
			genre28.setName("Аппаратное обеспечение");
			genre27.addGenre(genre28);
			genre28.save();

			Genre genre29 = new Genre();
			genre29.setName("Базы и банки данных. СУБД");
			genre27.addGenre(genre29);
			genre29.save();

			Genre genre30 = new Genre();
			genre30.setName("Защита информации. Компьютерная безопасность");
			genre27.addGenre(genre30);
			genre30.save();

			Genre genre31 = new Genre();
			genre31.setName("ИКТ. Общие вопросы");
			genre27.addGenre(genre31);
			genre31.save();

			Genre genre32 = new Genre();
			genre32.setName("Компьютерные сети. Интернет");
			genre27.addGenre(genre32);
			genre32.save();

			Genre genre33 = new Genre();
			genre33.setName("Операционные системы и программы-оболочки");
			genre27.addGenre(genre33);
			genre33.save();

			Genre genre34 = new Genre();
			genre34.setName("Прикладное программное обеспечение");
			genre27.addGenre(genre34);
			genre34.save();

			Genre genre35 = new Genre();
			genre35.setName("Языки и средства программирования");
			genre27.addGenre(genre35);
			genre35.save();

			Genre genre36 = new Genre();
			genre36.setName("Искусство");
			genre36.save();

			Genre genre37 = new Genre();
			genre37.setName("Архитектура. Градостроительство");
			genre36.addGenre(genre37);
			genre37.save();

			Genre genre38 = new Genre();
			genre38.setName("Дизайн");
			genre36.addGenre(genre38);
			genre38.save();

			Genre genre39 = new Genre();
			genre39.setName("Живопись. Графика");
			genre36.addGenre(genre39);
			genre39.save();

			Genre genre40 = new Genre();
			genre40.setName("Зрелищные виды искусств, кино");
			genre36.addGenre(genre40);
			genre40.save();

			Genre genre41 = new Genre();
			genre41.setName("История искусств. Искусствоведение");
			genre36.addGenre(genre41);
			genre41.save();

			Genre genre42 = new Genre();
			genre42.setName("Мода");
			genre36.addGenre(genre42);
			genre42.save();

			Genre genre43 = new Genre();
			genre43.setName("Музыка");
			genre36.addGenre(genre43);
			genre43.save();

			Genre genre44 = new Genre();
			genre44.setName("Скульптура. Декоративно-прикладное искусство");
			genre36.addGenre(genre44);
			genre44.save();

			Genre genre45 = new Genre();
			genre45.setName("Фотоискусство");
			genre36.addGenre(genre45);
			genre45.save();

			Genre genre46 = new Genre();
			genre46.setName("История. Исторические науки");
			genre46.save();

			Genre genre47 = new Genre();
			genre47.setName("Всемирная история. Древний мир. История цивилизаций");
			genre46.addGenre(genre47);
			genre47.save();

			Genre genre48 = new Genre();
			genre48.setName("История зарубежных стран");
			genre46.addGenre(genre48);
			genre48.save();

			Genre genre49 = new Genre();
			genre49.setName("История России");
			genre46.addGenre(genre49);
			genre49.save();

			Genre genre50 = new Genre();
			genre50.setName("Этнография. Традиции. Праздники. Ритуалы");
			genre46.addGenre(genre50);
			genre50.save();

			Genre genre51 = new Genre();
			genre51.setName("Медицина");
			genre51.save();

			Genre genre52 = new Genre();
			genre52.setName("Популярная и нетрадиционная медицина");
			genre51.addGenre(genre52);
			genre52.save();

			Genre genre53 = new Genre();
			genre53.setName("Специальная медицина");
			genre51.addGenre(genre53);
			genre53.save();

			Genre genre54 = new Genre();
			genre54.setName("Политика. Политические науки. Социология");
			genre54.save();

			Genre genre55 = new Genre();
			genre55.setName("Политические мемуары, журналистские расследования");
			genre54.addGenre(genre55);
			genre55.save();

			Genre genre56 = new Genre();
			genre56.setName("Политическое положение зарубежных стран");
			genre54.addGenre(genre56);
			genre56.save();

			Genre genre57 = new Genre();
			genre57.setName("Политическое положение России");
			genre54.addGenre(genre57);
			genre57.save();

			Genre genre58 = new Genre();
			genre58.setName("Политология. Геополитика");
			genre54.addGenre(genre58);
			genre58.save();

			Genre genre59 = new Genre();
			genre59.setName("Социология");
			genre54.addGenre(genre59);
			genre59.save();

			Genre genre60 = new Genre();
			genre60.setName("Право. Юридические науки");
			genre60.save();

			Genre genre61 = new Genre();
			genre61.setName("Государственное, конституционное, административное");
			genre60.addGenre(genre61);
			genre61.save();

			Genre genre62 = new Genre();
			genre62.setName("Государство и право зарубежных стран");
			genre60.addGenre(genre62);
			genre62.save();

			Genre genre63 = new Genre();
			genre63.setName("Гражданское право");
			genre60.addGenre(genre63);
			genre63.save();

			Genre genre64 = new Genre();
			genre64.setName("Земельное право. Аграрное право");
			genre60.addGenre(genre64);
			genre64.save();

			Genre genre65 = new Genre();
			genre65.setName("Международное право");
			genre60.addGenre(genre65);
			genre65.save();

			Genre genre66 = new Genre();
			genre66.setName("Правоохранительные органы. Арбитраж. Адвокатура");
			genre60.addGenre(genre66);
			genre66.save();

			Genre genre67 = new Genre();
			genre67.setName("Судопроизводство. ГПП. УПП");
			genre60.addGenre(genre67);
			genre67.save();

			Genre genre68 = new Genre();
			genre68.setName("Теория государства и права");
			genre60.addGenre(genre68);
			genre68.save();

			Genre genre69 = new Genre();
			genre69.setName("Уголовное право. Криминология. Криминалистика");
			genre60.addGenre(genre69);
			genre69.save();

			Genre genre70 = new Genre();
			genre70.setName("Финансовое право. Налоговое право");
			genre60.addGenre(genre70);
			genre70.save();

			Genre genre71 = new Genre();
			genre71.setName("Психология");
			genre71.save();

			Genre genre72 = new Genre();
			genre72.setName("Детская психология");
			genre71.addGenre(genre72);
			genre72.save();

			Genre genre73 = new Genre();
			genre73.setName("Общая психология");
			genre71.addGenre(genre73);
			genre73.save();

			Genre genre74 = new Genre();
			genre74.setName("Популярная психология");
			genre71.addGenre(genre74);
			genre74.save();

			Genre genre75 = new Genre();
			genre75.setName("Прикладная и практическая психология");
			genre71.addGenre(genre75);
			genre75.save();

			Genre genre76 = new Genre();
			genre76.setName("Психологические школы и направления");
			genre71.addGenre(genre76);
			genre76.save();

			Genre genre77 = new Genre();
			genre77.setName("Социальная психология");
			genre71.addGenre(genre77);
			genre77.save();

			Genre genre78 = new Genre();
			genre78.setName("Религия. Теология. Мифология");
			genre78.save();

			Genre genre79 = new Genre();
			genre79.setName("История религии. Религиоведение");
			genre78.addGenre(genre79);
			genre79.save();

			Genre genre80 = new Genre();
			genre80.setName("Мифология");
			genre78.addGenre(genre80);
			genre80.save();

			Genre genre81 = new Genre();
			genre81.setName("Прочие религии мира");
			genre78.addGenre(genre81);
			genre81.save();

			Genre genre82 = new Genre();
			genre82.setName("Христианство");
			genre78.addGenre(genre82);
			genre82.save();

			Genre genre83 = new Genre();
			genre83.setName("Философские науки");
			genre83.save();

			Genre genre84 = new Genre();
			genre84.setName("Культурология");
			genre83.addGenre(genre84);
			genre84.save();

			Genre genre85 = new Genre();
			genre85.setName("Логика. Этика. Эстетика");
			genre83.addGenre(genre85);
			genre85.save();

			Genre genre86 = new Genre();
			genre86.setName("Философия. История философии");
			genre83.addGenre(genre86);
			genre86.save();

			Genre genre87 = new Genre();
			genre87.setName("Эзотерика");
			genre83.addGenre(genre87);
			genre87.save();

			Genre genre88 = new Genre();
			genre88.setName("Экономика. Управление. Предпринимательство");
			genre88.save();

			Genre genre89 = new Genre();
			genre89.setName("Бизнес. Предпринимательство");
			genre88.addGenre(genre89);
			genre89.save();

			Genre genre90 = new Genre();
			genre90.setName("Бухгалтерский учет. Налогообложение. Аудит");
			genre88.addGenre(genre90);
			genre90.save();

			Genre genre91 = new Genre();
			genre91.setName("Внешние экономические связи. Таможенное дело");
			genre88.addGenre(genre91);
			genre91.save();

			Genre genre92 = new Genre();
			genre92.setName("История экономической мысли. Политэкономия");
			genre88.addGenre(genre92);
			genre92.save();

			Genre genre93 = new Genre();
			genre93.setName("Маркетинг. Реклама и связи с общественностью");
			genre88.addGenre(genre93);
			genre93.save();

			Genre genre94 = new Genre();
			genre94.setName("Мировая экономика. Зарубежные страны и Россия");
			genre88.addGenre(genre94);
			genre94.save();

			Genre genre95 = new Genre();
			genre95.setName("Отраслевая экономика");
			genre88.addGenre(genre95);
			genre95.save();

			Genre genre96 = new Genre();
			genre96.setName("Управление. Менеджмент");
			genre88.addGenre(genre96);
			genre96.save();

			Genre genre97 = new Genre();
			genre97.setName("Финансы. Банковское дело. Денежное обращение. Ценные бумаги");
			genre88.addGenre(genre97);
			genre97.save();

			Genre genre98 = new Genre();
			genre98.setName("Языкознание");
			genre98.save();

			Genre genre99 = new Genre();
			genre99.setName("Английский язык");
			genre98.addGenre(genre99);
			genre99.save();

			Genre genre100 = new Genre();
			genre100.setName("Испанский язык");
			genre98.addGenre(genre100);
			genre100.save();

			Genre genre101 = new Genre();
			genre101.setName("Итальянский язык");
			genre98.addGenre(genre101);
			genre101.save();

			Genre genre102 = new Genre();
			genre102.setName("Многоязычные словари, разговорники");
			genre98.addGenre(genre102);
			genre102.save();

			Genre genre103 = new Genre();
			genre103.setName("Немецкий язык");
			genre98.addGenre(genre103);
			genre103.save();

			Genre genre104 = new Genre();
			genre104.setName("Общее и прикладное языкознание");
			genre98.addGenre(genre104);
			genre104.save();

			Genre genre105 = new Genre();
			genre105.setName("Прочие языки мира");
			genre98.addGenre(genre105);
			genre105.save();

			Genre genre106 = new Genre();
			genre106.setName("Русский язык");
			genre98.addGenre(genre106);
			genre106.save();

			Genre genre107 = new Genre();
			genre107.setName("Русский язык для иностранцев");
			genre98.addGenre(genre107);
			genre107.save();

			Genre genre108 = new Genre();
			genre108.setName("Французский язык");
			genre98.addGenre(genre108);
			genre108.save();

			Genre genre109 = new Genre();
			genre109.setName("Естественные науки");
			genre109.save();

			Genre genre110 = new Genre();
			genre110.setName("Литературоведение. Критика");
			genre110.save();

			Genre genre111 = new Genre();
			genre111.setName("Средства массовой информации");
			genre111.save();

			Genre genre112 = new Genre();
			genre112.setName("Статистика. Демография. Социальное обеспечение");
			genre112.save();

			Genre genre113 = new Genre();
			genre113.setName("Транспорт");
			genre113.save();

			Genre genre114 = new Genre();
			genre114.setName("Энциклопедии");
			genre114.save();



		}


	}

}
