package controllers;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import models.Author;
import models.Book;
import models.College;
import models.Genre;
import models.Material;
import models.Media;
import models.User;
import models.form.AvatarUploadForm;
import models.form.RegisterUserForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.Logger;
import util.Helper;
import views.html.afterRegister;
import views.html.editProfile;
import views.html.editUserBand;
import views.html.index;
import views.html.profile;
import views.html.register;
import views.html.base.listenAuthors;
import views.html.base.listenBooks;
import views.html.base.listenColleges;
import views.html.base.listenGenres;
import views.html.base.listenUsers;
import be.objectify.deadbolt.java.actions.SubjectNotPresent;
import be.objectify.deadbolt.java.actions.SubjectPresent;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.PagingList;

public class Users extends Controller {
	
	private final static Form<RegisterUserForm> sRegisterUserForm = Form.form(RegisterUserForm.class);
	private final static Form<AvatarUploadForm> sAvatarUploadForm = Form.form(AvatarUploadForm.class);
	
	  @SubjectNotPresent
	  public static Result login(String email, String password) {
		Logger.trace("login: try to ajax login procress for: " + email);
	    //if(email == null || email.equals(UNDEFINED)) {
	    if(email == null) {
	      return ok("Заполните поле email");
	    }

	    //if(password == null || email.equals(UNDEFINED)) {
	    if(password == null) {
	      return ok("Заполните поле password");
	    }

	    User user = User.findByEmail(email);
	    if(user == null) {
	      return ok("Пользователь с таким email не существует");
	    }

	    String passwordHash = Helper.getDoubleMD5(password);
	    if(!passwordHash.equals(user.password)) {
	      return ok("Не верный email или пароль");
	    }

	    user.setHash(Helper.getMD5RandomString());
	    user.setIp(request().remoteAddress());
	    user.update();

	    session().put(User.FIELD_ID, user.getId().toString());
	    session().put(User.FIELD_HASH, user.getHash());

	    return ok();
	  }


	
	//@Restrictions({@And(SecurityRole.ROLE_ADMIN), @And(SecurityRole.ROLE_EDITOR), @And(SecurityRole.ROLE_USER)})
	@SubjectPresent
	public static Result changeUserSecondName(Long id, String newValue) {
		User user = getUser();
		user.setSecondName(newValue);
		user.update();
		return ok();
	}
	
	@SubjectPresent
	public static Result changeUserFirstName(Long id, String newValue) {
		User user = getUser();
		user.setFirstName(newValue);
		user.update();
		return ok();
	}
	
	@SubjectPresent
	public static Result changeUserMiddleName(Long id, String newValue) {
		User user = getUser();
		user.setMiddleName(newValue);
		user.update();
		return ok();
	}
	
	@SubjectPresent
	public static Result changeUserStatus(Long id, String newValue) {
		if(newValue == null || newValue.length() == 0) {
			flash("error", "Status can't be null");
			return badRequest();
	    }
		
		models.Status status = models.Status.findById(Long.parseLong(newValue));
		if(status == null) {
			flash("error", "Unknown status: " + newValue);
			return badRequest();			
		}
		
		User user = getUser();
		
		if(user == null) {
			flash("error", "Cant' find user");
			return badRequest();			
		}
		
		user.setStatus(status);
		user.update();
		
		return ok();
	}

	@SubjectPresent
	public static Result changeUserCollege(Long id, String newValue) {
		if(newValue == null || newValue.length() == 0) {
			flash("error", "College can't be null");
			return badRequest();
	    }
		
		User user = getUser();
		
		if(user == null) {
			flash("error", "Cant' find user");
			return badRequest();			
		}

		// or by id (newValue - is college id?)
		College college = College.findById(Long.parseLong(newValue));
		if(college == null) {
			college = new College();
			college.setName(newValue);
			college.save();
		}
		
		user.setCollege(college);
		user.update();
		
		return ok();
	}
	
	@SubjectPresent
	public static Result changeUserBirthday(Long id, String newValue) {
		// TODO: check parse errors
		User user = getUser();
		
		try {
			user.setBirthday(Application.formatter.parse(newValue));
			user.update();
        } catch(ParseException e) {
        }
		

		return ok();
	}
	
	@SubjectNotPresent
	public static Result register() {
		return ok(register.render(sRegisterUserForm));
	}
	
	@SubjectNotPresent
	public static Result afterRegister() {
		return ok(afterRegister.render());
	}
	
	public static User getUser() {
		String id = session().get(User.FIELD_ID);
		String hash = session().get(User.FIELD_HASH);

		if (id == null || hash == null)
			return null;

		return User.findByIdAndHash(id, hash);
	}
	
	@SubjectNotPresent
	public static Result processRegister() {
		Form<RegisterUserForm> registerUserForm = sRegisterUserForm.bindFromRequest();
		if (registerUserForm.hasErrors()) {
			return badRequest(register.render(registerUserForm));
		}

        User user = new User();
		user.setEmail(registerUserForm.field(User.FIELD_EMAIL).value());
		user.setPassword(Helper.getDoubleMD5(registerUserForm.field(User.FIELD_PASSWORD).value()));
		user.addUserRole();
		user.save();
		
		flash("Поздравляем, вы успешно зарегестрировались!");
		
		return redirect(routes.Users.afterRegister());
	}
	
	public static Result profileAuthUser() {
		User user = getUser();
		return ok(profile.render(user));		
	}
	
	@SubjectPresent
	public static Result profile(Long id) {
		User user = User.findById(id);
		return ok(profile.render(user));
	}


	
	@SubjectPresent
	public static Result processAvatarUpload(Long id) {
		User user = User.findById(id);
		controllers.Status status = Medias.upload(user.getId(), User.FIELD_AVATAR, true);
		if(status.error != null) {
			flash("error", status.error);
			return badRequest(status.error);
		}
		user.setAvatar(status.result);
		user.update();
		// try to crop:
		Medias.crop((Media)status.object);
	        return ok(editProfile.render(user, sAvatarUploadForm));
	}
	
	public static Result editProfile(Long id) {
		User user = User.findById(id);
		if(user == null) {
			Logger.error("editProfile: Can't find user with id: " + id);
			return badRequest("Can't find user with id: " + id);
		}
		if(!user.access()) {
			Logger.error("editProfile: Access denied for edit user with id: " + user.getId() + " by user: " + (Users.getUser() == null ? null : Users.getUser().getId()));
			return badRequest("Access denied!" );
		}
		return ok(editProfile.render(user, sAvatarUploadForm)); 
	}
	
	@SubjectPresent
	public static Result logout() {
		User user = getUser();
		if (user != null) {
			user.hash = null;
			user.ip = null;
			user.update();
			user = null;
			session().clear();
		}
		return ok(index.render(null));
	}
	
	@SubjectPresent
	public static Result editUserBand(Long id) {
		User curUser = getUser();
		User editedUser = User.findById(id);
		if(editedUser == null)
			return badRequest("Can't find edited user!");
		
		if(curUser.getId() != editedUser.getId() && !curUser.isAdmin())
			return badRequest("Access denied");
		
		return ok(editUserBand.render(editedUser));
	}

	@SubjectPresent
	public static Result setListenObject(Long userId, String name, Long objId, Boolean isListen) {
		if(name == null)
			return badRequest("Field can't be null!");
		
		User user = User.findById(userId);
		if(user == null)
			return badRequest("Can't get user by id!");
		
		User auser = Users.getUser();
		if(auser == null)
			return badRequest("Can't get auth user");
		
		if(user.getId() != userId && !user.isAdmin())
			return badRequest("Access denied!"); 
		
	  Band band = new Band(user.getCachedBand());
    if(isListen)
      band.addId(name, objId);
    else
      band.removeId(name, objId);
    user.setCachedBand(band.getUserBand());
   
		if(name.equals("user")) {
			User add = User.findById(objId);
			if(add == null)
				return badRequest("CVan't find add object");
			
			Set<User> lobjects = user.getListenUsers();
			if(isListen) {
				if(lobjects == null) {
					lobjects = new HashSet<User>();
					user.setListenUsers(lobjects);;
				}
				lobjects.add(add);
			} else {
				if(lobjects != null) {
					Iterator<User> iter = lobjects.iterator();
					while(iter.hasNext()) {
						User lobject = iter.next();
						if(lobject.getId() == add.getId()) {
							iter.remove();
						}
					}
				}
			}
			user.update();
			Ebean.saveManyToManyAssociations(user, "listenUsers");
			
		} else if(name.equals("author")) {
			Author add = Author.findById(objId);
			if(add == null)
				return badRequest("CVan't find add object");
			
			Set<Author> lobjects = user.getListenAuthors();
			if(isListen) {
				if(lobjects == null) {
					lobjects = new HashSet<Author>();
					user.setListenAuthors(lobjects);;
				}
				lobjects.add(add);
			} else {
				if(lobjects != null) {
					Iterator<Author> iter = lobjects.iterator();
					while(iter.hasNext()) {
						Author lobject = iter.next();
						if(lobject.getId() == add.getId()) {
							iter.remove();
						}
					}
				}
			}
			user.update();
			Ebean.saveManyToManyAssociations(user, "listenAuthors");
		} else if(name.equals("genre")) {
			Genre add = Genre.findById(objId);
			if(add == null)
				return badRequest("CVan't find add object");
			
			Set<Genre> lobjects = user.getListenGenres();
			if(isListen) {
				if(lobjects == null) {
					lobjects = new HashSet<Genre>();
					user.setListenGenres(lobjects);;
				}
				lobjects.add(add);
			} else {
				if(lobjects != null) {
					Iterator<Genre> iter = lobjects.iterator();
					while(iter.hasNext()) {
						Genre lobject = iter.next();
						if(lobject.getId() == add.getId()) {
							iter.remove();
						}
					}
				}
			}
			user.update();
			Ebean.saveManyToManyAssociations(user, "listenGenres");			
		} else if(name.equals("book")) {
			Book add = Book.findById(objId);
			if(add == null)
				return badRequest("CVan't find add object");
			
			Set<Book> lobjects = user.getListenBooks();
			if(isListen) {
				if(lobjects == null) {
					lobjects = new HashSet<Book>();
					user.setListenBooks(lobjects);;
				}
				lobjects.add(add);
			} else {
				if(lobjects != null) {
					Iterator<Book> iter = lobjects.iterator();
					while(iter.hasNext()) {
						Book lobject = iter.next();
						if(lobject.getId() == add.getId()) {
							iter.remove();
						}
					}
				}
			}
			user.update();
			Ebean.saveManyToManyAssociations(user, "listenBooks");		
		} else if(name.equals("college")) {
			College add = College.findById(objId);
			if(add == null)
				return badRequest("CVan't find add object");
			
			Set<College> lobjects = user.getListenColleges();
			if(isListen) {
				if(lobjects == null) {
					lobjects = new HashSet<College>();
					user.setListenColleges(lobjects);;
				}
				lobjects.add(add);
			} else {
				if(lobjects != null) {
					Iterator<College> iter = lobjects.iterator();
					while(iter.hasNext()) {
						College lobject = iter.next();
						if(lobject.getId() == add.getId()) {
							iter.remove();
						}
					}
				}
			}
			user.update();
			Ebean.saveManyToManyAssociations(user, "listenColleges");			
		} else {
			return badRequest("Unknown field name: " + name);
		}
		
		return ok();
	}
	
	@SubjectPresent
	public static Result getListenObjectsPagesCount(Long id, String name) {
		if(id<0)
			return badRequest("Negaitve user index");
		if(name == null)
			return badRequest("Null listen object name");
		
		User user = User.findById(id);
		if(user == null)
			return badRequest("Can't find edited user by id: " + id);
		
		User auser = getUser();
		if(auser == null)
			return badRequest("Can't get authorized user");
		
		if(user.getId() != auser.getId() && !user.isAdmin())
			return badRequest("Access denied");
		
		if(name.equals("user")) {
			PagingList<User> pList = User.find.orderBy(User.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()); 
			return ok(pList == null ? "0" : pList.getTotalPageCount()+"");
		} else if(name.equals("college")) {
			PagingList<College> pList = College.find.orderBy(User.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize());
			return ok(pList == null ? "0" : pList.getTotalPageCount()+"");
		} else if(name.equals("book")) {
			PagingList<Book> pList = Book.find.orderBy(User.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize());
			return ok(pList == null ? "0" : pList.getTotalPageCount()+"");
		} else if(name.equals("genre")) {
			PagingList<Genre> pList = Genre.find.orderBy(User.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()); 
			return ok(pList == null ? "0" : pList.getTotalPageCount()+"");
		} else if(name.equals("author")) {
			PagingList<Author> pList = Author.find.orderBy(User.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()); 
			return ok(pList == null ? "0" : pList.getTotalPageCount()+"");
		} else {
			return badRequest("Unknown listen object identifier: " + name);
		}		
	}
	
	@SubjectPresent
	public static Result getListenObjectsPage(Long id, String name, Integer pid) {
		if(pid < 1)
			return badRequest("Wrong page index: " + pid);
		
		if(id<0)
			return badRequest("Negaitve user index");
		
		if(name == null)
			return badRequest("Null listen object name");
		
		User user = User.findById(id);
		if(user == null)
			return badRequest("Can't find edited user by id: " + id);
		
		User auser = getUser();
		if(auser == null)
			return badRequest("Can't get authorized user");
		
		if(user.getId() != auser.getId() && !user.isAdmin())
			return badRequest("Access denied");
		
		if(name.equals("user")) {
			return ok(listenUsers.render(
					User.find.orderBy(User.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()).getPage(pid-1).getList()
					));
		} else if(name.equals("college")) {
			return ok(listenColleges.render(
					College.find.orderBy(College.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()).getPage(pid-1).getList()
					));
		} else if(name.equals("book")) {
			return ok(listenBooks.render(
					Book.find.orderBy(Book.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()).getPage(pid-1).getList()
					));
		} else if(name.equals("genre")) {
			return ok(listenGenres.render(
					Genre.find.orderBy(Genre.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()).getPage(pid-1).getList()
					));
		} else if(name.equals("author")) {
			return ok(listenAuthors.render(
					Author.find.orderBy(Author.FIELD_ID).where().eq("usersListen.id", id).findPagingList(Application.getPagesSize()).getPage(pid-1).getList()
					));
		} else {
			return badRequest("Unknown listen object identifier: " + name);
		}				
	}
	
	@SubjectPresent
	public static Result isIlike(Long userId, Long matId) {
		if(userId<0)
			return badRequest("Negaitve user index");

		User user = User.findById(userId);
		if(user == null)
			return badRequest("Can't find edited user by id: " + userId);
		
		User auser = getUser();
		if(auser == null)
			return badRequest("Can't get authorized user");
		
		if(user.getId() != auser.getId() && !user.isAdmin())
			return badRequest("Access denied");

		return ok(CacheUtils.isExists(user.getCachedIlikeMaterials(), matId)+"");
	}
	
	@SubjectPresent
	public static Result isIgo(Long userId, Long matId) {
		if(userId<0)
			return badRequest("Negaitve user index");

		User user = User.findById(userId);
		if(user == null)
			return badRequest("Can't find edited user by id: " + userId);
		
		User auser = getUser();
		if(auser == null)
			return badRequest("Can't get authorized user");
		
		if(user.getId() != auser.getId() && !user.isAdmin())
			return badRequest("Access denied");

		return ok(CacheUtils.isExists(user.getCachedIgoMaterials(), matId)+"");
	}

	public static boolean isAuthUserGoLocal(Material mat) {
		return CacheUtils.isExists(getUser().getCachedIgoMaterials(), mat.getId());
	}

	
	public static boolean isIgoLocal(User user, Material mat) {
		return CacheUtils.isExists(user.getCachedIgoMaterials(), mat.getId());
	}
	
	public static boolean isIlikeLocal(User user, Material mat) {
		return CacheUtils.isExists(user.getCachedIlikeMaterials(), mat.getId());
	}
	
	@SubjectPresent
	public static Result setIgo(Long userId, Long matId, Boolean isGoing) {
		if(userId<0)
			return badRequest("Negaitve user index");

		if(matId<0)
			return badRequest("Negaitve material index");
		
		User user = User.findById(userId);
		if(user == null)
			return badRequest("Can't find edited user by id: " + userId);
		
		Material mat = Material.findById(matId);
		if(mat == null)
			return badRequest("Can't find edited material by id " + matId);
		
		User auser = getUser();
		if(auser == null)
			return badRequest("Can't get authorized user");
		
		if(user.getId() != auser.getId() && !user.isAdmin())
			return badRequest("Access denied");
		
		
		
		Set<Material> mats = user.getIgoMaterials();
		if(isGoing) {			
			if(mats == null) {
				mats = new HashSet<Material>();
				user.setIgoMaterials(mats);
			}
			// check if its already exists
			mats.add(mat);
			mat.setCachedIgoUsersCount((mat.getCachedIgoUsersCount() == null ? 0: mat.getCachedIgoUsersCount()) +1);
			user.setCachedIgoMaterialsCount((user.getCachedIgoMaterialsCount() == null ? 0: user.getCachedIgoMaterialsCount()) +1);
		} else {			
			mats.remove(mat); // ?? may be not works
			mat.setCachedIgoUsersCount((mat.getCachedIgoUsersCount() == null ? 0: mat.getCachedIgoUsersCount())-1);
			user.setCachedIgoMaterialsCount((user.getCachedIgoMaterialsCount() == null ? 0: user.getCachedIgoMaterialsCount()) -1);
		}
			
		mat.setCachedIgoUsers(CacheUtils.setId(mat.getCachedIgoUsers(), user.getId(), isGoing));	
		user.setCachedIgoMaterials(CacheUtils.setId(user.getCachedIgoMaterials(), mat.getId(), isGoing));		
		user.update();
		Ebean.saveManyToManyAssociations(user, "igoMaterials");
		mat.update();
		return ok();
	}
	
	@SubjectPresent
	public static Result setIlike(Long userId, Long matId, Boolean isLike) {
		if(userId<0)
			return badRequest("Negaitve user index");

		if(matId<0)
			return badRequest("Negaitve material index");
		
		User user = User.findById(userId);
		if(user == null)
			return badRequest("Can't find edited user by id: " + userId);
		
		Material mat = Material.findById(matId);
		if(mat == null)
			return badRequest("Can't find edited material by id " + matId);
		
		User auser = getUser();
		if(auser == null)
			return badRequest("Can't get authorized user");
		
		if(user.getId() != auser.getId() && !user.isAdmin())
			return badRequest("Access denied");
		
		Set<Material> mats = user.getIlikeMaterials();		
		if(isLike) {
			if(mats == null) {
				mats = new HashSet<Material>();
				user.setIlikeMaterials(mats);
			}
			// check if its already exists
			mats.add(mat);
			mat.setCachedIlikeUsersCount((mat.getCachedIlikeUsersCount() == null ? 0: mat.getCachedIlikeUsersCount()) + 1);
		} else {
			mats.remove(mat); // ?? may be not works
			mat.setCachedIlikeUsersCount((mat.getCachedIlikeUsersCount() == null ? 0: mat.getCachedIlikeUsersCount()) - 1);
		}
		mat.setCachedIlikeUsers(CacheUtils.setId(mat.getCachedIlikeUsers(), user.getId(), isLike));
		user.setCachedIlikeMaterials(CacheUtils.setId(user.getCachedIlikeMaterials(), mat.getId(), isLike));
		user.update();
		Ebean.saveManyToManyAssociations(user, "ilikeMaterials");
		mat.update();
		return ok();
	}
	  
	public static boolean isAuthUserLikeMaterial(Material material) {
		return CacheUtils.isExists(getUser().getCachedIlikeMaterials(), material.getId());
	}	
	
	public static boolean isUserLikeMaterial(User user, Material material) {
		return CacheUtils.isExists(user.getCachedIlikeMaterials(), material.getId());
	}
	
  //======================== band manipulate functions ===========================//
  /**
  * band options format example:
  * "user=1,4,3,4:genre=78,7:author=32,1"
  *
  *
  */
}


