package controllers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import models.Author;
import models.Book;
import models.College;
import models.Comment;
import models.Genre;
import models.Material;
import models.MaterialStatus;
import models.SecurityRole;
import models.User;
import models.form.PublishAfficheForm;
import models.form.PublishForm;
import models.form.PublishInterviewForm;
import models.form.PublishKnigoverthForm;
import models.form.PublishReviewForm;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.materialsByClass;
import views.html.profile;
import views.html.publishAffiche;
import views.html.publishInterview;
import views.html.publishKnigoverth;
import views.html.publishReview;
import views.html.viewMaterial;
import views.html.base.materialsPage;
import be.objectify.deadbolt.java.actions.And;
import be.objectify.deadbolt.java.actions.Restrictions;
import be.objectify.deadbolt.java.actions.SubjectPresent;

import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;

public class Materials extends Controller {

	private final static Form<PublishInterviewForm> sPublishInterviewForm = Form
			.form(PublishInterviewForm.class);
	private final static Form<PublishReviewForm> sPublishReviewForm = Form
			.form(PublishReviewForm.class);
	private final static Form<PublishKnigoverthForm> sPublishKnigoverthForm = Form
			.form(PublishKnigoverthForm.class);
	public final static Form<PublishAfficheForm> sPublishAfficheForm = Form
			.form(PublishAfficheForm.class);

	/* ====================== publish ========================= */

	public static boolean accessEditMat(Material mat) {
		User user = Users.getUser();
		if (user.isAdmin() || user.isEditor()
				|| user.getId() == mat.getOwner().getId())
			return true;

		if (mat.getEditorStatus().getId() == MaterialStatus
				.getUnvisibleStatus().getId()
				&& (user.isAdmin() || user.isEditor()))
			return true;
		return false;
	}

	public static Result remove(Long mid) {
		Material mat = Material.findById(mid);
		
		if (mat == null) {
			Logger.error("remove: Material is null!");
			return badRequest("Material can't be null");
		}
		
		User user = Users.getUser();
		if(user == null) {
			Logger.error("remove: can't get user!");
			return badRequest("Access denied");			
		}
		
		if(!(user.isAdmin() || user.isEditor() || user.getId() == mat.getOwner().getId())) {
			Logger.error("remove: access denied for user: " + user.getId() + " and material: " + mat.getId());
			return badRequest("Access denied");						
		}

		Logger.trace("remove: try to remove material: " + mat.getId());

		
		mat.getOwner().getMaterials().remove(mat);

		if(mat.getAuthors() != null) {
			Iterator<Author> aIter = mat.getAuthors().iterator();
			while(aIter.hasNext()) {
				Author author = aIter.next();
				if(author.getObjMaterials() != null) {
					Iterator<Material> aMats = author.getObjMaterials().iterator();
					while(aMats.hasNext()) {
						Material lMat = aMats.next();
						if(mat.getId() == lMat.getId()) {
							aMats.remove();
							author.update();
							break;
						}
					}
				}
			}
		}
		
		// remove comments
		Iterator<Comment> commIter = mat.getComments().iterator();
		while(commIter.hasNext()) {
			Comment comm = commIter.next();
			for(User userl : comm.getUnviewedUsers()) {
				Logger.trace("remove: remove comment: remove comment " + comm.getId() + " from cached comments of user: " + userl.getId());
				Logger.trace("remove: cached unviewed comments before: " + userl.getCachedUnviwedComments());
				String str = userl.getCachedUnviwedComments();
				List<Long> longs = CacheUtils.getList(str);
				String nstr = null;
				for(int i=0; i<longs.size(); i++) {
					if(comm.getId() != longs.get(i)) {
						if(nstr == null)
							nstr = longs.get(i) + "";
						else 
							nstr += "," + longs.get(i);
					}
				}
				userl.setCachedUnviwedComments(nstr);
				Logger.trace("remove: cached unviewed comments after: " + userl.getCachedUnviwedComments());
				Logger.trace("remove: cached unviewed comments count before: " + userl.getCachedUnviwedCommentsCount());
				userl.setCachedUnviwedCommentsCount(userl.getCachedUnviwedCommentsCount()-1);
				Logger.trace("remove: cached unviewed comments after: " + userl.getCachedUnviwedCommentsCount());
				Iterator<Comment> commIter2 = userl.getUnviwedComments().iterator();
				while(commIter2.hasNext()) {
					Comment comm2 = commIter2.next();
					if(comm2.getId() == comm.getId()) {
						commIter2.remove();
					}
				}
				Logger.trace("remove: remove comments: update user: " + userl);
				userl.update();
			}
			commIter.remove();
			mat.update();
			Logger.trace("remove: remove comments: try to delete comment: " + comm.getId());
			comm.delete();
			Logger.trace("remove: comment deleted + " + comm.getId());
		}

		// remove igo
		Iterator<User> iter = mat.getIgoUsers().iterator();
		while(iter.hasNext()) {
			User userl = iter.next();
			if(userl.getIgoMaterials().remove(mat)) {
				String str = userl.getCachedIgoMaterials();
				List<Long> longs = CacheUtils.getList(str);
				String nstr = null;
				for(int i=0; i<longs.size(); i++) {
					if(mat.getId() != longs.get(i)) {
						if(nstr == null)
							nstr = longs.get(i) + "";
						else 
							nstr += "," + longs.get(i);
					}
				}
				userl.setCachedIgoMaterials(nstr);
				userl.setCachedIgoMaterialsCount(userl.getCachedIgoMaterialsCount()-1);
			}
			userl.update();
			iter.remove();
			mat.update();
		}
		
		mat.delete();
		
		return ok();
	}
//	
//	public void removeComments(Comment comm) {
//		CacheUtils.getList(comm.getNested());
//	}
	
	/**
	 * 
	 * Доступ на редактирование материала аторизованным пользоватлем (user - на
	 * входе должен быть гарантировано авторизованым, т.е. получен с помощью
	 * USers.getUser()) Для этого аторизованный пользователь должен быть: -
	 * авторизован && ( админ || редакция || владелец данного материала ) - если
	 * материал скрыт, то редактировать может только редакция или админ
	 * 
	 */
	public static Result accessEditMat(String from, User user, Material mat) {
		if (mat == null) {
			Logger.error(from + ": Material is null!");
			return badRequest("MAterial can't be null");
		}

		// редактировать скрытый материал может только админ или редактор
		if (mat.getEditorStatus().getId() == MaterialStatus
				.getUnvisibleStatus().getId()
				&& (user.isAdmin() || user.isEditor())) {
			return null;
		}

		if (user == null) {
			Logger.error(from + ": Not authorized users!");
			return badRequest("Can't find current user!");
		}

		if (user.isAdmin() || user.isEditor()
				|| user.getId() == mat.getOwner().getId())
			return null;

		Logger.error(from + ": access denied for auth user with id: "
				+ user.getId() + ", for editing material with id: "
				+ mat.getId() + ", and woner id: " + mat.getOwner().getId()
				+ ", user isAdmin: " + user.isAdmin() + ", user isEditor: "
				+ user.isEditor());
		return badRequest("Access denied!");
	}

	public static Result accessEdit(String from, User user) {
		if (user == null) {
			Logger.error(from + ": Can't get user");
			return badRequest("Can't find user!");
		}

		User auser = Users.getUser();
		if (auser == null) {
			Logger.error(from + ": Not authorized users!");
			return badRequest("Can't find current user!");
		}

		if (!user.access()) {
			Logger.error(from + ": access denied for user with id: "
					+ auser.getId() + ", and authorized user: " + auser.getId()
					+ ", auser isAdmin: " + auser.isAdmin()
					+ ", auser isEditor: " + auser.isEditor());
			return badRequest("Access denied!");
		}
		return null;
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result publishInterview(Long uid) {
		User user = User.findById(uid);
		Result res = accessEdit("publishInterview", user);
		if (res != null)
			return res;
		return ok(publishInterview.render(user, null, sPublishInterviewForm));
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result publishReview(Long uid) {
		User user = User.findById(uid);
		Result res = accessEdit("publishReview", user);
		if (res != null)
			return res;
		return ok(publishReview.render(user, null, sPublishReviewForm));
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result publishKnigoverth(Long uid) {
		User user = User.findById(uid);
		Result res = accessEdit("publishKnigoverth", user);
		if (res != null)
			return res;
		return ok(publishKnigoverth.render(user, null, sPublishKnigoverthForm));
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result publishAffiche(Long uid) {
		User user = User.findById(uid);
		Result res = accessEdit("publishAffiche", user);
		if (res != null)
			return res;
		return ok(publishAffiche.render(user, null, sPublishAfficheForm));
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result processPublishInterview(Long uid, Long mid) {
		User user = User.findById(uid);
		Result res = accessEdit("processPublishAffiche", user);
		if (res != null)
			return res;

		Material mat = mid>=0 ?  Material.getById(mid) : null;
		
		Form<PublishInterviewForm> mform = sPublishInterviewForm
				.bindFromRequest();
		if (mform.hasErrors()) {
			Logger.trace("publishProcessInterview form contains some errors! ");
			return badRequest(publishInterview.render(Users.getUser(), mat,
					mform));
		}

		
		Material material = mat == null ? new Material() : mat;
		material.setDate(Application.cal.getTime());
		material.setContent(mform.field(Material.FIELD_CONTENT).value());
		material.setHeader(mform.field(Material.FIELD_HEADER).value());
		if(mat == null)
			material.setType(Material.CLASS_INTERVIEW);
		MaterialStatus mStatus = MaterialStatus.findById(Long.parseLong(mform
				.field("status").value()));
		material.setMatStatus(mStatus);

		if(mat == null)
			material.setEditorStatus(MaterialStatus.getVisibleStatus());

		controllers.Status status = Medias.upload(user.getId(),
				Material.FIELD_IMAGE, false);
		if (status.error != null) {
			flash("error", status.error);
			return badRequest(publishInterview.render(Users.getUser(), mat,
					mform));
		}

		if(mat == null || (mat!=null && status.result != null))
		material.setImage(status.result);
		
		if(mat == null)
		material.setOwner(user);
		
		if(mat == null)
			material.save();
		else 
			material.update();
		if(mat == null)
			user.addMaterial(material);
		user.update();
		return ok(profile.render(Users.getUser()));
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result processPublishReview(Long uid, Long mid) {
		User user = User.findById(uid);
		Result res = accessEdit("processPublishReview", user);
		if (res != null)
			return res;

		Material mat = mid>=0 ?  Material.getById(mid) : null;

		
		Form<PublishReviewForm> mform = sPublishReviewForm.bindFromRequest();
		if (mform.hasErrors()) {
			return badRequest(publishReview
					.render(Users.getUser(), mat, mform));
		}

		Material material = mat == null ? new Material() : mat;
		material.setDate(Application.cal.getTime());
		material.setContent(mform.field(Material.FIELD_CONTENT).value());
		material.setHeader(mform.field(Material.FIELD_HEADER).value());
		if(mat == null)
		material.setType(Material.CLASS_REVIEW);
		MaterialStatus mStatus = MaterialStatus.findById(Long.parseLong(mform
				.field("status").value()));
		material.setMatStatus(mStatus);
		if(mat == null)
		material.setEditorStatus(MaterialStatus.getVisibleStatus());

		controllers.Status status = Medias.upload(user.getId(),
				Material.FIELD_IMAGE, false);
		if (status.error != null) {
			flash("error", status.error);
			return badRequest(publishReview
					.render(Users.getUser(), null, mform));
		}
		
		if(mat == null ||(mat!=null && status.result !=null))
		material.setImage(status.result);

		String bookName = mform.field("bookName").value();
		String bookAuthor = mform.field("bookAuthor").value();
		String genreName = mform.field("genre").value();

		Book book = saveBook(bookName, bookAuthor, genreName);
		if(material.getBooks() !=null && !material.getBooks().isEmpty()) 	{
			material.getBooks().clear();
			material.update();
		}
		Logger.trace("publish review: Add book:  " + book.getName() + ", athored: " + book.getAuthor().getName());
		CacheUtils.addBook(material, book);
			
		if(mat == null) {
		 material.setOwner(user);
		 material.save();
		 user.addMaterial(material);
		 user.update();
		} else {
			material.save();
			
		}
		
		com.avaje.ebean.Ebean.saveManyToManyAssociations(material, "books");
		material = Material.findById(material.getId());
		return ok(profile.render(Users.getUser()));
	}

	protected static Book saveBook(String bookName, String bookAuthor,
			String genreName) {
		bookAuthor = bookAuthor.trim().replace(" +", " ");
		bookName = bookName.trim().replace(" +", " ");
		
		Genre genre = (genreName == null || genreName.length() == 0) ? null
				: Genre.findById(Long.parseLong(genreName));
		
		Author author = Author.findByName(bookAuthor);
		Book book = null;
		if (author == null) {
			author = new Author();
			author.setName(bookAuthor);
			author.save();
			book = new Book();
			book.setName(bookName);
			book.setAuthor(author);
			book.setGenre(genre);
			book.save();
		} else {
		
		book = Book.findByNameAndAuthor(bookName, author);		


		if (book == null) {
			book = new Book();
			book.setName(bookName);
			book.setAuthor(author);
			book.setGenre(genre);
			book.save();
		} 
		}
		Logger.trace("Book was saved: name= " + bookName + ", author="
				+ bookAuthor + ", genre=" + genreName);
		return book;
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result processPublishKnigoverth(Long uid, Long mid) {
		User user = User.findById(uid);
		Result res = accessEdit("processPublishKnigoverth", user);
		if (res != null)
			return res;

		Material mat = mid >= 0 ? Material.findById(mid) : null;
		
		Form<PublishKnigoverthForm> mform = sPublishKnigoverthForm
				.bindFromRequest();
		if (mform.hasErrors()) {
			return badRequest(publishKnigoverth.render(Users.getUser(), mat,
					mform));
		}

		Material material = mat == null ? new Material() : mat;
		material.setDate(Application.cal.getTime());
		material.setContent(mform.field(Material.FIELD_CONTENT).value());
		material.setHeader(mform.field(Material.FIELD_HEADER).value());
		if(mat == null)
		material.setType(Material.CLASS_KNIGOVERTH);
		MaterialStatus mStatus = MaterialStatus.findById(Long.parseLong(mform
				.field("status").value()));
		material.setMatStatus(mStatus);
		if(mat == null)
		material.setEditorStatus(MaterialStatus.getVisibleStatus());

		controllers.Status status = Medias.upload(user.getId(),
				Material.FIELD_IMAGE, false);
		if (status.error != null) {
			flash("error", status.error);
			return badRequest(publishKnigoverth.render(Users.getUser(), mat,
					mform));
		}
		
		if(mat == null ||(mat!=null && status.result != null))
		material.setImage(status.result);

		// save books
		if(material.getBooks() !=null && !material.getBooks().isEmpty()) {
			material.getBooks().clear();
			material.update();
		}
		for (int i = 1; i < 4; i++) {
			
			String bookName = mform.field("bookName" + i).value();
			String bookAuthor = mform.field("bookAuthor" + i).value();
			String bookGenre = mform.field("bookGenre" + i).value();
			
			if (bookAuthor == null || bookAuthor.trim().length() == 0
					|| bookName == null || bookName.trim().length() == 0) {
				continue;
			}

			Book book = saveBook(bookName, bookAuthor, bookGenre);
			CacheUtils.addBook(material, book);
		}

		// save authors
		if(material.getAuthors() !=null && !material.getAuthors().isEmpty()) {
			material.getAuthors().clear();
			material.update();
		}
		for (int i = 1; i < 4; i++) {
			String authorName = mform.field("author" + i).value();
			if (authorName == null || authorName.trim().length() == 0)
				continue;
			Author author = Author.findByName(authorName);
			if (author == null) {
				author = new Author();
				author.setName(authorName);
				author.save();
			}
			CacheUtils.addAuthor(material, author);
		}

		// save genres
		if(material.getGenres() !=null && !material.getGenres().isEmpty()) {
			material.getGenres().clear();
			material.update();
		}
		for (int i = 1; i < 4; i++) {
			String genreName = mform.field("genre" + i).value();
			if (genreName == null || genreName.trim().length() == 0)
				continue;
			Genre genre = Genre.findById(Long.parseLong(genreName));
			CacheUtils.addGenre(material, genre);
		}

		if(mat == null) {
		user = Users.getUser();
		material.setOwner(user);
		material.save();
		user.addMaterial(material);
		user.update();
		} else {
			material.update();
		}
		return ok(profile.render(Users.getUser()));
	}

	/**
	 * 
	 * uid - id того, кто публикует
	 * 
	 */
	public static Result processPublishAffiche(Long uid, Long mid) {
		User user = User.findById(uid);
		Result res = accessEdit("processPublishAffiche", user);
		if (res != null)
			return res;

		Material mat = mid >=0? Material.findById(mid) : null;
		
		Form<PublishAfficheForm> mform = sPublishAfficheForm.bindFromRequest();
		if (mform.hasErrors()) {
			return badRequest(publishAffiche.render(Users.getUser(), mat,
					mform));
		}

		String strDate = mform.field("date").value();
		String strTime = mform.field("time").value();
		Logger.trace("processPublishReview: date from form: " + strDate);
		Logger.trace("processPublishReview: time from form: " + strTime);
		
		String[] splittedDate = strDate.split("/");
		String strDateTime = splittedDate[2] + "/" + splittedDate[1] + "/" + 
				(splittedDate[0].length() == 2 ? "20" + splittedDate[0] : splittedDate[0]) + " " + strTime;
		Logger.trace("processPublishAffiche: date: " + strDate + ", time: "
				+ strTime + ", for parse: " + strDateTime);
		Date startDateTime = null;
		
		try {
			startDateTime = Application.materialFormatterFromPublish
					.parse(strDateTime);
			Logger.trace("processPublishReview: parsed date and time: " + strDateTime);
		} catch (ParseException e) {
			flash("error", "Can't pasre datas");
			Logger.error("Can't parse data: " + strDateTime);
			return badRequest(publishAffiche.render(Users.getUser(), null,
					mform));
		}
		Material material = mat == null ? new Material() : mat;
		material.setDate(Application.cal.getTime());
		Logger.trace("processPublishAffiche: parsed date and time: "
				+ startDateTime);

		material.setStartDate(startDateTime);
		material.setContent(mform.field(Material.FIELD_CONTENT).value());
		material.setHeader(mform.field(Material.FIELD_HEADER).value());
		material.setType(Material.CLASS_AFFICHE);
		MaterialStatus mStatus = MaterialStatus.findById(Long.parseLong(mform
				.field("status").value()));
		material.setMatStatus(mStatus);
		if(mat == null)
		material.setEditorStatus(MaterialStatus.getVisibleStatus());

		controllers.Status status = Medias.upload(user.getId(),
				Material.FIELD_IMAGE, false);
		if (status.error != null) {
			flash("error", status.error);
			return badRequest(publishAffiche.render(Users.getUser(), null,
					mform));
		}
		if(mat == null ||(mat!=null && status.result !=null))
		material.setImage(status.result);

		if(mat == null) {
		material.setOwner(user);
		material.save();
		user.addMaterial(material);
		user.update();
		} else {
			mat.update();
		}
		return ok(profile.render(user));
	}

	public static PagingList<Material> getPagingListByMaterialsClass(Long uid,
			String className) {
		if (className == null)
			return null;
		PagingList<Material> pagingList = null;
		if (className.equals(Material.CLASS_ALL)) {
			pagingList = Material.allInv(Application.getPagesSize());
		} else if (className.equals(Material.CLASS_REVIEW_STUDENT)) {
			pagingList = Material.allReviewsFromStudents(Application
					.getPagesSize());
		} else if (className.equals(Material.CLASS_REVIEW_PROFESSOR)) {
			pagingList = Material.allReviewsFromProfessors(Application
					.getPagesSize());
		} else if (className.equals(Material.CLASS_USER)) {
			User user = uid < 0 ? Users.getUser() : User.findById(uid);
			Band band = new Band(user.getCachedBand());
			return band.getPagingList();
		} else if (className.equals(Material.CLASS_UNVIEWED_COMMENTS)) {
			User user = uid < 0 ? Users.getUser() : User.findById(uid);
			pagingList = getMaterialsWithUnviewedCommentsPagingList(user);
		} else if (className.equals(Material.CLASS_ALL_UNVISIBLE)) {
			pagingList = Material.allInvisible(Application.getPagesSize());
		} else if (className.equals(Material.CLASS_MY)) {
			User user = uid < 0 ? Users.getUser() : User.findById(uid);
			pagingList = Material
					.allWithOwner(user, Application.getPagesSize());
		} else if (className.equals(Material.CLASS_ALL_UNVISIBLE)) {
			pagingList = Material.allInvisible(Application.getPagesSize());
		} else if (className.equals(Material.CLASS_USER_DRAFT)) {
			User user = uid < 0 ? Users.getUser() : User.findById(uid);
			pagingList = Material.allMyDraft(user, Application.getPagesSize());
		} else if (className.equals(Material.CLASS_IGO)) {
			User user = uid < 0 ? Users.getUser() : User.findById(uid);
			pagingList = Material.allIgo(user, Application.getPagesSize());
		} else {
			pagingList = Material.allByClass(Application.getPagesSize(),
					className);
		}
		return pagingList;
	}

	protected static PagingList<Material> getMaterialsWithUnviewedCommentsPagingList(
			User user) {
		List<Long> list = CacheUtils.getList(user.getCachedUnviwedComments());
		return Material.find.order().desc(Material.FIELD_ID).where()
				.eq("editorStatus", MaterialStatus.getVisibleStatus())
				.eq("matStatus", MaterialStatus.getPublishedStatus())
				.in("comments.id", list)
				.findPagingList(Application.getPagesSize());
	}

	public static Result getMaterialsByClassPagesCount(Long uid,
			String className) {
		PagingList<Material> pagingList = getPagingListByMaterialsClass(uid,
				className);
		if (pagingList == null)
			return ok("0");
		return ok(pagingList.getTotalPageCount() + "");
	}

	public static Result getMaterialsByClassPage(Long uid, String className,
			Integer id) {
		PagingList<Material> pagingList = getPagingListByMaterialsClass(uid,
				className);
		if (pagingList == null || id < 1)
			return ok();
		Page<Material> localPage = pagingList.getPage(id - 1);
		List<Material> list = localPage.getList();
		User user = User.findById(uid);
		return ok(materialsPage.render(user, list));
	}

	public static Result materialsByClass(Long uid, String className) {
		User user = User.findById(uid);
		if (uid < 0)
			user = Users.getUser();
		return ok(materialsByClass.render(user, className, null, null, null,
				null, null, null));
	}

	public static PagingList<Material> getPagingListByMaterialField(Long uid,
			String fieldName, Long fieldId) {
		String actualField = null;
		if (fieldName == null || fieldId < 0)
			return null;
		if (fieldName.equals("user")) {
			actualField = "owner.id";
		} else if (fieldName.equals("book")) {
			actualField = "books.id";
		} else if (fieldName.equals("genre")) {
			actualField = "books.genre.id";
			List<Long> genres = CacheUtils.getList(Genre.findById(fieldId)
					.getCachedGenres());
			genres.add(fieldId);
			return Material.find.order(Material.FIELD_ID).where()
					.in(actualField, genres)
					.findPagingList(Application.getPagesSize());
		} else if (fieldName.equals("author")) {
			actualField = "books.author.id";
		} else if (fieldName.equals("college")) {
			actualField = "owner.college.id";
		}
		if (actualField == null)
			return null;
		return Material.find.order(Material.FIELD_ID).where()
				.eq(actualField, fieldId)
				.findPagingList(Application.getPagesSize());
	}

	public static Result getMaterialsCustomByFieldPagesCount(Long uid,
			String fieldName, Long fieldId) {
		PagingList<Material> pagingList = getPagingListByMaterialField(uid,
				fieldName, fieldId);
		if (pagingList == null)
			return ok("0");
		return ok(pagingList.getTotalPageCount() + "");
	}

	public static Result getMaterialsCustomByFieldPage(Long uid,
			String fieldName, Long fieldId, Integer id) {
		PagingList<Material> pagingList = getPagingListByMaterialField(uid,
				fieldName, fieldId);
		if (pagingList == null || id < 1)
			return ok();
		Page<Material> localPage = pagingList.getPage(id - 1);
		List<Material> list = localPage.getList();
		return ok(materialsPage.render(User.findById(uid), list));
	}

	public static Result materialsByOwner(Long id) {
		User user = Users.getUser();
		User object = User.findById(id);
		List<User> list = new ArrayList<User>(1);
		list.add(object);
		return ok(materialsByClass.render(user, Material.CLASS_CUSTOM, list,
				null, null, null, null, null));
	}

	public static Result materialsByGenre(Long id) {
		User user = Users.getUser();
		Genre object = Genre.findById(id);
		List<Genre> list = new ArrayList<Genre>(1);
		list.add(object);
		return ok(materialsByClass.render(user, Material.CLASS_CUSTOM, null,
				list, null, null, null, null));
	}

	public static Result materialsByAuthor(Long id) {
		User user = Users.getUser();
		Author object = Author.findById(id);
		List<Author> list = new ArrayList<Author>(1);
		list.add(object);
		return ok(materialsByClass.render(user, Material.CLASS_CUSTOM, null,
				null, list, null, null, null));
	}

	public static Result materialsByCollege(Long id) {
		User user = Users.getUser();
		College object = College.findById(id);
		List<College> list = new ArrayList<College>(1);
		list.add(object);
		return ok(materialsByClass.render(user, Material.CLASS_CUSTOM, null,
				null, null, list, null, null));
	}

	public static Result materialsByBook(Long id) {
		User user = Users.getUser();
		Book object = Book.findById(id);
		List<Book> list = new ArrayList<Book>(1);
		list.add(object);
		return ok(materialsByClass.render(user, Material.CLASS_CUSTOM, null,
				null, null, null, list, null));
	}

	public static Result viewMaterial(Long id) {
		Material material = Material.findById(id);
		User user = Users.getUser();
		return ok(viewMaterial.render(user, material));
	}

	public static Result getIgoUsersCount(Long matId) {
		if (matId < 0)
			return badRequest("Negative material id!");

		Material mat = Material.findById(matId);
		if (mat == null)
			return badRequest("can't find material id with index: " + matId);

		return ok((mat.getCachedIgoUsersCount() == null ? 0 : mat
				.getCachedIgoUsersCount()) + "");
	}

	public static Result getIlikeUsersCount(Long matId) {
		if (matId < 0)
			return badRequest("Negative material id!");

		Material mat = Material.findById(matId);
		if (mat == null)
			return badRequest("can't find material id with index: " + matId);

		return ok((mat.getCachedIgoUsersCount() == null ? 0 : mat
				.getCachedIgoUsersCount()) + "");
	}

	public static Result getMyMaterialsCountA(Long id) {
		return ok(User.findById(id).getMyMaterialsCount() + "");
	}

	public static Result getMyDraftsCountA(Long id) {
		return ok(User.findById(id).getMyDraftsCount() + "");
	}

	public static Result getIgoMaterialsCountA(Long id) {
		return ok(User.findById(id).getCachedIgoMaterialsCount() + "");
	}

	public static Result getUnviewedCommentsCountA(Long id) {
		User user = User.findById(id);
		return ok(user.getCachedUnviwedCommentsCount() + "");
		// return ok(Comment.getUnviewedCommentsCount(User.findById(id))+"");
	}

	/**
	 * 
	 * Пользователь c идентификатором uid редактирует материал с идентификатором
	 * mid Материал имею право редактировать: - редактор - администратор -
	 * пользователь, который является владельцем материала
	 * 
	 **/
	public static Result editMaterial(Long uid, Long mid) {
		Material mat = Material.findById(mid);
		User user = User.findById(uid); // may be not needs
		User auser = Users.getUser();
		Result res = accessEditMat("editMaterial", auser, mat);
		if (res != null)
			return res;

		if (mat.getType().equals(Material.CLASS_AFFICHE)) {
			PublishAfficheForm form = new PublishAfficheForm();
			fillPublishForm(form, mat);
			Logger.trace("editMAterial.affiche: date from DB: " + mat.getStartDate());
			String[] dt = Application.simlpeFormatter.format(mat.getStartDate()).split("/"); // dd/MM/yyyy/hh/mm/ss
			form.date = dt[2].substring(2) + "/" + dt[1] + "/" + dt[0]; // to format yyyy/MM/dd
			form.time = dt[3] + ":" + dt[4]; // to format hh:mm
			Form<PublishAfficheForm> mform = sPublishAfficheForm
					.fill((PublishAfficheForm) form);
			Logger.trace("editMAterial.affiche: form date: " + form.date);
			Logger.trace("editMAterial.affiche: form time: " + form.time);
			return ok(publishAffiche.render(user, mat, mform));
		} else if (mat.getType().equals(Material.CLASS_KNIGOVERTH)) {
			PublishKnigoverthForm form = new PublishKnigoverthForm();
			fillPublishForm(form, mat);

			// fill books
			if (mat.getBooks() != null) {
				int i = 1;
				for (Book book : mat.getBooks()) {
					if (i == 1) {
						form.bookName1 = book.getName();
						form.bookAuthor1 = book.getAuthor().getName();
						form.bookGenre1 = book.getGenre().getId().toString();
					} else if (i == 2) {
						form.bookName2 = book.getName();
						form.bookAuthor2 = book.getAuthor().getName();
						form.bookGenre2 = book.getGenre().getId().toString();
					} else if (i == 3) {
						form.bookName3 = book.getName();
						form.bookAuthor3 = book.getAuthor().getName();
						form.bookGenre3 = book.getGenre().getId().toString();
					}
					i++;
				}
			}

			// fill authors
			if (mat.getAuthors() != null) {
				int i = 1;
				for (Author author : mat.getAuthors()) {
					if (i == 1) {
						form.author1 = author.getName();
					} else if (i == 2) {
						form.author2 = author.getName();
					} else if (i == 3) {
						form.author3 = author.getName();
					}
					i++;
				}
			}

			// fill genres
			if (mat.getGenres() != null) {
				int i = 1;
				for (Genre genre : mat.getGenres()) {
					if (i == 1) {
						form.genre1 = genre.getId().toString();
					} else if (i == 2) {
						form.genre2 = genre.getId().toString();
					} else if (i == 3) {
						form.genre3 = genre.getId().toString();
					}
					i++;
				}
			}

			Form<PublishKnigoverthForm> mform = sPublishKnigoverthForm
					.fill((PublishKnigoverthForm) form);
			
			return ok(publishKnigoverth.render(user, mat, mform));
		} else if (mat.getType().equals(Material.CLASS_REVIEW)) {
			PublishReviewForm form = new PublishReviewForm();
			fillPublishForm(form, mat);

			Book book = mat.getBooks().iterator().next();
			form.bookName = book.getName();
			form.bookAuthor = book.getAuthor().getName();
			form.genre = book.getGenre().getId().toString();

			Form<PublishReviewForm> mform = sPublishReviewForm
					.fill((PublishReviewForm) form);			
			return ok(publishReview.render(user, mat, mform));
		} else if (mat.getType().equals(Material.CLASS_INTERVIEW)) {
			PublishInterviewForm form = new PublishInterviewForm();
			fillPublishForm(form, mat);

			Form<PublishInterviewForm> mform = sPublishInterviewForm
					.fill((PublishInterviewForm) form);
			Logger.trace("editMaterial: interview: status from filled form: " + mform.field("status").value());
			return ok(publishInterview.render(user, mat, mform));
		}

		return badRequest("Internal error");
	}

	public static void fillPublishForm(PublishForm form, Material mat) {
		form.content = mat.getContent();
		form.header = mat.getHeader();
		form.status = mat.getMatStatus().getId().toString();
	}

	@Restrictions({ @And(SecurityRole.ROLE_ADMIN),
			@And(SecurityRole.ROLE_EDITOR) })
	public static Result setVisible(Long uid, Long mid, Boolean isVisible) {
		Material material = Material.findById(mid);
		material.setEditorStatus(isVisible ? MaterialStatus.getVisibleStatus()
				: MaterialStatus.getUnvisibleStatus());
		material.save();
		return ok();
	}

	
	@SubjectPresent
	public static Result getBookInputForm(String bookName, String authorName,
			String genreName) {
		// send clean form
		return ok(views.html.forms.bookForm.render(null,
				Materials.sPublishAfficheForm, bookName, authorName, genreName));
	}

	@SubjectPresent
	public static Result getAuthorInputForm(String authorName) {
		// send clean form
		return ok(views.html.forms.authorForm.render(
				Materials.sPublishAfficheForm, authorName));
	}

	@SubjectPresent
	public static Result getGenreInputForm(String genreName) {
		// send clean form
		return ok(views.html.forms.genreForm.render(
				Materials.sPublishAfficheForm, genreName));
	}

	
}
