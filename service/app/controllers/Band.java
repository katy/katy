package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import models.Author;
import models.Book;
import models.College;
import models.Genre;
import models.Material;
import models.ModelInterface;
import models.User;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagingList;

public class Band {

  private static Map<String, String> transMap;

  private String bandOpts;
  
  public Band(String bandOpts) {
    this.bandOpts = bandOpts;
  }

  public String getUserBand() {
    return bandOpts;
  }

  public Band addId(String className, Long id) {
    bandOpts = addIdToBand(bandOpts, className, id);
    return this;
  }

  public Band removeId(String className, Long id) {
    bandOpts = removeIdFromBand(bandOpts, className, id);
    return this;
  }

  public List<Material> getPage(Integer pageNum) {
    PagingList<Material> pList = getPagingList();
    if(pList == null)
      return null;
    return pList.getPage(pageNum).getList();
  }

  public Integer getPagesCount() {
    PagingList<Material> pList = getPagingList();
    if(pList == null)
      return null;
    return pList.getTotalPageCount();
  }

  public void add(ModelInterface model) {
    if(model instanceof User)
      addId("user", model.getId());
    else if(model instanceof College)
      addId("college", model.getId());
    else if(model instanceof Book)
      addId("book", model.getId());
    else if(model instanceof Author)
      addId("author", model.getId());
    else if(model instanceof Genre)
      addId("genre", model.getId()); 
  }

  public void remove(ModelInterface model) {
    if(model instanceof User)
      removeId("user", model.getId());
    else if(model instanceof College)
      removeId("college", model.getId());
    else if(model instanceof Book)
      removeId("book", model.getId());
    else if(model instanceof Author)
      removeId("author", model.getId());
    else if(model instanceof Genre)
      removeId("genre", model.getId());   
  }

  public PagingList<Material> getPagingList() {
    ExpressionList<Material> exprs = createExpressionList(convertBandStringToMap(bandOpts));
    if(exprs == null)
      return null;
    return  exprs.findPagingList(Application.getPagesSize());
  }

  private static ExpressionList<Material> createExpressionList(Map<String, List<Long>> map) {
    if(map == null || map.size() == 0)
      return null;
    
    ExpressionList<Material> exprs = Material.find.order().desc(Material.FIELD_ID).where();
    if(map.size() > 1) {
      exprs = exprs.disjunction();
    }

    Map<String, String> transMap = getTranslateFiledsMap();
    Iterator<Entry<String, List<Long>>> iter = map.entrySet().iterator();
    while(iter.hasNext()) {
      Entry<String, List<Long>> entry = iter.next();
      if(entry.getKey().equals("genre")) {
        List<Genre> dbGenres = Genre.find.where().in("id", entry.getValue()).findList();
	List<Long> list = new ArrayList<Long>();
	for(Genre genre : dbGenres) {
	  list.addAll(CacheUtils.getList(genre.getCachedGenres()));
          list.add(genre.getId());
        }
        exprs = exprs.in(transMap.get(entry.getKey()), list);
      } else {
        exprs = exprs.in(transMap.get(entry.getKey()), entry.getValue());
      }
    }

    if(map.size() > 1) {
      exprs = exprs.endJunction();
    }
    return exprs;
  }

  public static Map<String, String> getTranslateFiledsMap() {
    if(transMap == null) {
      transMap = new HashMap<String, String>();
      transMap.put("user","owner.id");
      transMap.put("college","owner.college.id");
      transMap.put("book","books.id");
      transMap.put("author","books.author.id");
      transMap.put("genre","books.genre.id");
    }
    return transMap;
  }

  private static Map<String, List<Long>> convertBandStringToMap(String bandOpts) {
    if(bandOpts == null || bandOpts.length() == 0)
      return new HashMap<String, List<Long>>();
    Map<String, List<Long>> map = new HashMap<String, List<Long>>();
    for(String bandField : bandOpts.split(";")) {
      String[] fparams = bandField.split("=");
      List<Long> ids = new ArrayList<Long>();
      map.put(fparams[0], ids);
      for(String strId : fparams[1].split(",")) {
    	  ids.add(Long.parseLong(strId));
      }
    }
    return map;
  }	
  
  private static String convertBandMapToString(Map<String, List<Long>> map) {
    String bandOpts = "";
	if(map == null || map.size() < 1) 
	  return bandOpts;
	Iterator<Entry<String, List<Long>>> entries = map.entrySet().iterator();
	int i = 0;
	while(entries.hasNext()) {
	  if(i != 0)
		bandOpts += ";";
	  Entry<String, List<Long>> entry = entries.next();
	  bandOpts += entry.getKey()+"=";
	  List<Long> ids = entry.getValue();
	  int j = 0;
	  Iterator<Long> idsIter = ids.iterator();
	  while(idsIter.hasNext()) {
	    if(j != 0)
		  bandOpts += ",";
	    bandOpts += idsIter.next();
		j++;
	  }
	  i++;
	}
	return bandOpts;
  }
  
  private static String addIdToBand(String bandOpts, String className, Long id) {
	Map<String, List<Long>> map = convertBandStringToMap(bandOpts);
	List<Long> ids = map.get(className);
	if(ids == null) {
	  ids = new ArrayList<Long>();
	  map.put(className, ids);
	} 
	if(!ids.contains(id))
		ids.add(id);
	return convertBandMapToString(map);
  }
  
  private static String removeIdFromBand(String bandOpts, String className, Long id) {
	Map<String, List<Long>> map = convertBandStringToMap(bandOpts);
	List<Long> ids = map.get(className);
	ids.remove(id);
	if(ids.isEmpty()) {
		map.remove(className);
	}
	return convertBandMapToString(map);
  }	

}


