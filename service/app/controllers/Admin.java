package controllers;

import java.util.List;

import models.College;
import models.Genre;
import models.Param;
import models.SecurityRole;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.admin;
import views.html.adminColleges;
import views.html.adminGenres;
import views.html.adminPages;
import views.html.adminParams;
import views.html.adminTests;
import views.html.adminUsers;
import views.html.base.collegesPage;
import views.html.base.genresPage;
import views.html.base.pagesPage;
import views.html.base.paramsPage;
import views.html.base.usersPage;
import be.objectify.deadbolt.java.actions.Restrict;

import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;

public class Admin extends Controller {

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result admin() {
		return ok(admin.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result adminUsers() {
		return ok(adminUsers.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result adminPages() {
		return ok(adminPages.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result adminParams() {
		return ok(adminParams.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result adminTests() {
		return ok(adminTests.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result adminColleges() {
		return ok(adminColleges.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result adminGenres() {
		return ok(adminGenres.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result setUserEditor(Long id, Boolean isEditor) {
		User user = User.findById(id);
		if (user == null) {
			return badRequest("Can't find user");
		}
		if (isEditor) {
			user.addEditorRole();
		} else {
			user.removeEditorRole();
		}
		user.update();
		return ok();
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getGenresPagesCount() {
		return ok(Genre.allPages(Application.getPagesSize())
				.getTotalPageCount() + "");
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getGenresPage(Integer page) {
		PagingList<Genre> pagingList = Genre.allPages(Application
				.getPagesSize());
		Page<Genre> localPage = pagingList.getPage(page - 1);
		List<Genre> list = localPage.getList();
		return ok(genresPage.render(list));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getPagesCount() {
		return ok(models.Page.allPages(Application.getPagesSize())
				.getTotalPageCount() + "");
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getPagesPage(Integer page) {
		PagingList<models.Page> pagingList = models.Page.allPages(Application
				.getPagesSize());
		Page<models.Page> localPage = pagingList.getPage(page - 1);
		List<models.Page> list = localPage.getList();
		return ok(pagesPage.render(list));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getUsersPagesCount(String userType) {
		return ok(User.allPages(userType, Application.getPagesSize())
				.getTotalPageCount() + "");
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getUsersPage(String userType, Integer page) {
		PagingList<User> pagingList = User.allPages(userType,
				Application.getPagesSize());
		Page<User> localPage = pagingList.getPage(page - 1);
		List<User> list = localPage.getList();
		return ok(usersPage.render(list));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getParamsPagesCount() {
		return ok(Param.allPages(Application.getPagesSize())
				.getTotalPageCount() + "");
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getParamsPage(Integer page) {
		PagingList<Param> pagingList = Param.allPages(Application
				.getPagesSize());
		Page<Param> localPage = pagingList.getPage(page - 1);
		List<Param> list = localPage.getList();
		return ok(paramsPage.render(list));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getCollegesPagesCount() {
		return ok(College.allPages(Application.getPagesSize())
				.getTotalPageCount() + "");
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getCollegesPage(Integer page) {
		PagingList<College> pagingList = College.allPages(Application
				.getPagesSize());
		Page<College> localPage = pagingList.getPage(page - 1);
		List<College> list = localPage.getList();
		return ok(collegesPage.render(list));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeParamDescr(Long id, String newValue) {
		if (id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}

		if (newValue == null || newValue.length() == 0) {
			flash("error", "Description can't be null");
			return badRequest();
		}

		Param param = Param.findById(id);
		if (param == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}

		Application.getParams().remove(param.getName());

		param.setDescr(newValue);
		param.update();

		return ok();
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeParamValue(Long id, String newValue) {
		if (id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}
		if (newValue == null)
			newValue = "";

		Param param = Param.findById(id);
		if (param == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}

		Application.getParams().remove(param.getName());

		param.setValue(newValue);
		param.update();

		return ok();
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeParamName(Long id, String newValue) {
		if (id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}

		if (newValue == null || newValue.length() == 0) {
			flash("error", "Name can't be null");
			return badRequest();
		}

		Param param = Param.findById(id);
		if (param == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}

		Application.getParams().remove(param.getName());

		param.setName(newValue);
		param.update();

		return ok();
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeCollegeName(Long id, String newValue) {
		if (id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}

		if (newValue == null || newValue.length() == 0) {
			flash("error", "Name can't be null");
			return badRequest();
		}

		College college = College.findById(id);
		if (college == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}

		college.setName(newValue);
		college.update();

		return ok();
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeGenreName(Long id, String newValue) {
		if (id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}

		if (newValue == null || newValue.length() == 0) {
			flash("error", "Name can't be null");
			return badRequest();
		}

		Genre genre = Genre.findById(id);
		if (genre == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}

		genre.setName(newValue);
		genre.update();

		return ok();
	}

}
