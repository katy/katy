package controllers;

import models.User;
import models.form.EditPageForm;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.adminPages;
import views.html.editPage;
import views.html.viewPage;

public class Pages extends Controller {

	public final static Form<EditPageForm> sEditPageForm = Form
			.form(EditPageForm.class);

	public static Result accessEdit(String msg, User user) {
		if (user == null) {
			Logger.error(msg + ": van't find authorized user!");
			return badRequest("Can't find authorized user!");
		}
		if (!user.isEditorAdmin()) {
			Logger.error(msg + ": user must be admin or editor");
			return badRequest("user must be admin or editor");
		}
		return null;
	}

	public static Result pageAction(String msg, User user, models.Page page) {
		Result result = accessEdit("remove", user);
		if (result != null)
			return result;
		if (page == null) {
			Logger.error(msg + ": Can't find page");
			return badRequest("Не могу найти страницу");
		}
		return null;
	}

	public static Result page(Long pid) {
		models.Page page = models.Page.findById(pid);
		if (page == null) {
			Logger.error("page: Can't find page with id: " + pid);
			return badRequest("Не могу найти страницу");
		}
		return ok(viewPage.render(page));
	}

	public static Result edit(Long pid) {
		User user = Users.getUser();
		models.Page page = models.Page.findById(pid);
		Result result = pageAction("edit", user, page);
		if (result != null)
			return result;

		EditPageForm form = new EditPageForm();
		form.header = page.getHeader();
		form.content = page.getContent();
		Form<EditPageForm> mform = sEditPageForm.fill(form);

		return ok(editPage.render(Users.getUser(), page, mform));
	}

	public static Result create() {
		User user = Users.getUser();
		Result result = accessEdit("create", user);
		if (result != null)
			return result;
		return ok(editPage.render(user, null, sEditPageForm));
	}

	public static Result processEdit(Long pid) {
		User user = Users.getUser();
		Result result = accessEdit("processEdit", user);
		if (result != null)
			return result;

		models.Page page = null;
		if (pid > 0) {
			page = models.Page.findById(pid);
			if (page == null) {
				Logger.error("page: Can't find page with id: " + pid);
				return badRequest("Не могу найти страницу");
			}
		}

		Form<EditPageForm> mform = sEditPageForm.bindFromRequest();
		if (mform.hasErrors()) {
			return badRequest(editPage.render(Users.getUser(), page, mform));
		}

		if (page == null)
			page = new models.Page();

		page.setContent(mform.field(models.Page.FIELD_CONTENT).value());
		page.setHeader(mform.field(models.Page.FIELD_HEADER).value());
		page.save();
		return ok(adminPages.render());
	}

	public static Result setVisible(Long pid, Boolean visible) {
		User user = Users.getUser();
		models.Page page = models.Page.findById(pid);
		Result result = pageAction("setVisible", user, page);
		if (result != null)
			return result;
		page.setVisible(visible);
		page.update();
		return ok();
	}

	public static Result remove(Long pid) {
		User user = Users.getUser();
		models.Page page = models.Page.findById(pid);
		Result result = pageAction("remove", user, page);
		if (result != null)
			return result;
		page.delete();
		return ok();
	}
}
