package controllers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import models.Media;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import util.Helper;

import com.google.common.net.MediaType;

/**
*
* Управление мультимедийным контентом
*
* Медиа контент не связан ни с одной сущностью кроме как с владельцем
*
*/
public class Medias extends Controller {

	/**
	*
	* Загружает картинку и возвращает относительную ссылку или описание ошибки в виде Status 
	* - userId - id того, кто должен будет владеть картинкой
	* - Users.getUser() - тот, кто выполняет данное действие
	* - fieldName - поле в форме, которое содержит адресс картинки
	*
	*/
	public static controllers.Status upload(Long ownerId, String fieldName, boolean isNeed) {
		if(ownerId == null) {
			Logger.error("uploadImage: ownerId is null!");
			return new controllers.Status("Не указан идентификатор владелеца медиа контента", null);
		}

		if(ownerId < 0 ) {
			Logger.error("uploadImage: ownerId is negative: " + ownerId);
			return new controllers.Status("Указан не корректный идентификатор владелеца медиа контента", null);
		}

		if(fieldName == null) {
			Logger.error("uploadImage: fieldName is nukll");
			return new controllers.Status("Не указано поле с аддрессом медиа контента.", null);
		}
	
	        MultipartFormData body = request().body().asMultipartFormData();
	        FilePart picture = body.getFile(fieldName);

	        if(picture == null) {
			if(isNeed) {
				Logger.error("uploadImage: picture is null");
		                return new controllers.Status("Невозможно получить медиа контент по указанному полю.", null);
			} 
			return new controllers.Status(null, null);
	        }

		String contentType = picture.getContentType();
	        String extension = getExtensionByContentType(contentType);

        	if(extension == null) {
	                Logger.error("uploadImage: not supported content type: " + contentType);
	                return new controllers.Status("Не поддерживаемый тип медиа контента." + contentType, null);
	        }

        	File file = picture.getFile();
	        if(file == null) {
	                Logger.error("uploadImage: source file is null ");
	                return new controllers.Status("Невозможно получить загруженный файл", null);
	        }

		if(file.length() > Application.getPictureSize()) {
                        Logger.error("uploadImage: source file too large:  " + file.length() + " bytes, and limit: " + Application.getPictureSize() + " Kb");
                        return new controllers.Status("Загружаемый файл имеет размер более " + Application.getPictureSize() + " байт.", null);
		}
	
		models.Media media = new models.Media();
		media.setType(contentType);
		media.save();

		String fileName = media.getId() + "." + extension;
		String localPath = getLocalPath(fileName);

        	try {
	                File localPathDir = new File(getLocalDir());
                        if(!localPathDir.exists()) { 
				localPathDir.mkdirs();
			}
                        Helper.copyFile(file, new File(localPath));
                } catch (IOException e) {
                        Logger.error("uploadImage: problems with file uploading from " + file.getPath() + " to " + localPath );
			media.delete();
	                return new controllers.Status("Не могу загрузить файл", null);
                }
		
		Logger.trace("uploadImage: starting to save media content with name: " + fileName);
		media.setName(fileName);
		media.update();
		controllers.Status status = new controllers.Status(null, getAssetPath(fileName));
		status.object = media;
		return status;
	}

	public static String getBITypeByCT(String ct) {
		ct = ct.toUpperCase();
		if(ct.equals("IMAGE/PNG")) {
			return "png";
		} else if(ct.equals("IMAGE/JPEG")) {
			return "jpg";
		} else if(ct.equals("IMAGE/GIF")) {
			return "gif";
		}
		Logger.trace("get buffered image type by content type: not implemented for: " + ct);
		return null;
	}

	public static String getExtensionByContentType(String contentType) {
	        String extension = null;
		if(contentType != null) {
		        if(contentType.equals(MediaType.JPEG.toString()))
		                extension = "jpg";
		        if(contentType.equals(MediaType.PNG.toString()))
		                extension = "png";
		        if(contentType.equals(MediaType.BMP.toString()))
		                extension = "bmp";
		        if(contentType.equals(MediaType.GIF.toString()))
		                extension = "gif";
		}
		return extension;
	}

        public static String getAssetPath(String fileName) {
                return Application.getImagesPath() + fileName;
        }

	public static String getLocalDir() {
                return "public/" + Application.getImagesPath();
	}

        public static String getLocalPath(String fileName) {
                return getLocalDir() + fileName;
        }

	/**
	*
	* Обрезает картинку до квадратных размеров
	*
	*/
	public static void crop(Media media) {
		String ct = getBITypeByCT(media.getType());
		if(ct == null) {
			return;
		}
		String path = getLocalPath(media.getName());
		File file = new File(path);
		if(!file.exists()) {
			Logger.error("crop: file not exists " + path);
		}
		try {
			BufferedImage img = ImageIO.read(file);
		    	int width = img.getWidth();
		    	int height = img.getHeight();
		    	if(width != height) {
		    		int size = width < height ? width : height;
				Logger.trace("crop: image from " + width + "x" + height + " to " + size+"x" +size );
		    		BufferedImage newImg = img.getSubimage((width-size)/2, (height-size)/2, size, size);
		    		ImageIO.write(newImg, ct, file);
		    	}
		} catch (IOException e) {
			Logger.error("crop: exception: " + e);
		}
	}

}


