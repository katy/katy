package controllers;

import java.util.Iterator;
import java.util.List;

import models.Comment;
import models.Material;
import models.User;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.base.commentPage;
import views.html.base.commentsPage;
import be.objectify.deadbolt.java.actions.SubjectPresent;

public class Comments extends Controller {

	/**
	 * 
	 * uid - id того, для кого проводится операция
	 * 
	 */
	@SubjectPresent
	public static Result setCommentViewed(Long uid, Long cid) {
		if (cid < 0) {
			Logger.error("setCommentViewed: comment id  is negative: " + cid);
			return badRequest("Comment id is negative!");
		}

		User auser = Users.getUser();
		Comment comment = Comment.findById(cid);
		if (comment == null) {
			Logger.error("setCommentViewed: Can't find comment with id: " + cid);
			return badRequest("Can't find comment with id: " + cid);
		}

		if ((auser.getId() != comment.getMaterial().getOwner().getId())
				&& !(comment.getComment() != null && comment.getComment()
						.getOwner().getId() == auser.getId())) {
			Logger.error("Acces denied in setCommentViewed: auth user: "
					+ auser.getId() + ", material owner: "
					+ comment.getMaterial().getOwner().getId()
					+ ", comment owner: "
					+ comment.getComment().getOwner().getId());
			return badRequest("Access denied!");
		}

		comment.getUnviewedUsers().remove(auser);
		auser.getUnviwedComments().remove(comment);

		auser.setCachedUnviwedCommentsCount(auser
				.getCachedUnviwedCommentsCount() - 1);
		auser.setCachedUnviwedComments(CacheUtils.setId(
				auser.getCachedUnviwedComments(), comment.getId(), false));
		auser.update();
		comment.update();

		return ok();
	}

	public static Result getCommentsPage(Long userId, Long matId, Integer index) {
		Material material = Material.getById(matId);

		if (material == null) {
			Logger.error("getCommentsPage: Can't find material with id: "
					+ matId);
			return badRequest("Can't find material with id: " + matId);
		}

		User user = User.findById(userId);
		if (user == null) {
			Logger.error("getCommentsPage: Can't find user with id: " + userId);
			return badRequest("Can't find user with id: " + userId);
		}

		return ok(commentsPage.render(user,
				Comment.getCommentsTree(user, material)));

	}

	public static Result getCommentsPageSize(Long matId) {
		Material material = Material.getById(matId);

		if (material == null) {
			Logger.error("getCommentsPageSize: Can't find material with id : "
					+ matId);
			return badRequest("Can't find material");
		}
		return ok(material.getCommentsPageSize().toString());
	}

	@SubjectPresent
	public static Result remove(Long cid) {
		User user = Users.getUser();
		if(!(user.isAdmin() || user.isEditor())) {
			Logger.error("removeComment: access denied for user with id: " + user.getId() + " for remove comment with id " + cid);
			return badRequest("access denied");
		}

		Comment comment = Comment.findById(cid);
		if(comment == null) {
			Logger.error("removeComment: can't find comment with id: " + cid);
			return badRequest("Comment not exists!");
		}
		
		Logger.trace("removeComment: try to remove comment with id: " + cid + " by user with id " + user.getId());
		removeRecursive(comment, comment.getMaterial());
		Logger.trace("removeComment: comment remove operation finished");
		return ok();
	}
	
	protected static void removeRecursive(Comment comment, Material mat) {
		Logger.error("removeComment: removeRecursive started for comment: " + comment.getId());
		// remove from parent comment container if it exists
		Comment parentComment = comment.getComment();
		if(parentComment != null) {
			Iterator<Comment> chIter = comment.getChilds().iterator();
			while(chIter.hasNext()) {
				Comment chComment = chIter.next();
				if(chComment.getId() == comment.getId()) {
					chIter.remove();
					comment.setParent(null);
					parentComment.update();
					break;
				}
			}
		}
		
		// remove childrens
		Iterator<Comment> chIter = comment.getChilds().iterator(); 
		while(chIter.hasNext()) {
			Comment childComment = chIter.next();
			removeRecursive(childComment, mat);
		}
		
		// remove from unviewed 
		for(User uvUser : comment.getUnviewedUsers()) {
			Iterator<Comment> commIter = uvUser.getUnviwedComments().iterator(); 
			while(commIter.hasNext()) {
				Comment com = commIter.next(); 
				if(com.getId() == comment.getId()) {
					Logger.trace("comments.remove: remove unviewed comment  " + comment.getId() + " from user: " + uvUser.getId());
					commIter.remove();

					Logger.trace("comments.remove: cached unviewed comments before: " + uvUser.getCachedUnviwedComments());
					Logger.trace("comments.remove: cached unviewed comments count before: " + uvUser.getCachedUnviwedCommentsCount());

					String str = uvUser.getCachedUnviwedComments();
					List<Long> longs = CacheUtils.getList(str);
					String nstr = null;
					for(int i=0; i<longs.size(); i++) {
						if(comment.getId() != longs.get(i)) {
							if(nstr == null)
								nstr = longs.get(i) + "";
							else 
								nstr += "," + longs.get(i);
						}
					}
					uvUser.setCachedUnviwedComments(nstr);
					uvUser.setCachedUnviwedCommentsCount(uvUser.getCachedUnviwedCommentsCount()-1);
					uvUser.update();
					Logger.trace("comments.remove: cached unviewed comments after: " + uvUser.getCachedUnviwedComments());
					Logger.trace("comments.remove: cached unviewed comments count after: " + uvUser.getCachedUnviwedCommentsCount());		
					break;
				}
			}
		}		
		
		// remove from material
		Iterator<Comment> cIter = mat.getComments().iterator();
		while(cIter.hasNext()) {
			Comment mComment = cIter.next();
			if(mComment.getId() == comment.getId()) {
				Logger.trace("comments.remove: remove mat comment  " + comment.getId() + " from material: " + mat.getId());
				cIter.remove();

				Logger.trace("comments.remove: cached mat comments before: " + mat.getCachedComments());
				Logger.trace("comments.remove: cached mat comments count before: " + mat.getCachedCommentsCount());

				String str = mat.getCachedComments();
				
				String[] strings = str == null ? new String[0] : str.split(",");
				
				String nstr = null;
				for(int i=0; i<strings.length; i++) {
					String curString = strings[i];
					String curStrings[] = curString.split(":");
					Long curLong = Long.parseLong(curStrings[0]);
					if(comment.getId() != curLong) {
						if(nstr == null)
							nstr = curString;
						else 
							nstr += "," + curString;
					}
				}
				mat.setCachedComments(nstr);
				mat.setCachedCommentsCount(mat.getCachedCommentsCount()-1);
				mat.update();
				Logger.trace("comments.remove: cached mat comments after: " + mat.getCachedComments());
				Logger.trace("comments.remove: cached mat comments count after: " + mat.getCachedCommentsCount());		
				break;				
			}
		}
		mat.update();
		comment.delete();
	}

	@SubjectPresent
	public static Result sendComment(Long matId, Long commentId, String content) {
		if (content == null || content.length() == 0) {
			Logger.error("sendComment: content can't be null : content: "
					+ content);
			return badRequest("Can't be null");
		}

		Material material = Material.findById(matId);

		if (material == null) {
			Logger.error("sendComment: can't find material: " + matId);
			return badRequest("Can't find material");
		}

		// 1) сохранение комментария

		User user = Users.getUser();
		Comment comment = new Comment();
		comment.setDate(Application.cal.getTime());
		comment.setContent(content);
		comment.setOwner(user);
		comment.setMaterial(material);

		Comment ownerComment = null;
		if (commentId >= 0) {
			ownerComment = Comment.findById(commentId);
			if (ownerComment == null) {
				Logger.error("sendComment: can't find comment with id: "
						+ commentId);
				return badRequest("Can't find comment witg id: " + commentId);
			}
			comment.setComment(ownerComment);
		}

		comment.save();
		user.addMaterial(material);
		material.addComment(comment);
		material.setCachedCommentsCount(material.getCachedCommentsCount() + 1);
		material.setCachedComments(Comment.insertAfter(
				material.getCachedComments(), comment.getId(), commentId));

		comment.save();
		user.update();
		material.update();

		// 2) Оповещение о непрочитанных комментариях владельца материала
		if (material.getOwner().getId() != user.getId()) {
			User owner = material.getOwner();
			owner.addUnviewedComment(comment);
			owner.setCachedUnviwedCommentsCount(owner
					.getCachedUnviwedCommentsCount() == null ? 1 : owner
					.getCachedUnviwedCommentsCount() + 1);
			owner.setCachedUnviwedComments(CacheUtils.setId(
					owner.getCachedUnviwedComments(), comment.getId(), true));
			material.update();
			owner.update();
		}

		// 3) если это комментарий на комментарий, то владельца родительского
		// комментария нужно оповестить
		if (ownerComment != null
				&& ownerComment.getOwner().getId() != user.getId()
				&& material.getOwner().getId() != ownerComment.getOwner()
						.getId()) {
			User pOwner = ownerComment.getOwner();
			pOwner.addUnviewedComment(comment);
			pOwner.setCachedUnviwedCommentsCount(pOwner
					.getCachedUnviwedCommentsCount() == null ? 1 : pOwner
					.getCachedUnviwedCommentsCount() + 1);
			pOwner.setCachedUnviwedComments(CacheUtils.setId(
					pOwner.getCachedUnviwedComments(), comment.getId(), true));
			material.update();
			pOwner.update();
		}

		// TODO:
		/*
		 * Comment ownComment = null; if(commentId != null && commentId != -1) {
		 * ownComment = Comment.findById(commentId); User ownerComment =
		 * ownComment.getOwner(); ownerComment.addUnviewedComment(comment);
		 * ownerComment
		 * .setCachedUnviwedCommentsCount(ownerComment.getCachedUnviwedCommentsCount
		 * ()+1);
		 * ownerComment.setCachedUnviwedComments(CacheUtils.setId(ownerComment
		 * .getCachedUnviwedComments(), comment.getId(), true));
		 */
		return ok(commentPage.render(user, comment, null));
	}

}
