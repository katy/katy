package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.Author;
import models.Book;
import models.Genre;
import models.Material;

public class CacheUtils {

	public static void addBook(Material material, Book book) {
		Set<Book> books = material.getBooks();
		if (books == null) {
			books = new HashSet<Book>();
			material.setBooks(books);
		}
		books.add(book);
	}

	public static void addAuthor(Material material, Author author) {
		Set<Author> authors = material.getAuthors();
		if (authors == null) {
			authors = new HashSet<Author>();
			material.setAuthors(authors);
		}
		authors.add(author);
	}

	public static void addGenre(Material material, Genre genre) {
		Set<Genre> genres = material.getGenres();
		if (genres == null) {
			genres = new HashSet<Genre>();
			material.setGenres(genres);
		}
		genres.add(genre);
	}

	public static boolean isExists(String str, Long id) {
		if (str == null)
			return false;
		String strId = id.toString();
		for (String string : str.split(",")) {
			if (string.equals(strId))
				return true;
		}
		return false;
	}

	public static String setId(String str, Long id, Boolean isAdd) {
		if (isAdd)
			return str == null ? id + "" : str + "," + id;
		String nString = null;
		String strId = id.toString();
		for (String string : str.split(",")) {
			if (!string.equals(strId)) {
				if (nString == null) {
					nString = string;
				} else {
					nString += "," + string;
				}
			}
		}
		return nString;
	}

	public static List<Long> getList(String str) {
		List<Long> list = new ArrayList<Long>();
		if (str != null) {
			for (String strs : str.split(",")) {
				list.add(Long.parseLong(strs));
			}
		}
		return list;
	}

	public static String insertAfter(String str, Long id, Long afterId) {
		String nString = null;
		String strId = id.toString();
		String strAfterId = afterId.toString();
		if (str == null)
			return strId;
		for (String string : str.split(",")) {

			if (string.equals(strAfterId)) {
				if (nString == null) {
					nString = strId + "," + string;
				} else {
					nString += "," + strId + "," + string;
				}
			} else {
				if (nString == null) {
					nString = string;
				} else {
					nString += "," + string;
				}
			}

		}
		return nString;
	}
}
