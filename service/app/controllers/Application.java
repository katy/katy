package controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import models.Param;
import play.Routes;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import be.objectify.deadbolt.java.actions.SubjectPresent;

public class Application extends Controller {

	public static final String ERROR = "error";
	public static final String WARNING = "warning";
	public static final String SUCCESS = "success";

	public static final String PROJECT_NAME = "project_name";
	public static final String PRODUCT_IMAGES_FOLDER = "product_images_folder";
	public static final String PAGES_SIZE = "pages_size";

	public static final String PICTURE_SIZE = "picture_size";

	public static final String UNDEFINED = "undefined";

	private final static Map<String, Param> params = new HashMap<String, Param>();

	public final static SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy-MM-dd");

	public final static SimpleDateFormat commentFormatter = new SimpleDateFormat(
			"yyyy/MM/dd hh:mm");
	public final static SimpleDateFormat materialFormatter = new SimpleDateFormat(
			"yyyy/MM/dd hh:mm");

	public final static SimpleDateFormat materialFormatterFromPublish = new SimpleDateFormat(
			"dd/MM/yyyy hh:mm");
	
	public final static SimpleDateFormat simlpeFormatter = new SimpleDateFormat(
			"dd/MM/yyyy/hh/mm/ss");

	public final static SimpleDateFormat commentShortFormatter = new SimpleDateFormat(
			"yyyy/MM/dd");
	public final static SimpleDateFormat materialShortFormatter = new SimpleDateFormat(
			"yyyy/MM/dd");

	public final static SimpleDateFormat monthFormatter = new SimpleDateFormat(
			"MMM", new Locale("ru"));
	public final static SimpleDateFormat weekDayFormatter = new SimpleDateFormat(
			"EEEE", new Locale("ru"));
	public final static SimpleDateFormat dayFormatter = new SimpleDateFormat(
			"dd");

	public static Calendar cal = Calendar.getInstance();

	public static String getParamValue(String paramName) {
		Param param = params.get(paramName);
		if (param == null) {
			param = Param.findByName(paramName);
			params.put(paramName, param);
		}
		return param.getValue();
	}

	public static void setParamValue(String paramName, String value) {
		Param param = params.get(paramName);
		if (param == null) {
			param = Param.findByName(paramName);
			if (param == null) {
				param = new Param();
				param.name = paramName;
				param.value = value;
				param.save();
			} else {
				param.value = value;
				param.update();
			}
			params.put(paramName, param);
		} else {
			param.setValue(value);
			param.update();
		}
	}

	public static String getProjectName() {
		return getParamValue(PROJECT_NAME);
	}

	public static Integer getPictureSize() {
		return Integer.parseInt(getParamValue(PICTURE_SIZE));
	}

	public static String getImagesPath() {
		return getParamValue(PRODUCT_IMAGES_FOLDER);
	}

	public static Integer getPagesSize() {
		return Integer.parseInt(getParamValue(PAGES_SIZE));
	}

	public static Result index() {

		// MailerAPI mail =
		// play.Play.application().plugin(MailerPlugin.class).email();
		/*
		 * mail.setSubject("simplest mailer test");
		 * mail.setRecipient("some display name <cromlehg@gmail.com>");
		 * mail.setFrom("some dispaly name <somefromadd@email.com>");
		 * mail.send("A text only message",
		 * "<html><body><p>An <b>html</b> message</p></body></html>" );
		 */
		// java.lang.reflect.Method[] methods =
		// MailerAPI.class.getDeclaredMethods();

		// Loop through the methods and print out their names
		// for (java.lang.reflect.Method method : methods) {
		// System.out.println(method.getName());
		// }

		return ok(index.render(Users.getUser()));
	}

	protected static Map<String, models.Param> getParams() {
		return params;
	}


	/* ====================== javascript routers ===================== */

	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes.javascriptRouter("appJsRoutes",
				routes.javascript.Users.login(),

				routes.javascript.Materials.getBookInputForm(),
				routes.javascript.Materials.getAuthorInputForm(),
				routes.javascript.Materials.getGenreInputForm(),

				routes.javascript.Admin.changeParamName(),
				routes.javascript.Admin.changeParamValue(),
				routes.javascript.Admin.changeParamDescr(),

				routes.javascript.Users.changeUserSecondName(),
				routes.javascript.Users.changeUserFirstName(),
				routes.javascript.Users.changeUserMiddleName(),
				routes.javascript.Users.changeUserBirthday(),
				routes.javascript.Users.changeUserStatus(),
				routes.javascript.Users.changeUserCollege(),

				routes.javascript.Users.setListenObject(),

				routes.javascript.Users.getListenObjectsPagesCount(),
				routes.javascript.Users.getListenObjectsPage(),

				routes.javascript.Users.setIgo(), 
				routes.javascript.Users.isIgo(), 
				routes.javascript.Users.setIlike(),
				routes.javascript.Users.isIlike(),

				routes.javascript.Admin.getUsersPagesCount(),
				routes.javascript.Admin.getUsersPage(),

				routes.javascript.Admin.setUserEditor(),
				routes.javascript.Comments.sendComment(),
				routes.javascript.Comments.remove(),

				//routes.javascript.Materials.removeMaterial(),
				routes.javascript.Materials.setVisible(),

				routes.javascript.Admin.getParamsPagesCount(),
				routes.javascript.Admin.getParamsPage(),

				routes.javascript.Admin.getPagesCount(),
				routes.javascript.Admin.getPagesPage(),

				routes.javascript.Admin.getGenresPagesCount(),
				routes.javascript.Admin.getGenresPage(),
				routes.javascript.Admin.getCollegesPagesCount(),
				routes.javascript.Admin.getCollegesPage(),
				routes.javascript.Admin.changeCollegeName(),
				routes.javascript.Admin.changeGenreName(),
				routes.javascript.Pages.remove(), 
				routes.javascript.Pages.setVisible(),

				routes.javascript.Materials.remove(),
				routes.javascript.Materials.getIgoUsersCount(),
				routes.javascript.Materials.getIlikeUsersCount(),
				routes.javascript.Materials.getMaterialsByClassPagesCount(),
				routes.javascript.Materials.getMaterialsByClassPage(),
				routes.javascript.Comments.setCommentViewed(),

				routes.javascript.Comments.getCommentsPageSize(),
				routes.javascript.Comments.getCommentsPage(),

				routes.javascript.Materials
						.getMaterialsCustomByFieldPagesCount(),
				routes.javascript.Materials.getMaterialsCustomByFieldPage(),

				routes.javascript.Materials.getMyMaterialsCountA(),
				routes.javascript.Materials.getMyDraftsCountA(),
				routes.javascript.Materials.getIgoMaterialsCountA(),
				routes.javascript.Materials.getUnviewedCommentsCountA()

		));
	}

}
