package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.jws.soap.SOAPBinding.Use;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.data.format.Formats;
import play.db.ebean.Model;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagingList;

import controllers.Application;
import controllers.Users;

@Entity
@Table(name = "page")
public class Page extends Model implements ModelInterface  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_HEADER = "header";
	

	@Id
	public Long id;

	@NotNull
	@Column(nullable = false)
	public String header;
	
	@NotNull
	@Column(nullable = false)
	@Lob
	public String content;

	public Boolean visible = false;

	public Boolean userView = false;
	public Boolean userEdit = false;
	public Boolean editorView = false;
	public Boolean editorEdit = false;

        public static final Finder<Long, Page> find = new Finder<Long, Page>(Long.class, Page.class);

	public Long getId() {
		return id;
	}		

	public void setId(Long id) {
		this.id = id;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setUserView(Boolean userView) {
		this.userView = userView;
	}

	public void setEditorView(Boolean editorView) {
		this.editorView = editorView;
	}

	public void setEditorEdit(Boolean editorEdit) {
		this.editorEdit = editorEdit;
	}

	public void setUserEdit(Boolean userEdit) {
		this.userEdit = userEdit;
	}

	public Boolean getUserView() {
		return userView;
	}

	public Boolean getEditorView() {
		return editorView;
	}

	public Boolean getUserEdit() {
		return userEdit;
	}

	public Boolean getEditorEdit() {
		return editorEdit;
	}

	public String getHeader() {
		return header;
	}
	
	public void setHeader(String header) {
		this.header = header;
	}

	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

        public static Page getById(Long id) {
                return find.where().eq(FIELD_ID, id).findUnique();
        }

        public static PagingList<Page> allPages(int pageSize) {
                return find.order().desc(FIELD_ID).findPagingList(pageSize);
        }

	public static List<Page> allVisible() {
                return find.where().eq("visible", true).findList();
	}

        public static List<Page> all() {
                return find.all();
        }

        public static Page findById(Long id) {
                return find.where().eq(FIELD_ID, id).findUnique();
        }


}
