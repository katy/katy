package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import play.data.format.Formats;
import play.db.ebean.Model;

@MappedSuperclass
public abstract class AbstractSelectModel extends Model implements ModelInterface  {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_CLASS_NAME = "className";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_VALUE = "value";
	public static final String FIELD_DATE = "date";
		
	@Id
	public Long id;
	
	@NotNull
	@Column(nullable = false)
	public String name;
	
	public String value;
	
	public String descr;

	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date date;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	public String getDescr() {
		return descr; 
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
}
