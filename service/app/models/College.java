package models;

import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.avaje.ebean.PagingList;

@Entity
@Table(name="college")
public class College extends AbstractSelectModel  {

	private static final long serialVersionUID = 1L;
	
	@ManyToMany(mappedBy="listenColleges")
	public Set<User> usersListen;
	
	public Set<User> getUsersListen() {
		return usersListen;
	}

	public void setUsersListen(Set<User> usersListen) {
		this.usersListen = usersListen;
	}
	
	public static final Finder<Long, College> find = new Finder<Long, College>(Long.class, College.class);

	public static List<College> all() {
		return find.all();
	}
	
	public static College findByName(String name) {
		return find.where().eq(FIELD_NAME, name).findUnique();
	}
	
	public static College findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public static String cachedStatusesString;
	
	public static String getCollegesSelect() {
		if(cachedStatusesString == null) {
			List<College> all = all();
			cachedStatusesString = "";
			if(all != null && !all.isEmpty()) {
				cachedStatusesString += "[";
				for(int i=0; i<all.size(); i++) {
					if(i>0)
						cachedStatusesString += ",";
					College college = all.get(i);					
					cachedStatusesString += "{id: '" + college.getId() + "', text: '" + college.getName() + "'}";  
				}
				cachedStatusesString += "],";
			}
		}
		return cachedStatusesString;
	}
	
	public static int getCount() {
		return find.findRowCount();
	}

	
	public static PagingList<College> allPages(int pageSize) {
		return find.order().desc(FIELD_ID).findPagingList(pageSize);
	}

}
