package models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="book")
public class Book extends AbstractSelectModel  {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_AUTHOR = "authors";
	public static final String FIELD_GENRE = "genre";
	
	@ManyToOne
	public Author author;
	
	@ManyToOne
	public Genre genre;
	
	@ManyToMany(mappedBy="listenBooks")
	public Set<User> usersListen;
	
	@ManyToMany(mappedBy="books")
	public Set<Material> objMaterials;
	
	
	public Set<Material> getObjMaterials() {
		return objMaterials;
	}

	public void setObjMaterials(Set<Material> objMaterials) {
		this.objMaterials = objMaterials;
	}
	
	public Set<User> getUsersListen() {
		return usersListen;
	}

	public void setUsersListen(Set<User> usersListen) {
		this.usersListen = usersListen;
	}

	public static final Finder<Long, Book> find = new Finder<Long, Book>(Long.class, Book.class);

	public static Book findByNameAndAuthor(String name, Author author) {
		String prepName = name.trim().toUpperCase().replace(" +", " ");
		return find.where("LOWER(name) = LOWER(\"" + prepName + "\")").where().eq("author", author).findUnique();
	}
	
	public static Book findByName(String name) {
		String prepName = name.trim().toUpperCase().replace(" +", " ");
		return find.where("LOWER(name) = LOWER(\"" + prepName + "\")").findUnique();
		//return find.where().eq(FIELD_NAME, name).findUnique();
	}
	
	public static Book findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	

	public void setAuthor(Author author) {
		this.author = author;
	}
	
	public Author getAuthor() {
		return author;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

}
