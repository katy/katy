package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Collections;
import java.lang.Long;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.Transient;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.data.format.Formats;
import play.db.ebean.Model;
import controllers.Application;
import controllers.CNode;

@Entity
@Table(name="comment")
public class Comment extends Model implements ModelInterface  {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_OWNER = "onwer";
	public static final String FIELD_MATERIAL = "material";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_DATE = "date";
	
	@Id
	public Long id;
	
	@NotNull
	@Column(nullable = false)
	@Lob
	public String content;
	
	@ManyToOne
	@NotNull
	@Column(nullable = false)
	public User owner;
	
	@ManyToOne
	public Material material;

	@OneToMany(mappedBy = "comment")
	public List<Comment> childs;
	
	@ManyToOne
	public Comment comment;
	
	@ManyToMany
	public List<User> unviewedUsers;
	
	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date date;

	public static final Finder<Long, Comment> find = new Finder<Long, Comment>(Long.class, Comment.class);
	
	public List<Comment> getChilds() {
		return childs;
	}

	public void setChilds(List<Comment> childs) {
		this.childs = childs;
	}

	public void addUnviewedUser(User user) {
		if(getUnviewedUsers() == null) {
			unviewedUsers = new ArrayList<User>();
		}
		getUnviewedUsers().add(user);
	}
	
	public List<User> getUnviewedUsers() {
		return unviewedUsers;
	}

	public void setUnviewedUsers(List<User> unviewedUsers) {
		this.unviewedUsers = unviewedUsers;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public String getFormatedDate() {
		return getDate() == null ? null :Application.commentFormatter.format(getDate());	
	}

	public String getFormatedShortDate() {
		return getDate() == null ? null : Application.commentShortFormatter.format(getDate());	
	}
	
    public static Comment findById(Long id) {
        return find.where().eq(FIELD_ID, id).findUnique();
    }


	// -------------------------------------------------------------------------------------------------------------

	@Transient 
	public Comment parent;

	@Transient
	public List<Comment> nested;

	@Transient
	public List<Comment> getNested() {
		if(nested == null)
			return Collections.emptyList();
		return nested;
	}

	public Comment getParent() {
		return parent;
	}

	public void setParent(Comment comment) {
		this.parent = comment;
	}

	public void addNested(Comment comment) {
		if(nested == null) {
			nested = new ArrayList<Comment>();
		} else {
			if(comment.parent == this)
				return;
		}
		nested.add(comment);
		comment.parent = this;
	}

 	public static CNode getDoubleList(String str) {
		CNode node = new CNode();
		if(str != null) {
			for(String lStr : str.split(",")) {
				String[] llStr = lStr.split(":");
				node.indexes.add(Long.parseLong(llStr[0]));
				node.parents.add(Long.parseLong(llStr[1]));
			}
		}
		return node;
	}

    public static String insertAfter(String str, Long index, Long afterIndex) {
		String nString = null;
		String strId = index.toString();
		String strAfterId = afterIndex.toString();
		if(str == null)
			return strId + ":" + afterIndex;
	        if(afterIndex == -1)
        		return str + "," + strId + ":" + strAfterId; 
		for(String stringLocal : str.split(",")) {
			String[] strs = stringLocal.split(":");
			if(strs[0].equals(strAfterId)) {
				if(nString == null) {
					nString = strId + ":" + strAfterId  + "," + stringLocal;
				} else {
					nString += "," + strId + ":" + strAfterId + "," + stringLocal;
				}
			} else {
				if(nString == null) {
					nString = stringLocal;
				} else {
					nString += "," + stringLocal;
				}
			}
		}
		return nString;
    }

	public static boolean insertRight(List<Comment> list, Comment comm, Long parentIndex) {
		for(int i=0; i<list.size(); i++) {
			Comment lComm = list.get(i);
			if(lComm.getId() == parentIndex) {
				lComm.addNested(comm);
				return true;
			}
			if(insertRight(lComm.getNested(), comm, parentIndex))
				return true;
		}
		return false;
	}

	public static List<Comment> getCommentsTree(User user, Material material) {
		String commentsString = material.getCachedComments();
		List<Comment> tree = new ArrayList<Comment>();
		if(commentsString != null) {
			CNode node = getDoubleList(commentsString);
			List<Comment> dbComms = find.where().in("id", node.indexes).findList();
			// check for right sequence
			for(int i=node.indexes.size()-1; i>=0; i--) {
				Long index = node.indexes.get(i);
				Comment comm = null;
				for(int j=0; j<dbComms.size(); j++) {
					Comment lCom = dbComms.get(j);
					if(lCom.getId() == index) {
						comm = lCom;
						break;
					}
				}
				if(!insertRight(tree, comm, node.parents.get(i)))
					tree.add(comm);
			}
		}
		return tree;
	}
	
}
