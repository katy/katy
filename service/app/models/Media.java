package models;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;
import controllers.Application;
import controllers.Medias;

@Entity
@Table(name="media")
public class Media extends Model implements ModelInterface  {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_TYPE = "type";
	public static final String FIELD_DATE = "date";
	
	@Id
	public Long id;

	public Long ownerId;
	
	public String name;

	@NotNull
	@Column(nullable = false)
	public String type;
	
	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long id) {
		this.ownerId = ownerId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static final Finder<Long, Media> find = new Finder<Long, Media>(Long.class, Media.class);
	
	@Override	
	public void delete() {
		(new File(Medias.getLocalPath(name))).delete();
		super.delete();
	}
	
}
