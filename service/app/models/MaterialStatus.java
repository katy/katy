package models;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="material_status")
public class MaterialStatus extends AbstractSelectModel  {

	private static final long serialVersionUID = 1L;
	
	public static final String STATUS_PUBLISHED = "Опубликован"; 
	public static final String STATUS_DRAFT = "Черновик";
	
	public static final String STATUS_UNVISIBLE = "Невидимый";
	public static final String STATUS_VISIBLE = "Видимый"; 
	
	public static final Finder<Long, MaterialStatus> find = new Finder<Long, MaterialStatus>(Long.class, MaterialStatus.class);


	public static List<MaterialStatus> userStatuses;

        public static List<MaterialStatus> getUserStatuses() {
		if(userStatuses == null) {
                	userStatuses = new ArrayList<MaterialStatus>();
			userStatuses.add(getDraftStatus());
			// last element will be default selected
			userStatuses.add(getPublishedStatus());
		}
                return userStatuses;
        }

	public static MaterialStatus createStatus(String name) {
		MaterialStatus status = new MaterialStatus();
		status.name = name;
		status.save();
		return status;
	}

	public static MaterialStatus getOrCreateStatus(String name) {
		MaterialStatus status = findByName(name);
		if (status == null) {
			status = createStatus(name);
		}
		return status;
	}
	
	public static MaterialStatus getDraftStatus() {
		return getOrCreateStatus(MaterialStatus.STATUS_DRAFT);
	}
	
	public static MaterialStatus getPublishedStatus() {
		return getOrCreateStatus(MaterialStatus.STATUS_PUBLISHED);
	}

	public static MaterialStatus getUnvisibleStatus() {
		return getOrCreateStatus(MaterialStatus.STATUS_UNVISIBLE);
	}
	
	public static MaterialStatus getVisibleStatus() {
		return getOrCreateStatus(MaterialStatus.STATUS_VISIBLE);
	}
	
	public static List<MaterialStatus> all() {
		return find.all();
	}
	
	public static MaterialStatus findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}

	public static MaterialStatus findByName(String name) {
		return find.where().eq(FIELD_NAME, name).findUnique();
	}
	
	public static String cachedStatusesString;
	
	public static String getStatusesSelect() {
		if(cachedStatusesString == null) {
			List<MaterialStatus> all = all();
			cachedStatusesString = "";
			if(all != null && !all.isEmpty()) {
				cachedStatusesString += "[";
				for(int i=0; i<all.size(); i++) {
					if(i>0)
						cachedStatusesString += ",";
					MaterialStatus status = all.get(i);					
					cachedStatusesString += "{id: \"" + status.getId() + "\", text: \"" + status.getName() + "\"}";  
				}
				cachedStatusesString += "],";
			}
		}
		return cachedStatusesString;
	}

	
}
