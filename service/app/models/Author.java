package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;

@Entity
@Table(name="author")
public class Author extends Model implements ModelInterface  {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_AUTHOR = "author";
	public static final String FIELD_BOOKS = "books";
	public static final String FIELD_VERSES = "verses";
	public static final String FIELD_DATE = "date";
	
	@Id
	public Long id;
	
	@NotNull
	@Column(nullable = false)
	public String name;

	@OneToMany(mappedBy = "author")
	public Set<Book> books;
	
	@ManyToMany(mappedBy="listenAuthors")
	public Set<User> usersListen;
	
	@ManyToMany(mappedBy="authors")
	private Set<Material> objMaterials;
	
	
	public Set<Material> getObjMaterials() {
		return objMaterials;
	}

	public void setObjMaterials(Set<Material> objMaterials) {
		this.objMaterials = objMaterials;
	}
	
	public Set<User> getUsersListen() {
		return usersListen;
	}

	public void setUsersListen(Set<User> usersListen) {
		this.usersListen = usersListen;
	}
	
	public static final Finder<Long, Author> find = new Finder<Long, Author>(Long.class, Author.class);
	
	public static Author findByName(String name) {
		String prepName = name.trim().toUpperCase().replace(" +", " ");
		return find.where("LOWER(name) = LOWER(\"" + prepName + "\")").findUnique();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	public void addBook(Book book) {
		if(books == null)
			books = new HashSet<Book>();
		if(!books.contains(book))
			books.add(book);
	}

	public static Author findById(Long id) {	
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	
}
