package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name="banner")
public class Banner extends Model implements ModelInterface  {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_LINK = "link";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_WIDTH = "width";
	public static final String FIELD_HEIGHT = "height";
	public static final String FIELD_DATE = "date";
	
	@Id
	public Long id;
	
	public String link;
	
	public String description;
	
	public Integer width;
	
	public Integer height;
	
	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date date;
	
	
	public static final Finder<Long, Banner> find = new Finder<Long, Banner>(Long.class, Banner.class);
	
	public static Banner findById(Long id) {	
		return find.where().eq(FIELD_ID, id).findUnique();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
}
