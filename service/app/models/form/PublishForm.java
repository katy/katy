package models.form;

import play.data.validation.Constraints.Required;

public class PublishForm {
	
	@Required(message = "validation.required.header")
	public String header;
	
	@Required(message = "validation.required.content")
	public String content;

	@Required(message = "validation.required.content")
	public String status;
	
	public Object image;

	
}
