package models.form;

import play.data.validation.Constraints.Required;


public class PublishAfficheForm extends PublishForm {

	@Required(message = "validation.required.header")
	public String date;
	
	@Required(message = "validation.required.header")
	public String time;

	
}