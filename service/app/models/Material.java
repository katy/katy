package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.jws.soap.SOAPBinding.Use;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.data.format.Formats;
import play.db.ebean.Model;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagingList;

import controllers.Application;
import controllers.Users;

@Entity
@Table(name = "material")
public class Material extends Model implements ModelInterface  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// classificator
	public static final String CLASS_AFFICHE = "CLASS_AFFICHE";
	
	public static final String CLASS_REVIEW = "CLASS_REVIEW";
	public static final String CLASS_KNIGOVERTH = "CLASS_KNIGOVERTH";
	public static final String CLASS_INTERVIEW = "CLASS_INTERVIEW";
	
	public static final String CLASS_ALL = "CLASS_ALL";
	
	public static final String CLASS_REVIEW_STUDENT = "CLASS_REVIEW_STUDENT";
	
	public static final String CLASS_REVIEW_PROFESSOR = "CLASS_REVIEW_PROFESSOR";
	
	public static final String CLASS_BAND = "CLASS_BAND";
	
	public static final String CLASS_CUSTOM = "CLASS_CUSTOM";
	
	public static final String CLASS_USER = "CLASS_USER";
	
	public static final String CLASS_IGO = "CLASS_IGO";
	
	public static final String CLASS_MY = "CLASS_MY";

	public static final String CLASS_ALL_UNVISIBLE = "CLASS_ALL_UNVISIBLE";

	public static final String CLASS_USER_DRAFT = "CLASS_USER_DRAFT";
	
	public static final String CLASS_UNVIEWED_COMMENTS = "CLASS_UNVIEWED_COMMENTS";
	
	
	

	public static final String FIELD_ID = "id";
	public static final String FIELD_DATE = "date";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_OWNER = "owner";
	public static final String FIELD_TYPE = "type";
	public static final String FIELD_BOOK = "book";
	public static final String FIELD_VERSE = "verse";
	public static final String FIELD_HEADER = "header";
	public static final String FIELD_PUBLICAION = "publication";
	public static final String FIELD_COMMENTS = "comments";
	public static final String FIELD_IMAGE = "image";
	public static final String FIELD_CACHED_COMMENT_COUNT = "cachedCommentsCount";
	public static final String FIELD_START_DATE = "startDate"; // дата проведения
	public static final String FIELD_END_DATE = "startDate"; // дата окончания
		
	

	@Id
	public Long id;

	@NotNull
	@Column(nullable = false)
	public String header;
	
	@NotNull
	@Column(nullable = false)
	@Lob
	public String content;
	
	public String image;
	
	@NotNull
	@Column(nullable = false)
	public String type; // class of interview, knigoverth, Translate, iread, review, affiche
	
	@NotNull
	@Column(nullable = false)
	@OneToOne
	public User owner;
	
	public String publication;
	
	public String getSmallContent(int size) {
		String content = getContent();
		if(content.length()>size)
			return content.substring(0, size) + "...";
		return content;
	}
	
	// атрибуты материала (тэги)
	
	//@ManyToMany
	//private Set<User> users;
	
	public String cachedAttrUsers;

	@ManyToMany
	public Set<Book> books;
	
	public String cachedAttrBooks;
	
	@ManyToMany
	public Set<Author> authors;
	
	public String cachedAttrAuthors;
	
	@ManyToMany
	public Set<Genre> genres;
	
	public String cachedAttrGenres;
	
	public String theme;
	
	// я пойду
	@ManyToMany(mappedBy="igoMaterials")
	@JoinTable(name="users_igo_materials",
	 inverseJoinColumns=@JoinColumn(name="user_id"),
	 joinColumns=@JoinColumn(name="material_id")
	)
	public Set<User> igoUsers;
	
	public String cachedIgoUsers;
	
	public Integer cachedIgoUsersCount = 0;
	
	// лайки
	@ManyToMany(mappedBy="ilikeMaterials")
	@JoinTable(name="users_ilike_materials",
		inverseJoinColumns=@JoinColumn(name="user_id"),
		joinColumns=@JoinColumn(name="material_id")
	)
	public Set<User> ilikeUsers;
	
	public String cachedIlikeUsers;
	
	public Integer cachedIlikeUsersCount = 0;
	
	
	// комментарии
	
	@OneToMany(mappedBy = "material")
	public List<Comment> comments;
	
	public Integer cachedCommentsCount = 0;
	
	public String cachedComments;
	
	// даты
	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date date;
	
	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date startDate;

	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date endDate;

	
	
	public String getCachedComments() {
		return cachedComments;
	}

	public void setCachedComments(String cachedComments) {
		this.cachedComments = cachedComments;
	}

	@OneToOne
	public MaterialStatus matStatus;
	
	@OneToOne
	public MaterialStatus editorStatus;
	
	
  	public MaterialStatus getEditorStatus() {
		return editorStatus;
	}

	public void setEditorStatus(MaterialStatus editorStatus) {
		this.editorStatus = editorStatus;
	}

	public Integer getCachedIgoUsersCount() {
		return cachedIgoUsersCount;
	}

	public void setCachedIgoUsersCount(Integer cachedIgoUsersCount) {
		this.cachedIgoUsersCount = cachedIgoUsersCount;
	}

	public Integer getCachedIlikeUsersCount() {
		return cachedIlikeUsersCount;
	}

	public void setCachedIlikeUsersCount(Integer cachedIlikeUsersCount) {
		this.cachedIlikeUsersCount = cachedIlikeUsersCount;
	}

	public String getCachedAttrUsers() {
		return cachedAttrUsers;
	}

	public void setCachedAttrUsers(String cachedAttrUsers) {
		this.cachedAttrUsers = cachedAttrUsers;
	}

	public String getCachedAttrBooks() {
		return cachedAttrBooks;
	}

	public void setCachedAttrBooks(String cachedAttrBooks) {
		this.cachedAttrBooks = cachedAttrBooks;
	}

	public String getCachedAttrAuthors() {
		return cachedAttrAuthors;
	}

	public void setCachedAttrAuthors(String cachedAttrAuthors) {
		this.cachedAttrAuthors = cachedAttrAuthors;
	}

	public String getCachedAttrGenres() {
		return cachedAttrGenres;
	}

	public void setCachedAttrGenres(String cachedAttrGenres) {
		this.cachedAttrGenres = cachedAttrGenres;
	}

	public String getCachedIgoUsers() {
		return cachedIgoUsers;
	}

	public void setCachedIgoUsers(String cachedIgoUsers) {
		this.cachedIgoUsers = cachedIgoUsers;
	}

	public String getCachedIlikeUsers() {
		return cachedIlikeUsers;
	}

	public void setCachedIlikeUsers(String cachedIlikeUsers) {
		this.cachedIlikeUsers = cachedIlikeUsers;
	}

	public Set<User> getIgoUsers() {
		return igoUsers;
	}
	
	public void setIgoUsers(Set<User> igoUsers) {
		this.igoUsers = igoUsers;
	}
	
	public Set<User> getIlikeUsers() {
		return ilikeUsers;
	}
	
	public void setIlikeUsers(Set<User> ilikeUsers) {
		this.ilikeUsers = ilikeUsers;
	}
	
	public String getTheme() {
		return theme;
	}
	
	public void setTheme(String theme) {
		this.theme = theme;
	}
	
	public Integer getCachedCommentsCount() {
		return cachedCommentsCount;    
    }

	public void setCachedCommentsCount(Integer cachedCommentsCount) {
		this.cachedCommentsCount = cachedCommentsCount;
	}
	
	public MaterialStatus getMatStatus() {
		return matStatus;
	}

	public void setMatStatus(MaterialStatus matStatus) {
		this.matStatus = matStatus;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Comment> getComments() {
		return comments;
	}
	
	public String getViewHeader() {
		return getHeader().trim().toUpperCase();
	}
	
	public static Material getById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public Integer getCommentsPageSize() {
		return Comment.find.order().desc(Comment.FIELD_ID).where().eq("material_id", getId().toString()).findPagingList(Application.getPagesSize()).getTotalPageCount();
	}
	
	public List<Comment> getCommentsPage(Integer id) {
		PagingList<Comment> pList = Comment.find.order().desc(Comment.FIELD_ID).where().eq("material_id", getId().toString()).findPagingList(Application.getPagesSize());
		return pList.getPage(id-1).getList();
	}
	
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public void addComment(Comment comment) {
		if(getComments() == null)
			comments = new ArrayList<Comment>();
		comments.add(comment);
	}
	
	
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Set<Book> getBooks() {
		return books;
	}

	public void setBook(Set<Book> books) {
		this.books = books;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getPublication() {
		return publication;
	}

	public void setPublication(String publication) {
		this.publication = publication;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public static Material findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public static ExpressionList<Material> getVisibleMats() {
		return find.order().desc(FIELD_ID).where()
			.eq("editorStatus", MaterialStatus.getVisibleStatus())
			.eq("matStatus", MaterialStatus.getPublishedStatus());
	}
	
	public static ExpressionList<Material> getAllMats() {
		return find.order().desc(FIELD_ID).where();
	}
	
	public static PagingList<Material> allInvisible(int pageSize) {
		return find.order().desc(FIELD_ID).where().eq("editorStatus", MaterialStatus.getUnvisibleStatus()).findPagingList(pageSize);
	}

	public static PagingList<Material> allMyDraft(User user, int pageSize) {
		return find.order().desc(FIELD_ID).where()
			.eq(FIELD_OWNER + ".id", user.getId())
			.eq("editorStatus", MaterialStatus.getVisibleStatus())
			.eq("matStatus", MaterialStatus.getDraftStatus()).findPagingList(pageSize);
	}

	public static PagingList<Material> allInv(int pageSize) {
		return getVisibleMats().findPagingList(pageSize);
	}
	
	public static PagingList<Material> allReviewsFromStudents(int pageSize) {
		return getVisibleMats().eq(FIELD_TYPE, CLASS_REVIEW).eq("owner.status.name", Status.STATUS_STUDENT).findPagingList(pageSize);
	}
	
	public static PagingList<Material> allReviewsFromProfessors(int pageSize) {
		return getVisibleMats().eq(FIELD_TYPE, CLASS_REVIEW).eq("owner.status.name", Status.STATUS_PROFESSOR).findPagingList(pageSize);
	}
	
	public static PagingList<Material> allWithOwner(User user, Integer pagesSize) {
		return getVisibleMats().eq(FIELD_OWNER + ".id", user.getId()).findPagingList(pagesSize);
	}
	
	public static PagingList<Material> allIgo(User user, Integer pagesSize) {
		return getVisibleMats().eq("igoUsers.id", user.getId()).findPagingList(pagesSize);
	}	


	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public static final Finder<Long, Material> find = new Finder<Long, Material>(Long.class, Material.class);
	
	public static Material findById(String id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public static PagingList<Material> all(int pageSize) {
		return find.findPagingList(pageSize);
	}
	
	public static PagingList<Material> allByClass(int pageSize, String className) {
		return getVisibleMats().eq(FIELD_TYPE, className.trim()).findPagingList(pageSize);
		//return find.order().desc(Comment.FIELD_ID).where().eq(FIELD_TYPE, className.trim()).findPagingList(pageSize);
	}
	
//	public Set<User> getUsers() {
//		return users;
//	}
//
//	public void setUsers(Set<User> users) {
//		this.users = users;
//	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Set<Genre> getGenres() {
		return genres;
	}

	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

	public String getFormattedFIO() {
		return getOwner() == null ? null : getOwner().getFormattedFIO();
	}
	
	public String getFormatedDate() {
		return getDate() == null ? null : Application.materialFormatter.format(getDate());	
	}
	
	public String getFormatedStart() {
		return getStartDate() == null ? null : Application.materialFormatter.format(getStartDate());	
	}
	
	public String getFormatedEnd() {
		return getEndDate() == null ? null : Application.materialFormatter.format(getEndDate());	
	}

	public String getShortFormatedDate() {
		return getDate() == null ? null : Application.materialShortFormatter.format(getDate());	
	}
	
	public String getShortFormatedStart() {
		return getStartDate() == null ? null : Application.materialShortFormatter.format(getStartDate());	
	}
	
	public String getShortFormatedEnd() {
		return getEndDate() == null ? null : Application.materialShortFormatter.format(getEndDate());	
	}
  
	public static Integer getUnvisibleCount() {
                return find.order().desc(FIELD_ID).where()
                        .eq("editorStatus", MaterialStatus.getUnvisibleStatus()).findRowCount();
	}

        public String getStartDay() {
                Date date = getStartDate();
                if(date!=null)
                	return Application.dayFormatter.format(date);
                return null;
        }

        public String getStartMonth() {
                Date date = getStartDate();
                if(date != null)
                	return Application.monthFormatter.format(date);
                return null;
        }

	public String getStartWeekDay() {
                Date date = getStartDate();
                if(date != null)
                	return Application.weekDayFormatter.format(date);
                return null;
        }

	public String getViewName() {
		if(getType().equals(Material.CLASS_AFFICHE)) 
			return "Афиша";
		if(getType().equals(Material.CLASS_REVIEW))
			return "Рецензия";
		if(getType().equals(Material.CLASS_KNIGOVERTH)) 
			return "Всякая всячина";
		if(getType().equals(Material.CLASS_INTERVIEW)) 
			return "Интервью";
		return "Неизвестный материал";
	}


}
