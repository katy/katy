package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="status")
public class Status extends AbstractSelectModel implements ModelInterface  {

	private static final long serialVersionUID = 1L;	
	
	public static final String STATUS_PROFESSOR = "Профессор";
	public static final String STATUS_STUDENT = "Студент";
	
	public static final Finder<Long, Status> find = new Finder<Long, Status>(Long.class, Status.class);

	public static Status createStatus(String name) {
		Status status = new Status();
		status.name = name;
		status.save();
		return status;
	}

	public static Status getOrCreateStatus(String name) {
		Status status = findByName(name);
		if (status == null) {
			status = createStatus(name);
		}
		return status;
	}
	
	public static Status getStudentStatus() {
		return getOrCreateStatus(Status.STATUS_STUDENT);
	}
	
	public static Status getProfessorStatus() {
		return getOrCreateStatus(Status.STATUS_PROFESSOR);
	}
	
	public static List<Status> all() {
		return find.all();
	}
	
	public static Status findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}

	public static Status findByName(String name) {
		return find.where().eq(FIELD_NAME, name).findUnique();
	}
	
	public static String cachedStatusesString;
	
	public static String getStatusesSelect() {
		if(cachedStatusesString == null) {
			List<Status> all = all();
			cachedStatusesString = "";
			if(all != null && !all.isEmpty()) {
				cachedStatusesString += "[";
				for(int i=0; i<all.size(); i++) {
					if(i>0)
						cachedStatusesString += ",";
					Status status = all.get(i);					
					cachedStatusesString += "{id: \"" + status.getId() + "\", text: \"" + status.getName() + "\"}";  
				}
				cachedStatusesString += "],";
			}
		}
		return cachedStatusesString;
	}

	
}
