package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.data.format.Formats;
import play.db.ebean.Model;
import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;

import com.avaje.ebean.PagingList;

import controllers.Application;
import controllers.Users;

@Entity
@Table(name="users")
public class User extends Model implements Subject, ModelInterface {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_PASSWORD = "password";
	public static final String FIELD_HASH = "hash";
	public static final String FIELD_IP = "ip";
	
	public static final String FIELD_FIRST_NAME = "firstName";
	public static final String FIELD_SECOND_NAME = "secondName";
	public static final String FIELD_MIDDLE_NAME = "middleName";

	public static final String FIELD_EMAIL = "email";
	public static final String FIELD_BIRTHDAY = "birthday";

	public static final String FIELD_COLLEGE = "college";
	
	public static final String FIELD_STATUS = "status";
	
	public static final String FIELD_AVATAR = "avatar";
	
	public static final String FIELD_ROLE = "roles";
	
	public static final String FIELD_MATERIALS = "materials";
	
	public static final String FIELD_FRIENDS = "friends";
	
	public static final String FIELD_REGISTER_DATE = "registerDate";
	
	public static final String FIELD_LAST_ENTER = "lastEnter";

	public static final String FIELD_CACHED_BAND = "cachedBand";
	
	@Id
	public Long id;
	
	@NotNull
	@Column(nullable = false)
	public String password;

	public String hash;

	public String ip;
	
	public String firstName;
	
	public String secondName;
	
	public String middleName;

	public String cachedBand;

	//statistica
	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date registerDate;
	
	@Formats.DateTime(pattern="dd/MM/yyyy/hh/mm/ss")
	public Date lastEnter;

	@ManyToMany(mappedBy="unviewedUsers")
	public List<Comment> unviwedComments;
	public String cachedUnviwedComments;
	public Integer cachedUnviwedCommentsCount = 0;

	@ManyToOne
	public Status status;
	
	@NotNull
    @Column(nullable = false, unique=true)
	public String email;
	
    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date birthday;
    
    @ManyToOne
    public College college;

	public String avatar;
	
	@ManyToMany
	public List<SecurityRole> roles;
	
	@OneToMany(mappedBy = "owner")
	public List<Material> materials;

	// настройки ленты - все те сущности, которые мы слушаем

	@ManyToMany
	@JoinTable(name="users_users",
	 joinColumns=@JoinColumn(name="person_id"),
	 inverseJoinColumns=@JoinColumn(name="friend_id")
	)
	private Set<User> listenUsers;

	@ManyToMany(mappedBy="listenUsers")
	@JoinTable(name="users_users",
	 joinColumns=@JoinColumn(name="friend_id"),
	 inverseJoinColumns=@JoinColumn(name="person_id")
	)
	private Set<User> usersListen;



	// мы слушаем
	
	@ManyToMany
	public Set<Book> listenBooks;
	
	@ManyToMany
	public Set<Author> listenAuthors;
	
	@ManyToMany
	public Set<Genre> listenGenres;
	
	@ManyToMany
	public Set<College> listenColleges;

	//
	//@ManyToMany(mappedBy="users")
	//private Set<Material> objMaterials;
	
	
	// я пойду
	@ManyToMany
	@JoinTable(name="users_igo_materials",
	 joinColumns=@JoinColumn(name="user_id"),
	 inverseJoinColumns=@JoinColumn(name="material_id")
	)
	public Set<Material> igoMaterials;
	
	public String cachedIgoMaterials;
	
	public Integer cachedIgoMaterialsCount = 0;
	
	// лайки
	@ManyToMany
	@JoinTable(name="users_ilike_materials",
	 joinColumns=@JoinColumn(name="user_id"),
	 inverseJoinColumns=@JoinColumn(name="material_id")
	)
	public Set<Material> ilikeMaterials;
	
	public String cachedIlikeMaterials;
	

	public List<Comment> getUnviwedComments() {
		return unviwedComments;
	}

	public void setUnviwedComments(List<Comment> unviwedComments) {
		this.unviwedComments = unviwedComments;
	}

	public String getCachedUnviwedComments() {
		return cachedUnviwedComments;
	}

	public void setCachedUnviwedComments(String cachedUnviwedComments) {
		this.cachedUnviwedComments = cachedUnviwedComments;
	}

	public Integer getCachedUnviwedCommentsCount() {
		return cachedUnviwedCommentsCount;
	}

	public void setCachedUnviwedCommentsCount(Integer cachedUnviwedCommentsCount) {
		this.cachedUnviwedCommentsCount = cachedUnviwedCommentsCount;
	}

	public Integer getCachedIgoMaterialsCount() {
		return cachedIgoMaterialsCount;
	}

	public void setCachedIgoMaterialsCount(Integer cachedIgoMaterialsCount) {
		this.cachedIgoMaterialsCount = cachedIgoMaterialsCount;
	}

	public String getCachedIgoMaterials() {
		return cachedIgoMaterials;
	}

	public void setCachedIgoMaterials(String cachedIgoMaterials) {
		this.cachedIgoMaterials = cachedIgoMaterials;
	}

	public String getCachedIlikeMaterials() {
		return cachedIlikeMaterials;
	}

	public void setCachedIlikeMaterials(String cachedIlikeMaterials) {
		this.cachedIlikeMaterials = cachedIlikeMaterials;
	}

	public Set<Material> getIgoMaterials() {
		return igoMaterials;
	}

	public void setIgoMaterials(Set<Material> igoMaterials) {
		this.igoMaterials = igoMaterials;
	}

	public Set<Material> getIlikeMaterials() {
		return ilikeMaterials;
	}

	public void setIlikeMaterials(Set<Material> ilikeMaterials) {
		this.ilikeMaterials = ilikeMaterials;
	}

//	public Set<Material> getObjMaterials() {
//		return objMaterials;
//	}
//
//	public void setObjMaterials(Set<Material> objMaterials) {
//		this.objMaterials = objMaterials;
//	}

	public void addUnviewedComment(Comment comment) {
		if(getUnviwedComments() == null) {
			unviwedComments = new ArrayList<Comment>();
		}
		getUnviwedComments().add(comment);
	}
	
	// can optimize it?
	public boolean isListenUser(Long id) {
		if(getListenUsers()!=null) {
			for(ModelInterface model : getListenUsers()) {
				if(model.getId() == id)
					return true;
			}
		}
		return false;
	}
	
	public boolean isListenAuthor(Long id) {
		if(getListenAuthors()!=null) {
			for(ModelInterface model : getListenAuthors()) {
				if(model.getId() == id)
					return true;
			}
		}
		return false;
	}
	
	public boolean isListenGenre(Long id) {
		if(getListenGenres()!=null) {
			for(ModelInterface model : getListenGenres()) {
				if(model.getId() == id)
					return true;
			}
		}
		return false;
	}
	
	public boolean isListenBook(Long id) {
		if(getListenGenres()!=null) {
			for(ModelInterface model : getListenBooks()) {
				if(model.getId() == id)
					return true;
			}
		}
		return false;
	}

	public boolean isListenCollege(Long id) {
		if(getListenGenres()!=null) {
			for(ModelInterface model : getListenColleges()) {
				if(model.getId() == id)
					return true;
			}
		}
		return false;
	}

  public String getCachedBand() {
    return cachedBand;
  }

  public void setCachedBand(String cachedBand) {
    this.cachedBand = cachedBand;
  }

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getLastEnter() {
		return lastEnter;
	}

	public void setLastEnter(Date lastEnter) {
		this.lastEnter = lastEnter;
	}

	public Set<User> getListenUsers() {
		return listenUsers;
	}

	public void setListenUsers(Set<User> listenUsers) {
		this.listenUsers = listenUsers;
	}

	public Set<User> getUsersListen() {
		return usersListen;
	}

	public void setUsersListen(Set<User> usersListen) {
		this.usersListen = usersListen;
	}

	public Set<Book> getListenBooks() {
		return listenBooks;
	}

	public void setListenBooks(Set<Book> listenBooks) {
		this.listenBooks = listenBooks;
	}

	public Set<Author> getListenAuthors() {
		return listenAuthors;
	}

	public void setListenAuthors(Set<Author> listenAuthors) {
		this.listenAuthors = listenAuthors;
	}

	public Set<Genre> getListenGenres() {
		return listenGenres;
	}

	public void setListenGenres(Set<Genre> listenGenres) {
		this.listenGenres = listenGenres;
	}

	public Set<College> getListenColleges() {
		return listenColleges;
	}

	public void setListenColleges(Set<College> listenColleges) {
		this.listenColleges = listenColleges;
	}

	public void replaceRole(SecurityRole role) {
    	if(role.getName().equals(SecurityRole.ROLE_EDITOR)) {
         		removeRole(SecurityRole.getUserRole());
            } else if(role.getName().equals(SecurityRole.ROLE_USER)) {
         		removeRole(SecurityRole.getEditorRole());
            }
    	roles.add(role);
    }

	public void addRole(SecurityRole role) {
		if(getRoles() == null)
			roles = new ArrayList<SecurityRole>();
		if(!getRoles().contains(role))
			roles.add(role);
	}

	public void removeRole(SecurityRole role) {
		if(getRoles() != null) {
			Iterator<? extends Role> iter = getRoles().iterator();
			while(iter.hasNext()) {
				SecurityRole srole = (SecurityRole)iter.next();
				if(srole.getName().equals(role.getName())) {
					iter.remove();
				}
			}
		}
	}

	public void addMaterial(Material material) {
		if(getMaterials() == null) {
			List<Material> mats = new ArrayList<Material>();
			mats.add(material);
			setMaterials(mats);
		} else {
			getMaterials().add(material);
		}
	}
	
	@Override
	public List<? extends Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<SecurityRole> roles) {
		this.roles = roles;
	}

	@Override
	public List<? extends Permission> getPermissions() {
		return Collections.emptyList();
	}

	@Override
	public String getIdentifier() {
		return email;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public String getFormatedBirthday() {
		return getBirthday() == null ? null :Application.formatter.format(getBirthday());	
	}

	public static int getCount() {
		return find.findRowCount();
	}
	
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public College getCollege() {
		return college;
	}
	
	public void setCollege(College college) {
		this.college = college;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}
	
	public static final Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

	public static User findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public Integer getMyMaterialsCount() {
		return Material.find.where()
			.eq("owner.id", getId())
			.eq("editorStatus.id", MaterialStatus.getVisibleStatus().getId())
			.eq("matStatus.id", MaterialStatus.getPublishedStatus().getId())
		.findRowCount();
	}

	public Integer getMyDraftsCount() {
		return Material.find.where()
			.eq("owner.id", getId())
			.eq("editorStatus.id", MaterialStatus.getVisibleStatus().getId())
			.eq("matStatus.id", MaterialStatus.getDraftStatus().getId())
		.findRowCount();
	}


	public static User findByIdAndHash(String id, String hash) {
		return find.where().eq(FIELD_ID, id).eq(FIELD_HASH, hash).findUnique();
	}

	public static User findByEmail(String email) {
		//using workaround for MySQL instead of: find.where().ieq(FIELD_ID, email.trim()).findUnique();
		return find.where("LOWER(email) = LOWER(\"" + email.trim() + "\")").findUnique();
	}

	/**
	* Определяет можно ли редактировать информацию текущего пользователя с помощью авторизованного Users.getUser():
 	* Для этого авторизованный пользователь должен быть:
        * - ЕСТЬ АВТОИЗОВАННЫЙ ПОЛЬЗОВАТЕЛЬ && ( он дамин  || он редакция ||  если он является текущим)
	**/
	public boolean access() {
		User user = Users.getUser();
		return user != null && (user.isEditor() || user.isAdmin() || user.getId() == getId());
	}

	public boolean isAdmin() {
		if(roles != null) {
			for(SecurityRole role : roles) {
				if(role.getName().equals(SecurityRole.ROLE_ADMIN)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isUserAdmin() {
		if(roles != null) {
			for(SecurityRole role : roles) {
				if(role.getName().equals(SecurityRole.ROLE_USER) || role.getName().equals(SecurityRole.ROLE_ADMIN)) {
					return true;
				}
			}
		}
		return false;
	}


        public boolean isEditorAdmin() {
                if(roles != null) {
                        for(SecurityRole role : roles) {
                                if(role.getName().equals(SecurityRole.ROLE_EDITOR) || role.getName().equals(SecurityRole.ROLE_ADMIN)) {
                                        return true;
                                }
                        }
                }
                return false;
        }


	public boolean isUser() {
		if(roles != null) {
			for(SecurityRole role : roles) {
				if(role.getName().equals(SecurityRole.ROLE_USER)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isEditor() {
		if(roles != null) {
			for(SecurityRole role : roles) {
				if(role.getName().equals(SecurityRole.ROLE_EDITOR)) {
					return true;
				}
			}
		}
		return false;
	}

	public static PagingList<User> allPages(String userType, int pageSize) {
		if(userType == null) {
			return find.findPagingList(pageSize);	
		} else {
			if(userType.equals("ONLY_USERS")) {
				return find.order().desc(FIELD_ID).where()
                                        .eq("roles." + SecurityRole.FIELD_NAME, "user")
					.findPagingList(pageSize);
			} else if(userType.equals("ONLY_EDITORS")) {
				return find.order().desc(FIELD_ID).where()
                                        .eq("roles." + SecurityRole.FIELD_NAME, "editor")
                                        .findPagingList(pageSize);
			}
		}
		return find.order().desc(FIELD_ID).findPagingList(pageSize);
	}
	
	public void addAdminRole() {
		addRole(SecurityRole.getAdminRole());
	}
	
	public void addEditorRole() {
		replaceRole(SecurityRole.getEditorRole());
	}
	
	public void addUserRole() {
		replaceRole(SecurityRole.getUserRole());
	}
	
	public void removeEditorRole() {
		replaceRole(SecurityRole.getUserRole());
	}


	public void setStatusStudent() {
		setStatus(Status.getStudentStatus());
	}
	
	public void setStatusProfessor() {
		setStatus(Status.getProfessorStatus());
	}
	
	public String getFormattedFIO() {
		if(getFirstName() == null && getSecondName() == null && getMiddleName() == null) {
			return null;
		}
		
		if(getFirstName() != null && getSecondName() == null && getMiddleName() == null) {
			return getFirstName();
		}
		
		if(getFirstName() == null && getSecondName() != null && getMiddleName() == null) {
			return getSecondName();
		}
		
		if(getFirstName() != null && getSecondName() != null && getMiddleName() == null) {
			return getSecondName();
		}
		
		if(getFirstName() != null && getSecondName() != null && getMiddleName() != null) {
			if(getFirstName().length()>=2 && getMiddleName().length()>=1) {
				return (getSecondName().toUpperCase().charAt(0) + getSecondName().toLowerCase().substring(1)) + " " + getFirstName().toUpperCase().charAt(0) + "." + getMiddleName().toUpperCase().charAt(0) + ".";  
			}
		}

		return null;

	}

	public Boolean isUnviewed(Long commentId) {
		if(cachedUnviwedComments == null)
			return false;
		if(cachedUnviwedComments.contains(",")) {
			return cachedUnviwedComments.contains("," + commentId) || cachedUnviwedComments.contains(commentId + ",");
		} else {
			return cachedUnviwedComments.equals(commentId.toString());
		}
	}
}
