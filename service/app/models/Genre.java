package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avaje.ebean.PagingList;

import controllers.CacheUtils;

@Entity
@Table(name="genre")
public class Genre extends AbstractSelectModel {

	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_VERSES = "verses";
	public static final String FIELD_BOOKS = "books";
	
	@ManyToOne
	public Genre parent;
	
	@OneToMany(mappedBy = "parent")
	public List<Genre> child;
	
	@OneToMany(mappedBy = "genre")
	public List<Book> books;
	
	@ManyToMany(mappedBy="listenGenres")
	public Set<User> usersListen;
	
	@ManyToMany(mappedBy="genres")
	public Set<Material> objMaterials;

	public String cachedGenres;

	public String getCachedGenres() {
		return cachedGenres;
	}

	public void setCachedGenres(String cachedGenres) {
		this.cachedGenres = cachedGenres;
	}
	
	public Set<Material> getObjMaterials() {
		return objMaterials;
	}
	
	public Boolean isBase() {
		return getChild() == null || getChild().isEmpty(); 
	}
	
	public void addGenre(Genre genre) {
		if(getChild() == null) {
			child = new ArrayList<Genre>();
		}
		genre.setParent(this);
		getChild().add(genre);

		genre.save();
		setCachedGenres(CacheUtils.setId(getCachedGenres(), genre.getId(), true));
		save();
	}

	public Genre getParent() {
		return parent;
	}

	public void setParent(Genre parent) {
		this.parent = parent;
	}

	public List<Genre> getChild() {
		return child;
	}

	public void setChild(List<Genre> child) {
		this.child = child;
	}

	public void setObjMaterials(Set<Material> objMaterials) {
		this.objMaterials = objMaterials;
	}
	
	public Set<User> getUsersListen() {
		return usersListen;
	}
	
	public void setUsersListen(Set<User> usersListen) {
		this.usersListen = usersListen;
	}

	public static final Finder<Long, Genre> find = new Finder<Long, Genre>(Long.class, Genre.class);
	
	public static Genre findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public static Genre findByName(String name) {
		String prepName = name.trim().toUpperCase().replace(" +", " ");
		return find.where("LOWER(name) = LOWER(\"" + prepName + "\")").findUnique();
	}
	
	public static Map<String, String> allGenresBase() {
         	Map<String, String> genres = new HashMap<String, String>();
		for(Genre genre : find.where().eq("base", true).findList()) {
			genres.put(genre.getId()+"", genre.getName());
		}
		return genres;
	}
	
	public static List<Genre> allGenresNested() {
		return find.where().eq("parent", null).findList();
	}

	public static Map<String, String> allGenres() {
		Map<String, String> genres = new HashMap<String, String>();
		for(Genre genre : find.all()) {
			genres.put(genre.getId()+"", genre.getName());
		}
		return genres;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
	public void addBook(Book book) {
		if(books == null)
			books = new ArrayList<Book>();
		if(!books.contains(book))
			books.add(book);
	}
	
	public static int getCount() {
		return find.findRowCount();
	}
	
	
	public static PagingList<Genre> allPages(int pageSize) {
		return find.order().desc(FIELD_ID).findPagingList(pageSize);
	}
	
}
